package Visitor;

import ast.*;

public interface Visitor {
    public void visit (AST ctx);

    public void visit (Type ctx);
    public void visit (BasicType ctx);
    public void visit (ClassType ctx);
    public void visit (VoidType ctx);
    public void visit (BoolType ctx);
    public void visit (IntType ctx);
    public void visit (StringType ctx);
    public void visit (ArrayType ctx);

    public void visit (Decl ctx);
    public void visit (FuncDecl ctx);
    public void visit (ClassDecl ctx);
    public void visit (VarDecl ctx);
    public void visit (VarDeclList ctx);

    public void visit (Stmt ctx);
    public void visit (EmptyStmt ctx);
    public void visit (ReturnStmt ctx);
    public void visit (BreakStmt ctx);
    public void visit (CompoundStmt ctx);
    public void visit (VarDeclStmt ctx);
    public void visit (WhileStmt ctx);
    public void visit (IfStmt ctx);
    public void visit (ForStmt ctx);
    public void visit (ContinueStmt ctx);

    public void visit (Expr ctx);
    public void visit (EmptyExpr ctx);
    public void visit (BinaryExpr ctx);
    public void visit (Creator ctx);
    public void visit (UnaryExpr ctx);
    public void visit (PrefixExpr ctx);
    public void visit (SuffixExpr ctx);
    public void visit (SelfIncExpr ctx);
    public void visit (SelfDecExpr ctx);
    public void visit (ArrayAcess ctx);
    public void visit (ClassAcess ctx);
    public void visit (PrmExpr ctx);
    public void visit (IdExpr ctx);
    public void visit (FuncInvoke ctx);
    public void visit (ConstExpr ctx);
    public void visit (StringConst ctx);
    public void visit (IntConst ctx);
    public void visit (BoolConst ctx);
    public void visit (NullConst ctx);

    public void visit(Symbol ctx);

    public void visit(Prog ctx);
}
