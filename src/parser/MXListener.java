// Generated from MX.g4 by ANTLR 4.5

package parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MXParser}.
 */
public interface MXListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MXParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MXParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MXParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableDeclaration_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration_declaration(MXParser.VariableDeclaration_declarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableDeclaration_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration_declaration(MXParser.VariableDeclaration_declarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition_declaration(MXParser.FunctionDefinition_declarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition_declaration(MXParser.FunctionDefinition_declarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDefinition_declaration(MXParser.ClassDefinition_declarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDefinition_declaration(MXParser.ClassDefinition_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(MXParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(MXParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#singleParameter}.
	 * @param ctx the parse tree
	 */
	void enterSingleParameter(MXParser.SingleParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#singleParameter}.
	 * @param ctx the parse tree
	 */
	void exitSingleParameter(MXParser.SingleParameterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code basicType_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 */
	void enterBasicType_variableType(MXParser.BasicType_variableTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code basicType_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 */
	void exitBasicType_variableType(MXParser.BasicType_variableTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 */
	void enterCompound_variableType(MXParser.Compound_variableTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 */
	void exitCompound_variableType(MXParser.Compound_variableTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primitiveType_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType_variableType(MXParser.PrimitiveType_variableTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primitiveType_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType_variableType(MXParser.PrimitiveType_variableTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ID_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 */
	void enterID_variableType(MXParser.ID_variableTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ID_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 */
	void exitID_variableType(MXParser.ID_variableTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code INT_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterINT_primitiveType(MXParser.INT_primitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code INT_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitINT_primitiveType(MXParser.INT_primitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BOOL_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterBOOL_primitiveType(MXParser.BOOL_primitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BOOL_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitBOOL_primitiveType(MXParser.BOOL_primitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code STRING_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterSTRING_primitiveType(MXParser.STRING_primitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code STRING_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitSTRING_primitiveType(MXParser.STRING_primitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void enterVariableInitializer(MXParser.VariableInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void exitVariableInitializer(MXParser.VariableInitializerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expression_variableCreator}
	 * labeled alternative in {@link MXParser#variableCreator}.
	 * @param ctx the parse tree
	 */
	void enterExpression_variableCreator(MXParser.Expression_variableCreatorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expression_variableCreator}
	 * labeled alternative in {@link MXParser#variableCreator}.
	 * @param ctx the parse tree
	 */
	void exitExpression_variableCreator(MXParser.Expression_variableCreatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(MXParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(MXParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterCompound_functionType(MXParser.Compound_functionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitCompound_functionType(MXParser.Compound_functionTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code functionBasicType_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBasicType_functionType(MXParser.FunctionBasicType_functionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code functionBasicType_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBasicType_functionType(MXParser.FunctionBasicType_functionTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code basicType_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 */
	void enterBasicType_functionType(MXParser.BasicType_functionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code basicType_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 */
	void exitBasicType_functionType(MXParser.BasicType_functionTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code VOID_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 */
	void enterVOID_functionType(MXParser.VOID_functionTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code VOID_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 */
	void exitVOID_functionType(MXParser.VOID_functionTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(MXParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(MXParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by the {@code singleParameter_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterSingleParameter_parameterList(MXParser.SingleParameter_parameterListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singleParameter_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitSingleParameter_parameterList(MXParser.SingleParameter_parameterListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterCompound_parameterList(MXParser.Compound_parameterListContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitCompound_parameterList(MXParser.Compound_parameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#funcParameter}.
	 * @param ctx the parse tree
	 */
	void enterFuncParameter(MXParser.FuncParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#funcParameter}.
	 * @param ctx the parse tree
	 */
	void exitFuncParameter(MXParser.FuncParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(MXParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(MXParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(MXParser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(MXParser.Compound_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primitiveStatement_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveStatement_statement(MXParser.PrimitiveStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primitiveStatement_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveStatement_statement(MXParser.PrimitiveStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code block_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlock_statement(MXParser.Block_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code block_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlock_statement(MXParser.Block_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement_statement(MXParser.ExpressionStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement_statement(MXParser.ExpressionStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableDeclaration_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration_statement(MXParser.VariableDeclaration_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableDeclaration_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration_statement(MXParser.VariableDeclaration_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement_statement(MXParser.IfStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement_statement(MXParser.IfStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code forStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement_statement(MXParser.ForStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code forStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement_statement(MXParser.ForStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement_statement(MXParser.WhileStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement_statement(MXParser.WhileStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code breakStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement_statement(MXParser.BreakStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code breakStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement_statement(MXParser.BreakStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code continueStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement_statement(MXParser.ContinueStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code continueStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement_statement(MXParser.ContinueStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returnStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement_statement(MXParser.ReturnStatement_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returnStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement_statement(MXParser.ReturnStatement_statementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SEMICOLON_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void enterSEMICOLON_statement(MXParser.SEMICOLON_statementContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SEMICOLON_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 */
	void exitSEMICOLON_statement(MXParser.SEMICOLON_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(MXParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(MXParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(MXParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(MXParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(MXParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(MXParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#forExpr0}.
	 * @param ctx the parse tree
	 */
	void enterForExpr0(MXParser.ForExpr0Context ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#forExpr0}.
	 * @param ctx the parse tree
	 */
	void exitForExpr0(MXParser.ForExpr0Context ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#forExpr1}.
	 * @param ctx the parse tree
	 */
	void enterForExpr1(MXParser.ForExpr1Context ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#forExpr1}.
	 * @param ctx the parse tree
	 */
	void exitForExpr1(MXParser.ForExpr1Context ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#forExpr2}.
	 * @param ctx the parse tree
	 */
	void enterForExpr2(MXParser.ForExpr2Context ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#forExpr2}.
	 * @param ctx the parse tree
	 */
	void exitForExpr2(MXParser.ForExpr2Context ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(MXParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(MXParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(MXParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(MXParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(MXParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(MXParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(MXParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(MXParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryExpression_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpression_expression(MXParser.BinaryExpression_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryExpression_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpression_expression(MXParser.BinaryExpression_expressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_expression(MXParser.Compound_expressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_expression(MXParser.Compound_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryExpression(MXParser.BinaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#binaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryExpression(MXParser.BinaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_binaryRelationExpression(MXParser.Compound_binaryRelationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_binaryRelationExpression(MXParser.Compound_binaryRelationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryCalculusExpression_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryCalculusExpression_binaryRelationExpression(MXParser.BinaryCalculusExpression_binaryRelationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryCalculusExpression_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryCalculusExpression_binaryRelationExpression(MXParser.BinaryCalculusExpression_binaryRelationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_binaryCalculusExpression(MXParser.Compound_binaryCalculusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_binaryCalculusExpression(MXParser.Compound_binaryCalculusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalRelationExpression_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 */
	void enterEqualRelationExpression_binaryCalculusExpression(MXParser.EqualRelationExpression_binaryCalculusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalRelationExpression_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 */
	void exitEqualRelationExpression_binaryCalculusExpression(MXParser.EqualRelationExpression_binaryCalculusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationExpression_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 */
	void enterRelationExpression_equalRelationExpression(MXParser.RelationExpression_equalRelationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationExpression_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 */
	void exitRelationExpression_equalRelationExpression(MXParser.RelationExpression_equalRelationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_equalRelationExpression(MXParser.Compound_equalRelationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_equalRelationExpression(MXParser.Compound_equalRelationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code shiftExpression_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpression_relationExpression(MXParser.ShiftExpression_relationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code shiftExpression_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpression_relationExpression(MXParser.ShiftExpression_relationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_relationExpression(MXParser.Compound_relationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_relationExpression(MXParser.Compound_relationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_shiftExpression(MXParser.Compound_shiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_shiftExpression(MXParser.Compound_shiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpression_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression_shiftExpression(MXParser.AdditiveExpression_shiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpression_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression_shiftExpression(MXParser.AdditiveExpression_shiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicativeExpression_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression_additiveExpression(MXParser.MultiplicativeExpression_additiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicativeExpression_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression_additiveExpression(MXParser.MultiplicativeExpression_additiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_additiveExpression(MXParser.Compound_additiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_additiveExpression(MXParser.Compound_additiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpressio_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpressio_multiplicativeExpression(MXParser.UnaryExpressio_multiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpressio_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpressio_multiplicativeExpression(MXParser.UnaryExpressio_multiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompound_multiplicativeExpression(MXParser.Compound_multiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompound_multiplicativeExpression(MXParser.Compound_multiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code suffixExpression_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterSuffixExpression_unaryExpression(MXParser.SuffixExpression_unaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code suffixExpression_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitSuffixExpression_unaryExpression(MXParser.SuffixExpression_unaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NEWcreator_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterNEWcreator_unaryExpression(MXParser.NEWcreator_unaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NEWcreator_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitNEWcreator_unaryExpression(MXParser.NEWcreator_unaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prefix_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrefix_unaryExpression(MXParser.Prefix_unaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prefix_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrefix_unaryExpression(MXParser.Prefix_unaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#prefixOperator}.
	 * @param ctx the parse tree
	 */
	void enterPrefixOperator(MXParser.PrefixOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#prefixOperator}.
	 * @param ctx the parse tree
	 */
	void exitPrefixOperator(MXParser.PrefixOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryExpression_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpression_suffixExpression(MXParser.PrimaryExpression_suffixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryExpression_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpression_suffixExpression(MXParser.PrimaryExpression_suffixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void enterArrayAcess_suffixExpression(MXParser.ArrayAcess_suffixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void exitArrayAcess_suffixExpression(MXParser.ArrayAcess_suffixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code suffix_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void enterSuffix_suffixExpression(MXParser.Suffix_suffixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code suffix_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void exitSuffix_suffixExpression(MXParser.Suffix_suffixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void enterClassAcess_suffixExpression(MXParser.ClassAcess_suffixExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 */
	void exitClassAcess_suffixExpression(MXParser.ClassAcess_suffixExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SELFPLUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 */
	void enterSELFPLUS_suffixOperator(MXParser.SELFPLUS_suffixOperatorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SELFPLUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 */
	void exitSELFPLUS_suffixOperator(MXParser.SELFPLUS_suffixOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SELFMINUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 */
	void enterSELFMINUS_suffixOperator(MXParser.SELFMINUS_suffixOperatorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SELFMINUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 */
	void exitSELFMINUS_suffixOperator(MXParser.SELFMINUS_suffixOperatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ID_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterID_primaryExpression(MXParser.ID_primaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ID_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitID_primaryExpression(MXParser.ID_primaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code constant_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstant_primaryExpression(MXParser.Constant_primaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code constant_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstant_primaryExpression(MXParser.Constant_primaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PAREN_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPAREN_primaryExpression(MXParser.PAREN_primaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PAREN_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPAREN_primaryExpression(MXParser.PAREN_primaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FuncInvoke_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterFuncInvoke_primaryExpression(MXParser.FuncInvoke_primaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FuncInvoke_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitFuncInvoke_primaryExpression(MXParser.FuncInvoke_primaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 */
	void enterCompound_funcInvoke(MXParser.Compound_funcInvokeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 */
	void exitCompound_funcInvoke(MXParser.Compound_funcInvokeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expression_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 */
	void enterExpression_funcInvoke(MXParser.Expression_funcInvokeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expression_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 */
	void exitExpression_funcInvoke(MXParser.Expression_funcInvokeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IntegerConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterIntegerConstant_constant(MXParser.IntegerConstant_constantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IntegerConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitIntegerConstant_constant(MXParser.IntegerConstant_constantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BoolConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterBoolConstant_constant(MXParser.BoolConstant_constantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BoolConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitBoolConstant_constant(MXParser.BoolConstant_constantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code StringConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterStringConstant_constant(MXParser.StringConstant_constantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code StringConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitStringConstant_constant(MXParser.StringConstant_constantContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NULL_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterNULL_constant(MXParser.NULL_constantContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NULL_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitNULL_constant(MXParser.NULL_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#creator}.
	 * @param ctx the parse tree
	 */
	void enterCreator(MXParser.CreatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#creator}.
	 * @param ctx the parse tree
	 */
	void exitCreator(MXParser.CreatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code single_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 */
	void enterSingle_arrayCreator(MXParser.Single_arrayCreatorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code single_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 */
	void exitSingle_arrayCreator(MXParser.Single_arrayCreatorContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 */
	void enterCompound_arrayCreator(MXParser.Compound_arrayCreatorContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 */
	void exitCompound_arrayCreator(MXParser.Compound_arrayCreatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#classDefinition}.
	 * @param ctx the parse tree
	 */
	void enterClassDefinition(MXParser.ClassDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#classDefinition}.
	 * @param ctx the parse tree
	 */
	void exitClassDefinition(MXParser.ClassDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code classMemberDeclaration_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassMemberDeclaration_memberDeclaration(MXParser.ClassMemberDeclaration_memberDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code classMemberDeclaration_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassMemberDeclaration_memberDeclaration(MXParser.ClassMemberDeclaration_memberDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compound_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterCompound_memberDeclaration(MXParser.Compound_memberDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compound_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitCompound_memberDeclaration(MXParser.Compound_memberDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassMemberDeclaration(MXParser.ClassMemberDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassMemberDeclaration(MXParser.ClassMemberDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#binaryRelationOperator}.
	 * @param ctx the parse tree
	 */
	void enterBinaryRelationOperator(MXParser.BinaryRelationOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#binaryRelationOperator}.
	 * @param ctx the parse tree
	 */
	void exitBinaryRelationOperator(MXParser.BinaryRelationOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#binaryCalculusOperator}.
	 * @param ctx the parse tree
	 */
	void enterBinaryCalculusOperator(MXParser.BinaryCalculusOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#binaryCalculusOperator}.
	 * @param ctx the parse tree
	 */
	void exitBinaryCalculusOperator(MXParser.BinaryCalculusOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#equalRelationOperator}.
	 * @param ctx the parse tree
	 */
	void enterEqualRelationOperator(MXParser.EqualRelationOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#equalRelationOperator}.
	 * @param ctx the parse tree
	 */
	void exitEqualRelationOperator(MXParser.EqualRelationOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#relationOperator}.
	 * @param ctx the parse tree
	 */
	void enterRelationOperator(MXParser.RelationOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#relationOperator}.
	 * @param ctx the parse tree
	 */
	void exitRelationOperator(MXParser.RelationOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#shiftOperator}.
	 * @param ctx the parse tree
	 */
	void enterShiftOperator(MXParser.ShiftOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#shiftOperator}.
	 * @param ctx the parse tree
	 */
	void exitShiftOperator(MXParser.ShiftOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#additiveOperator}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveOperator(MXParser.AdditiveOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#additiveOperator}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveOperator(MXParser.AdditiveOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MXParser#multiplicativeOperator}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeOperator(MXParser.MultiplicativeOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MXParser#multiplicativeOperator}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeOperator(MXParser.MultiplicativeOperatorContext ctx);
}