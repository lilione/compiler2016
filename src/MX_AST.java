import ast.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import parser.MXBaseListener;
import parser.MXParser;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.Stack;

public class MX_AST extends MXBaseListener {
	Stack<AST> stack = new Stack<AST>();

	@Override public void exitProgram(MXParser.ProgramContext ctx) {
		Prog prog = new Prog();
		LinkedList<Decl> decls = new LinkedList<Decl>();
		while (!stack.empty()) decls.add((Decl)stack.pop());
		while (decls.size() != 0) {
			prog.decls.add(decls.getLast());
			decls.removeLast();
		}
		stack.push(prog);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitFunctionDefinition_declaration(MXParser.FunctionDefinition_declarationContext ctx) { }

	@Override public void exitClassDefinition_declaration(MXParser.ClassDefinition_declarationContext ctx) { }

	@Override public void exitVariableDeclaration(MXParser.VariableDeclarationContext ctx) {
		Symbol symbol = Symbol.get(ctx.singleParameter().ID().getText());
		Expr expr = null;
		if (ctx.variableInitializer() != null) expr = (Expr)stack.pop();
		Type type = (Type)stack.pop();
		VarDecl varDecl = new VarDecl(type, symbol, expr);
		stack.push(varDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSingleParameter(MXParser.SingleParameterContext ctx) { }

	@Override public void exitBasicType_variableType(MXParser.BasicType_variableTypeContext ctx) { }

	@Override public void exitCompound_variableType(MXParser.Compound_variableTypeContext ctx) {
		Type type = (Type)stack.pop();
		ArrayType arrayType = new ArrayType(type);
		stack.push(arrayType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitPrimitiveType_variableType(MXParser.PrimitiveType_variableTypeContext ctx) { }

	@Override public void exitID_variableType(MXParser.ID_variableTypeContext ctx) {
		ClassType classType = new ClassType(Symbol.get(ctx.getText()));
		stack.push(classType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitINT_primitiveType(MXParser.INT_primitiveTypeContext ctx) {
		IntType intType = new IntType();
		stack.push(intType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBOOL_primitiveType(MXParser.BOOL_primitiveTypeContext ctx) {
		BoolType boolType = new BoolType();
		stack.push(boolType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSTRING_primitiveType(MXParser.STRING_primitiveTypeContext ctx) {
		StringType stringType = new StringType();
		stack.push(stringType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitVariableInitializer(MXParser.VariableInitializerContext ctx) { }

	@Override public void exitExpression_variableCreator(MXParser.Expression_variableCreatorContext ctx) { }

	@Override public void exitFunctionDefinition(MXParser.FunctionDefinitionContext ctx) {
		CompoundStmt compoundStmt = new CompoundStmt();
		VarDeclList varDeclList = null;
		Type type = null;
		if (ctx.block() != null) compoundStmt = (CompoundStmt)stack.pop();
		if (ctx.parameter().parameterList() != null) varDeclList = (VarDeclList)stack.pop();
		Symbol symbol = Symbol.get(ctx.ID().getText());
		if (ctx.functionType() != null) type = (Type)stack.pop();
		FuncDecl funcDecl = new FuncDecl(type, symbol, varDeclList, compoundStmt);
		stack.push(funcDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_functionType(MXParser.Compound_functionTypeContext ctx) {
		Type type = (Type) stack.pop();
		ArrayType arrayType = new ArrayType(type);
		stack.push(arrayType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitFunctionBasicType_functionType(MXParser.FunctionBasicType_functionTypeContext ctx) { }

	@Override public void exitBasicType_functionType(MXParser.BasicType_functionTypeContext ctx) { }

	@Override public void exitVOID_functionType(MXParser.VOID_functionTypeContext ctx) {
		VoidType voidType = new VoidType();
		stack.push(voidType);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitParameter(MXParser.ParameterContext ctx) { }

	@Override public void exitSingleParameter_parameterList(MXParser.SingleParameter_parameterListContext ctx) {
		VarDeclList varDeclList = new VarDeclList();
		varDeclList.listVarDecl.add((VarDecl)stack.pop());
		stack.push(varDeclList);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_parameterList(MXParser.Compound_parameterListContext ctx) {
		VarDecl single = (VarDecl)stack.pop();
		VarDeclList varDeclList = (VarDeclList) stack.pop();
		varDeclList.listVarDecl.add(single);
		stack.push(varDeclList);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitFuncParameter(MXParser.FuncParameterContext ctx) {
		VarDecl varDecl = new VarDecl((Type)stack.pop(), Symbol.get(ctx.singleParameter().ID().getText()));
		stack.push(varDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBlock(MXParser.BlockContext ctx) {
		if (ctx.statement() == null) {
			EmptyStmt emptyStmt = new EmptyStmt();
			CompoundStmt compoundStmt = new CompoundStmt();
			compoundStmt.stmtList.add(emptyStmt);
			stack.push(compoundStmt);
			stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
		}
	}

	@Override public void exitPrimitiveStatement_statement(MXParser.PrimitiveStatement_statementContext ctx) {
		CompoundStmt compoundStmt = new CompoundStmt();
		Stmt stmt;
		if ((stack.peek() instanceof CompoundStmt) && (((CompoundStmt) stack.peek()).stmtList.size() == 1) && (((CompoundStmt) stack.peek()).stmtList.getFirst() instanceof EmptyStmt)) {
			stmt = new EmptyStmt();
			stack.pop();
		}
		else stmt = (Stmt)stack.pop();
		compoundStmt.stmtList.add(stmt);
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_statement(MXParser.Compound_statementContext ctx) {
		Stmt stmt = (Stmt)stack.pop();
		CompoundStmt compoundStmt = (CompoundStmt)stack.pop();
		compoundStmt.stmtList.add(stmt);
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBlock_statement(MXParser.Block_statementContext ctx) { }

	@Override public void exitExpressionStatement_statement(MXParser.ExpressionStatement_statementContext ctx) { }

	@Override public void exitVariableDeclaration_statement(MXParser.VariableDeclaration_statementContext ctx) {
		VarDeclStmt varDeclStmt = new VarDeclStmt((VarDecl)stack.pop());
		stack.push(varDeclStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitIfStatement_statement(MXParser.IfStatement_statementContext ctx) {
		Expr condition;
		Stmt thenStmt, elseStmt = null;
		if (ctx.ifStatement().primitiveStatement(1) != null) elseStmt = (Stmt)stack.pop();
		thenStmt = (Stmt)stack.pop();
		condition = (Expr)stack.pop();
		IfStmt ifStmt = new IfStmt(condition, thenStmt, elseStmt);
		stack.push(ifStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitForStatement_statement(MXParser.ForStatement_statementContext ctx) {
		Expr expr0 = null, expr1 = null, expr2 = null;
		Stmt stmt = (Stmt)stack.pop();
		if (ctx.forStatement().forExpr2() != null) expr2 = (Expr)stack.pop();
		if (ctx.forStatement().forExpr1() != null) expr1 = (Expr)stack.pop();
		if (ctx.forStatement().forExpr0() != null) expr0 = (Expr)stack.pop();
		ForStmt forStmt = new ForStmt(expr0, expr1, expr2, stmt);
		stack.push(forStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitWhileStatement_statement(MXParser.WhileStatement_statementContext ctx) {
		Stmt stmt = (Stmt)stack.pop();
		Expr expr = (Expr)stack.pop();
		WhileStmt whileStmt = new WhileStmt(expr, stmt);
		stack.push(whileStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBreakStatement_statement(MXParser.BreakStatement_statementContext ctx) {
		BreakStmt breakStmt = new BreakStmt();
		stack.push(breakStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitContinueStatement_statement(MXParser.ContinueStatement_statementContext ctx) {
		ContinueStmt continueStmt = new ContinueStmt();
		stack.push(continueStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitReturnStatement_statement(MXParser.ReturnStatement_statementContext ctx) {
		Expr expr = null;
		if (ctx.returnStatement().expression() != null) expr = (Expr)stack.pop();
		ReturnStmt returnStmt = new ReturnStmt(expr);
		stack.push(returnStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSEMICOLON_statement(MXParser.SEMICOLON_statementContext ctx) {
		EmptyStmt emptyStmt = new EmptyStmt();
		stack.push(emptyStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitExpressionStatement(MXParser.ExpressionStatementContext ctx) { }

	@Override public void exitIfStatement(MXParser.IfStatementContext ctx) { }

	@Override public void exitForStatement(MXParser.ForStatementContext ctx) { }

	@Override public void exitWhileStatement(MXParser.WhileStatementContext ctx) { }

	@Override public void exitBreakStatement(MXParser.BreakStatementContext ctx) { }

	@Override public void exitContinueStatement(MXParser.ContinueStatementContext ctx) { }

	@Override public void exitReturnStatement(MXParser.ReturnStatementContext ctx) { }

	@Override public void exitBinaryExpression_expression(MXParser.BinaryExpression_expressionContext ctx) { }

	@Override public void exitCompound_expression(MXParser.Compound_expressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op = BinaryOp.ASSIGN;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBinaryExpression(MXParser.BinaryExpressionContext ctx) { }

	@Override public void exitCompound_binaryRelationExpression(MXParser.Compound_binaryRelationExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.binaryRelationOperator().getText().equals("&&")) op = BinaryOp.ANDAND;
		else op = BinaryOp.OROR;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBinaryCalculusExpression_binaryRelationExpression(MXParser.BinaryCalculusExpression_binaryRelationExpressionContext ctx) { }

	@Override public void exitCompound_binaryCalculusExpression(MXParser.Compound_binaryCalculusExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.binaryCalculusOperator().getText().equals("&")) op = BinaryOp.AND;
		else if (ctx.binaryCalculusOperator().getText().equals("^")) op = BinaryOp.XOR;
		else op = BinaryOp.OR;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitEqualRelationExpression_binaryCalculusExpression(MXParser.EqualRelationExpression_binaryCalculusExpressionContext ctx) { }

	@Override public void exitRelationExpression_equalRelationExpression(MXParser.RelationExpression_equalRelationExpressionContext ctx) { }

	@Override public void exitCompound_equalRelationExpression(MXParser.Compound_equalRelationExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.equalRelationOperator().getText().equals("==")) op = BinaryOp.EQUAL;
		else op = BinaryOp.NOTEQUAL;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitShiftExpression_relationExpression(MXParser.ShiftExpression_relationExpressionContext ctx) { }

	@Override public void exitCompound_relationExpression(MXParser.Compound_relationExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.relationOperator().getText().equals("<=")) op = BinaryOp.LESSTHANOREQUAL;
		else if (ctx.relationOperator().getText().equals(">=")) op = BinaryOp.GREATERTHANOREQUAL;
		else if (ctx.relationOperator().getText().equals("<")) op = BinaryOp.LESSTHAN;
		else op = BinaryOp.GREATERTHAN;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_shiftExpression(MXParser.Compound_shiftExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.shiftOperator().getText().equals(">>")) op = BinaryOp.SHIFTRIGHT;
		else op = BinaryOp.SHIFTLEFT;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitAdditiveExpression_shiftExpression(MXParser.AdditiveExpression_shiftExpressionContext ctx) { }

	@Override public void exitMultiplicativeExpression_additiveExpression(MXParser.MultiplicativeExpression_additiveExpressionContext ctx) { }

	@Override public void exitCompound_additiveExpression(MXParser.Compound_additiveExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.additiveOperator().getText().equals("+")) op = BinaryOp.PLUS;
		else op = BinaryOp.MINUS;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitUnaryExpressio_multiplicativeExpression(MXParser.UnaryExpressio_multiplicativeExpressionContext ctx) { }

	@Override public void exitCompound_multiplicativeExpression(MXParser.Compound_multiplicativeExpressionContext ctx) {
		Expr rvalue = (Expr)stack.pop();
		Expr lvalue = (Expr)stack.pop();
		BinaryOp op;
		if (ctx.multiplicativeOperator().getText().equals("*")) op = BinaryOp.STAR;
		else if (ctx.multiplicativeOperator().getText().equals("/")) op = BinaryOp.DIVIDE;
		else op = BinaryOp.MOD;
		BinaryExpr binaryExpr = new BinaryExpr(lvalue, op, rvalue);
		stack.push(binaryExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSuffixExpression_unaryExpression(MXParser.SuffixExpression_unaryExpressionContext ctx) { }

	@Override public void exitNEWcreator_unaryExpression(MXParser.NEWcreator_unaryExpressionContext ctx) { }

	@Override public void exitPrefix_unaryExpression(MXParser.Prefix_unaryExpressionContext ctx) {
		PrefixOp op;
		Expr expr = (Expr)stack.pop();
		if (ctx.prefixOperator().getText().equals("++")) op = PrefixOp.SELFPLUS;
		else if (ctx.prefixOperator().getText().equals("--")) op = PrefixOp.SELFMINUS;
		else if (ctx.prefixOperator().getText().equals("!")) op = PrefixOp.NOT;
		else if (ctx.prefixOperator().getText().equals("~")) op = PrefixOp.TILDE;
		else op = PrefixOp.MINUS;
		PrefixExpr prefixExpr = new PrefixExpr(op, expr);
		stack.push(prefixExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitPrefixOperator(MXParser.PrefixOperatorContext ctx) { }

	@Override public void exitPrimaryExpression_suffixExpression(MXParser.PrimaryExpression_suffixExpressionContext ctx) { }

	@Override public void exitArrayAcess_suffixExpression(MXParser.ArrayAcess_suffixExpressionContext ctx) {
		Expr name, para = null;
		if (ctx.expression() != null) para = (Expr)stack.pop();
		name = (Expr) stack.pop();
		ArrayAcess arrayAcess = new ArrayAcess(name, para);
		stack.push(arrayAcess);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSuffix_suffixExpression(MXParser.Suffix_suffixExpressionContext ctx) { }

	@Override public void exitClassAcess_suffixExpression(MXParser.ClassAcess_suffixExpressionContext ctx) {
		Expr sub = (Expr)stack.pop();
		Expr name = (Expr)stack.pop();
		ClassAcess classAcess = new ClassAcess(name, sub);
		stack.push(classAcess);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSELFPLUS_suffixOperator(MXParser.SELFPLUS_suffixOperatorContext ctx) {
		SelfIncExpr selfIncExpr = new SelfIncExpr((Expr)stack.pop());
		stack.push(selfIncExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSELFMINUS_suffixOperator(MXParser.SELFMINUS_suffixOperatorContext ctx) {
		SelfDecExpr selfDecExpr = new SelfDecExpr((Expr)stack.pop());
		stack.push(selfDecExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitID_primaryExpression(MXParser.ID_primaryExpressionContext ctx) {
		Symbol name = Symbol.get(ctx.ID().getText());
		IdExpr idExpr = new IdExpr(name);
		stack.push(idExpr);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitConstant_primaryExpression(MXParser.Constant_primaryExpressionContext ctx) { }

	@Override public void exitPAREN_primaryExpression(MXParser.PAREN_primaryExpressionContext ctx) { }

	@Override public void exitFuncInvoke_primaryExpression(MXParser.FuncInvoke_primaryExpressionContext ctx) {
		CompoundStmt para = null;
		if (ctx.funcInvoke() != null) para = (CompoundStmt)stack.pop();
		Symbol name = Symbol.get(ctx.ID().getText());
		FuncInvoke funcInvoke = new FuncInvoke(name, para);
		stack.push(funcInvoke);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitExpression_funcInvoke(MXParser.Expression_funcInvokeContext ctx) {
		CompoundStmt compoundStmt = new CompoundStmt();
		compoundStmt.stmtList.add((Stmt)stack.pop());
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_funcInvoke(MXParser.Compound_funcInvokeContext ctx) {
		Stmt stmt = (Stmt)stack.pop();
		CompoundStmt compoundStmt = (CompoundStmt)stack.pop();
		compoundStmt.stmtList.add(stmt);
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitIntegerConstant_constant(MXParser.IntegerConstant_constantContext ctx) {
		IntConst intConst = new IntConst(new BigInteger(ctx.getText()));
		stack.push(intConst);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBoolConstant_constant(MXParser.BoolConstant_constantContext ctx) {
		BoolConst boolConst = new BoolConst(Boolean.parseBoolean(ctx.getText()));
		stack.push(boolConst);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitStringConstant_constant(MXParser.StringConstant_constantContext ctx) {
		StringConst stringConst = new StringConst(ctx.getText());
		stack.push(stringConst);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitNULL_constant(MXParser.NULL_constantContext ctx) {
		NullConst nullConst = new NullConst();
		stack.push(nullConst);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCreator(MXParser.CreatorContext ctx) {
		CompoundStmt compoundStmt = null;
		if (ctx.arrayCreator() != null) compoundStmt = (CompoundStmt)stack.pop();
		Type type = (Type)stack.pop();
		Creator creator = new Creator(type, compoundStmt);
		stack.push(creator);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitSingle_arrayCreator(MXParser.Single_arrayCreatorContext ctx) {
		CompoundStmt compoundStmt = new CompoundStmt();
		if (ctx.expression() != null) compoundStmt.stmtList.add((Stmt)stack.pop());
		else compoundStmt.stmtList.add(new EmptyExpr());
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_arrayCreator(MXParser.Compound_arrayCreatorContext ctx) {
		Stmt stmt = new EmptyExpr();
		if (ctx.expression() != null) stmt = (Stmt)stack.pop();
		CompoundStmt compoundStmt = (CompoundStmt)stack.pop();
		compoundStmt.stmtList.add(stmt);
		stack.push(compoundStmt);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitClassDefinition(MXParser.ClassDefinitionContext ctx) {
		VarDeclList varDeclList = null;
		if (ctx.memberDeclaration() != null) varDeclList = (VarDeclList)stack.pop();
		ClassDecl classDecl = new ClassDecl(Symbol.get(ctx.ID().getText()), varDeclList);
		stack.push(classDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitClassMemberDeclaration_memberDeclaration(MXParser.ClassMemberDeclaration_memberDeclarationContext ctx) {
		VarDeclList varDeclList = new VarDeclList();
		varDeclList.listVarDecl.add((VarDecl)stack.pop());
		stack.push(varDeclList);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitCompound_memberDeclaration(MXParser.Compound_memberDeclarationContext ctx) {
		VarDecl single = (VarDecl)stack.pop();
		VarDeclList varDeclList = (VarDeclList)stack.pop();
		varDeclList.listVarDecl.add(single);
		stack.push(varDeclList);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitClassMemberDeclaration(MXParser.ClassMemberDeclarationContext ctx) {
		VarDecl varDecl = new VarDecl((Type)stack.pop(), Symbol.get(ctx.singleParameter().ID().getText()));
		stack.push(varDecl);
		stack.peek().info = new Info(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine());
	}

	@Override public void exitBinaryRelationOperator(MXParser.BinaryRelationOperatorContext ctx) { }

	@Override public void exitBinaryCalculusOperator(MXParser.BinaryCalculusOperatorContext ctx) { }

	@Override public void exitEqualRelationOperator(MXParser.EqualRelationOperatorContext ctx) { }

	@Override public void exitRelationOperator(MXParser.RelationOperatorContext ctx) { }

	@Override public void exitShiftOperator(MXParser.ShiftOperatorContext ctx) { }

	@Override public void exitAdditiveOperator(MXParser.AdditiveOperatorContext ctx) { }

	@Override public void exitMultiplicativeOperator(MXParser.MultiplicativeOperatorContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }

	@Override public void exitEveryRule(ParserRuleContext ctx) { }

	@Override public void visitTerminal(TerminalNode node) { }

	@Override public void visitErrorNode(ErrorNode node) { }
}