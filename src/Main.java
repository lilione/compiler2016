import ast.*;
import ir.LinearIR;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import parser.MXLexer;
import parser.MXParser;

import java.io.*;

public class Main {
    static Table table = new Table();
    static int scope = 0;
    static LinearIR linearIR = new LinearIR();
    static Table pos = new Table();

    public void built_in_table() {

    }

    public static void main(String[] args) throws IOException {
        //InputStream in = new FileInputStream("input.txt");
        InputStream in = System.in;
        ANTLRInputStream input = new ANTLRInputStream(in);
        MXLexer lexer = new MXLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MXParser parser = new MXParser(tokens);
        ParseTree tree = parser.program();

        if (parser.getNumberOfSyntaxErrors() != 0)  {
            System.out.println("Syntax Error!");
            System.exit(1);
        }

        ParseTreeWalker walker = new ParseTreeWalker();
        MX_AST evalByListener = new MX_AST();
        walker.walk(evalByListener, tree);

        AST root = evalByListener.stack.peek();
        /*Printer printer = new Printer();
        printer.visit(root);
        System.out.println("Done.");*/

        table.beginScope();
        scope++;

        table.put(Symbol.get("size"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("size"))));

        table.put(Symbol.get("length"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("length"))));
        table.put(Symbol.get("substring"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("substring"), new VarDeclList(new VarDecl(new IntType()), new VarDecl(new IntType())))));
        table.put(Symbol.get("parseInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("parseInt"))));
        table.put(Symbol.get("ord"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("ord"), new VarDeclList(new VarDecl(new IntType())))));

        table.put(Symbol.get("print"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("print"), new VarDeclList(new VarDecl(new StringType())))));
        table.put(Symbol.get("println"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("println"), new VarDeclList(new VarDecl(new StringType())))));
        table.put(Symbol.get("getString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("getString"))));
        table.put(Symbol.get("getInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("getInt"))));
        table.put(Symbol.get("toString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("toString"), new VarDeclList(new VarDecl(new IntType())))));
        root.round1(table, scope);
        root.round2(table, scope);
        root.round3(table, scope, false, null);

        root.translate(pos, table, scope, linearIR, null, null);
        //System.out.println(linearIR.print());

        scope--;
        table.endScope();

        System.out.println(linearIR.translate());
    }
}