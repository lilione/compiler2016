package ast;

import Visitor.Visitor;

public class NullType extends BasicType {

    public boolean equal(Type now) {
        if (now instanceof NullType) return true;
        return false;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }


    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        return new NullType();
    }
}
