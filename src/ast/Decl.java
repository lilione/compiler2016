package ast;

import Visitor.Visitor;

public abstract class Decl extends AST{
    public abstract void accept(Visitor visitor);
}