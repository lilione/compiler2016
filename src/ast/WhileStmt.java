package ast;

import Visitor.Visitor;
import ir.*;

public class WhileStmt extends Stmt {
	public Expr condition;
	public Stmt body;
	
	public WhileStmt(Expr condition, Stmt body) {
		this.condition = condition;
		this.body = body;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		if (!(condition.round3(table, scope, repeat, returnType) instanceof BoolType)) {
			System.out.println("Wrong WhileStmt!" + this.info.toString());
			System.exit(1);
		}
		table.beginScope(); scope++;
		body.round3(table, scope, true, returnType);
		scope--; table.endScope();
		return null;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Label l1 = new Label(), l2 = new Label(), l3 = new Label();
		linearIR.list.add(new Jump(l1));
		linearIR.list.add(l1);
		if (condition != null) {
			linearIR.list.add(new Branch(condition.translate(pos, table, scope, linearIR, up, down), l2, l3));
		}
		linearIR.list.add(l2);
		if (body != null) {
			pos.beginScope();
			body.translate(pos, table, scope, linearIR, l1, l3);
			pos.endScope();
		}
		linearIR.list.add(new Jump(l1));
		linearIR.list.add(l3);
		return null;
	}
}