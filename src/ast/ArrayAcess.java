package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;

import java.math.BigInteger;

public class ArrayAcess extends SuffixExpr{
    public Expr name, para;

    public ArrayAcess(Expr name, Expr para) {
        this.name = name;
        this.para = para;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        AST now = this;
        int num = 0;
        while (now instanceof ArrayAcess) {
            if (!(((ArrayAcess) now).para.round3(table, scope, repeat, returnType) instanceof IntType)) {
                System.out.println("Wrong ArrayAcess!" + this.info.toString());
                System.exit(1);
            }
            now = ((ArrayAcess)now).name;
            num++;
        }
        Type ret = (Type)now.round3(table, scope, repeat, returnType);
        for (int i = 0; i < num; i++) {
            if (!(ret instanceof ArrayType)) {
                System.out.println("Wrong ArrayAcess!" + this.info.toString());
                System.exit(1);
            }
            ret = ((ArrayType)ret).type;
        }
        ret.Lvalue = true;
        return ret;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Address tmp1 = name.translate(pos, table, scope, linearIR, up, down), tmp2 = para.translate(pos, table, scope, linearIR, up, down);
        Address tmp;
        if (tmp2 instanceof IntConst) {
            tmp = new IntConst(((IntConst)tmp2).value.multiply(BigInteger.valueOf(4)));
        }
        else {
            tmp = new Register();
            linearIR.list.add(new ir.Expr(tmp, Op.MUL, tmp2, new IntConst(BigInteger.valueOf(4))));
        }
        Address now;
        if (tmp1 instanceof IntConst && tmp instanceof IntConst) {
            now = new IntConst(((IntConst) tmp1).value.add(((IntConst) tmp).value));
        }
        else if (tmp1 instanceof IntConst) {
            now = new Register();
            linearIR.list.add(new ir.Expr(now, Op.ADD, tmp, tmp1));
        }
        else {
            now = new Register();
            linearIR.list.add(new ir.Expr(now, Op.ADD, tmp1, tmp));
        }
        Address ret = new Register();
        linearIR.list.add(new MemoryRead(ret, now, new IntConst(BigInteger.valueOf(4))));
        return ret;
    }

    @Override
    public Address getAddress(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Address tmp1 = name.translate(pos, table, scope, linearIR, up, down), tmp2 = para.translate(pos, table, scope, linearIR, up, down);
        Address tmp;
        if (tmp2 instanceof IntConst) {
            tmp = new IntConst(((IntConst)tmp2).value.multiply(BigInteger.valueOf(4)));
        }
        else {
            tmp = new Register();
            linearIR.list.add(new ir.Expr(tmp, Op.MUL, tmp2, new IntConst(BigInteger.valueOf(4))));
        }
        Address now;
        if (tmp1 instanceof IntConst && tmp instanceof IntConst) {
            now = new IntConst(((IntConst) tmp1).value.add(((IntConst) tmp).value));
        }
        else if (tmp1 instanceof IntConst) {
            now = new Register();
            linearIR.list.add(new ir.Expr(now, Op.ADD, tmp, tmp1));
        }
        else {
            now = new Register();
            linearIR.list.add(new ir.Expr(now, Op.ADD, tmp1, tmp));
        }
        return new Memory(now, new IntConst(BigInteger.valueOf(4)));
    }
}
