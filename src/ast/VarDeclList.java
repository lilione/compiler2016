package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

import java.util.*;

public class VarDeclList extends Decl {
    public LinkedList<VarDecl> listVarDecl;

    public VarDeclList() {
        this.listVarDecl = new LinkedList<>();
    }
    public VarDeclList(VarDecl varDecl) {
        this.listVarDecl = new LinkedList<>();
        listVarDecl.add(varDecl);
    }
    public VarDeclList(VarDecl varDecl1, VarDecl varDecl2) {
        this.listVarDecl = new LinkedList<>();
        listVarDecl.add(varDecl1);
        listVarDecl.add(varDecl2);
    }
    public VarDeclList(LinkedList<VarDecl> listVarDecl) {
        this.listVarDecl = listVarDecl;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        for (int i = 0; i < listVarDecl.size(); i++) {
            listVarDecl.get(i).round3(table, scope, repeat, returnType);
        }
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        for (int i = 0; i < listVarDecl.size(); i++) {
            listVarDecl.get(i).translate(pos, table, scope, linearIR, up, down);
        }
        return null;
    }
}
