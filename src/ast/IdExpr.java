package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;
import org.antlr.v4.runtime.misc.Pair;

public class IdExpr extends PrmExpr {
	public Symbol name;
	
	public IdExpr(Symbol name) {
		this.name = name;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		Pair<Integer, AST> now = (Pair<Integer, AST>) table.get(name);
		if (now == null) {
			System.out.println("Undefined ID!" + this.info.toString());
			System.exit(1);
		}
		Type ret;
		if (now.b instanceof FuncDecl) ret = ((FuncDecl)now.b).type;
		else if (now.b instanceof ClassDecl) ret = new ClassType(((ClassDecl)now.b).name);
		else {
			ret = (Type)now.b;
			ret.Lvalue = true;
		}
		return ret;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		return (Address)pos.get(name);
	}

	@Override
	public Address getAddress(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		return (Address)pos.get(name);
	}
}