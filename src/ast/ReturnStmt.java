package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

public class ReturnStmt extends Stmt {
	public Expr expr;

    public ReturnStmt(Expr expr) {
        this.expr = expr;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        Type ret = new VoidType();
        if (expr != null) ret = (Type)expr.round3(table, scope, repeat, returnType);
        if (returnType == null || !ret.equal((Type)returnType)) {
            System.out.println("Wrong ReturnStmt!" + this.info.toString());
            System.exit(1);
        }
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        if (expr == null) linearIR.list.add(new ir.Return(null));
        else linearIR.list.add(new ir.Return(expr.translate(pos, table, scope, linearIR, up, down)));
        return null;
    }
}