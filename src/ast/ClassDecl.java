package ast;

import Visitor.Visitor;
import org.antlr.v4.runtime.misc.Pair;

public class ClassDecl extends Decl {
	public Symbol name;
	public VarDeclList var;
	
	public ClassDecl(Symbol name, VarDeclList var) {
		this.name = name;
		this.var = var;
	}

	public int find(Symbol now) {
		int ret = 0;
		for (int i = 1; i < var.listVarDecl.size(); i++) {
			if (var.listVarDecl.get(i).name == now) {
				ret = i;
				break;
			}
		}
		return  ret;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void round1(Table table, int scope) {
		if (table.get(name) != null) {
			System.out.println("Repeated ClassDecl" + this.info.toString());
			System.exit(1);
		}
		table.put(name, new Pair<>(scope, this));
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		table.beginScope(); scope++;
		if (var != null) for (int i = 0; i < var.listVarDecl.size(); i++) var.listVarDecl.get(i).round3(table, scope, repeat, returnType);
		scope--; table.endScope();
		return null;
	}
}