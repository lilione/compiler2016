package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

public abstract class AST {
    public Info info;
    public abstract void accept(Visitor visitor);

    public void round1(Table table, int scope) {}

    public void round2(Table table, int scope) {}

    public AST round3(Table table, int scope, boolean repeat, AST returnType) {return null;}

    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {return null;}
}