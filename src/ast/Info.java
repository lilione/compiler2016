package ast;

public class Info {
    public int row, col;

    public Info(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public String toString() {
        return " At line " + row + ", column " + col + ".\n";
    }
}
