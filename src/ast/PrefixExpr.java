package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;

import java.math.BigInteger;

public class PrefixExpr extends UnaryExpr{
    public PrefixOp op;
    public Expr expr;

    public PrefixExpr(PrefixOp op, Expr expr) {
        this.op = op;
        this.expr = expr;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        Type ret = (Type)expr.round3(table, scope, repeat, returnType);
        if (op == PrefixOp.NOT) {
            if (ret instanceof BoolType) return ret;
        }
        else {
            if (ret instanceof IntType) return ret;
        }
        System.out.println("Wrong PrefixExpr!" + this.info.toString());
        System.exit(1);
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        if (op == PrefixOp.SELFPLUS) {
            Address now = expr.getAddress(pos, table, scope, linearIR, up, down);
            if (now instanceof Register) {
                linearIR.list.add(new ir.Expr(now, Op.ADD, now, new ir.IntConst(BigInteger.ONE)));
                return now;
            }
            else {
                Register tmp = new Register();
                linearIR.list.add(new MemoryRead(tmp, ((Memory)now).mem, ((Memory)now).offset));
                linearIR.list.add(new ir.Expr(tmp, Op.ADD, tmp, new ir.IntConst(BigInteger.ONE)));
                linearIR.list.add(new MemoryWrite(((Memory)now).mem, tmp, ((Memory)now).offset));
                return tmp;
            }
        }
        else if (op == PrefixOp.SELFMINUS) {
            Address now = expr.getAddress(pos, table, scope, linearIR, up, down);
            if (now instanceof Register) {
                linearIR.list.add(new ir.Expr(now, Op.SUB, now, new ir.IntConst(BigInteger.ONE)));
                return now;
            }
            else {
                Register tmp = new Register();
                linearIR.list.add(new MemoryRead(tmp, ((Memory)now).mem, ((Memory)now).offset));
                linearIR.list.add(new ir.Expr(tmp, Op.SUB, tmp, new ir.IntConst(BigInteger.ONE)));
                linearIR.list.add(new MemoryWrite(((Memory)now).mem, tmp, ((Memory)now).offset));
                return tmp;
            }
        }
        else {
            Address now = expr.translate(pos, table, scope, linearIR, up, down);
            if (op == PrefixOp.NOT) {
                if (now instanceof IntConst) {
                    return new IntConst(((IntConst)now).value.xor(BigInteger.ONE));
                }
                else {
                    Address tmp = new Register();
                    linearIR.list.add(new ir.Expr(tmp, Op.XOR, now, new IntConst(BigInteger.ONE)));
                    return tmp;
                }
            }
            else if (op == PrefixOp.TILDE) {
                if (now instanceof IntConst) {
                    return new IntConst(((IntConst)now).value.not());
                }
                else {
                    Address tmp = new Register();
                    linearIR.list.add(new ir.Expr(tmp, Op.NOT, now, null));
                    return tmp;
                }
            }
            else {//MINUS
                if (now instanceof IntConst) {
                    return new IntConst(((IntConst) now).value.negate());
                }
                else {
                    Address tmp = new Register();
                    linearIR.list.add(new ir.Expr(tmp, Op.NEG, tmp, null));
                    return tmp;
                }
            }
        }
    }
}