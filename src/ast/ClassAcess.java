package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;
import org.antlr.v4.runtime.misc.Pair;

import java.math.BigInteger;
import java.util.LinkedList;

public class ClassAcess extends SuffixExpr {
    public Expr name;
    public Expr sub;
    public Type nameType, subType;

    public ClassAcess(Expr name, Expr sub) {
        this.name = name;
        this.sub = sub;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        nameType = (Type)name.round3(table, scope, repeat, returnType);
        Type l = nameType;
        Type ret;
        Pair<Integer, AST> now;
        if (sub instanceof IdExpr) {
            if (l instanceof ClassType) {
                now = (Pair<Integer, AST>) table.get(((ClassType) l).name);
                LinkedList tmp = ((ClassDecl) now.b).var.listVarDecl;
                Symbol a = ((IdExpr)sub).name;
                for (int i = 0; i < tmp.size(); i++) {
                    Symbol b = ((VarDecl) tmp.get(i)).name;
                    if (a == b) {
                        ret = ((VarDecl) tmp.get(i)).type;
                        if (l.Lvalue == true) ret.Lvalue = true;
                        return ret;
                    }
                }
            }
        }
        else if (sub instanceof FuncInvoke) {
            subType = (Type)sub.round3(table, scope, repeat, returnType);
            ret = subType;
            if (l.Lvalue == true) ret.Lvalue = true;
            if (l instanceof ArrayType) {
                now = (Pair<Integer, AST>) table.get(Symbol.get("size"));
                if (((FuncDecl) now.b).equal((FuncInvoke) sub, table, scope, repeat, returnType)) return ret;
            }
            else if (l instanceof StringType) {
                now = (Pair<Integer, AST>) table.get(Symbol.get("length"));
                if (((FuncDecl)now.b).equal((FuncInvoke)sub, table, scope, repeat, returnType)) return ret;
                now = (Pair<Integer, AST>) table.get(Symbol.get("substring"));
                if (((FuncDecl)now.b).equal((FuncInvoke)sub, table, scope, repeat, returnType)) return ret;
                now = (Pair<Integer, AST>) table.get(Symbol.get("parseInt"));
                if (((FuncDecl)now.b).equal((FuncInvoke)sub, table, scope, repeat, returnType)) return ret;
                now = (Pair<Integer, AST>) table.get(Symbol.get("ord"));
                if (((FuncDecl)now.b).equal((FuncInvoke)sub, table, scope, repeat, returnType)) return ret;
            }
        }
        System.out.println("Invalid ClassAcess!" + this.info.toString());
        System.exit(1);
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Address l = name.translate(pos, table, scope, linearIR, up, down), ret = new Register();
        AST body = nameType;
        Pair<Integer, AST> now;
        if (sub instanceof IdExpr) {
            now = (Pair<Integer, AST>) table.get(((ClassType)body).name);
            int cnt = ((ClassDecl)now.b).find(((IdExpr) sub).name);
            linearIR.list.add(new MemoryRead(ret, l, new IntConst(BigInteger.valueOf(4 * cnt))));
        }
        else {
            LinkedList<Address> arg = new LinkedList<>();
            if (!(name instanceof ClassAcess)) {
                arg.add(l);
            }
            else {
                arg.add(sub.translate(pos, table, scope, linearIR, up, down));
            }
            if (((FuncInvoke)sub).para != null) {
                for (int i = 0; i < ((FuncInvoke) sub).para.stmtList.size(); i++) {
                    arg.add(((FuncInvoke) sub).para.stmtList.get(i).translate(pos, table, scope, linearIR, up, down));
                }
            }
            if (((FuncInvoke) sub).name == Symbol.get("size") || ((FuncInvoke) sub).name == Symbol.get("length")) {
                linearIR.list.add(new MemoryRead(ret, l, new IntConst(BigInteger.ZERO)));
            }
            else if (((FuncInvoke) sub).name == Symbol.get("ord")) {
                Register tmp = new Register();
                linearIR.list.add(new ir.Expr(tmp, Op.ADD, l, arg.get(1)));
                linearIR.list.add(new MemoryRead(ret, tmp, new IntConst(BigInteger.valueOf(4)), 1));
            }
            else if (((FuncInvoke) sub).name == Symbol.get("substring")) {
                linearIR.list.add(new BuiltInCall(ret, "substring", arg));
            }
            else if (((FuncInvoke) sub).name == Symbol.get("parseInt")) {
                linearIR.list.add(new BuiltInCall(ret, "parseInt", arg));
            }
        }
        return ret;
    }

    @Override
    public Address getAddress(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Address ret = null;
        if (sub instanceof IdExpr) {
            Address tmp = name.translate(pos, table, scope, linearIR, up, down);
            AST body = nameType;
            Pair<Integer, AST> now = (Pair<Integer, AST>) table.get(((ClassType)body).name);
            int cnt = ((ClassDecl)now.b).find(((IdExpr) sub).name);
            return new Memory(tmp, new IntConst(BigInteger.valueOf(4 * cnt)));
        }
        return ret;
    }
}
