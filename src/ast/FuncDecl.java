package ast;

import Visitor.Visitor;
import ir.*;
import org.antlr.v4.runtime.misc.Pair;

public class FuncDecl extends Decl {
	public Type type;
	public Symbol name;
	public VarDeclList prm;
	public CompoundStmt body;
	public Function callee;

	public FuncDecl(Type type, Symbol name) {
		this.type = type;
		this.name = name;
	}
	public FuncDecl(Type type, Symbol name, VarDeclList prm) {
		this.type = type;
		this.name = name;
		this.prm = prm;
	}
	public FuncDecl(Type type, Symbol name, VarDeclList prm, CompoundStmt body) {
		this.type = type;
		this.name = name;
		this.prm = prm;
		this.body = body;
	}

	public boolean equal(FuncInvoke now, Table table, int scope, boolean repeat, AST returnType) {
		if (now.name != name) return false;
		if (prm != null) {
			if (now.para == null) return false;
			if (now.para.stmtList.size() != prm.listVarDecl.size()) return false;
		}
		else {
			if (now.para != null) return false;
		}
		if (prm != null) for (int i = 0; i < prm.listVarDecl.size(); i++) {
			if (!prm.listVarDecl.get(i).type.equal((Type)now.para.stmtList.get(i).round3(table, scope, repeat, returnType))) return false;
		}
		return true;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void round2(Table table, int scope) {
		type.round2(table, scope);

		if (table.get(name) != null) {
			System.out.println("Repeated Function Name!" + this.info.toString());
			System.exit(1);
		}
		table.put(name, new Pair<>(scope, this));
		if (name == Symbol.get("main")) {
			if (!(type instanceof IntType)) {
				System.out.println("Main!" + this.info.toString());
				System.exit(1);
			}
			if (prm != null) {
				System.out.println("Main has parameters!" + this.info.toString());
				System.exit(1);
			}
		}
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		table.beginScope(); scope++;
		if (prm != null) prm.round3(table, scope, repeat, type);
		if (body != null) {
			body.round3(table, scope, repeat, type);
		}
		scope--; table.endScope();
		return null;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Label label = null;
		if (name.toString() != "main") label = new Label();
		else {
			label = new Label("main_enter");
		}
		Register.RegisterClear();
		pos.beginScope();
		table.beginScope(); scope++;
		callee = new Function(name.toString(), label);
		callee.name = name.toString();
		if (prm != null) {
			for (int i = 0; i < prm.listVarDecl.size(); i++) {
				Address tmp = new Register();
				pos.put(prm.listVarDecl.get(i).name, tmp);
				callee.para.add(tmp);
			}
		}
		int now = linearIR.list.size();
		if (body != null) {
			body.translate(pos, table, scope, linearIR, up, down);
		}
		for (int i = now; i < linearIR.list.size(); i++) {
			callee.body.add(linearIR.list.get(i));
		}
		while (linearIR.list.size() > now) linearIR.list.removeLast();
		for (int i = 0; i < callee.body.size(); i++) {
			if (callee.body.get(i) instanceof Call) {
				((Call) callee.body.get(i)).caller = callee;
			}
		}
		callee.size = Register.getRegisterNum();
		if (name.toString() == "main") {
			linearIR.mainsize = callee.size;
		}
		linearIR.list.add(callee);
		scope--; table.endScope();
		pos.endScope();
		return null;
	}
}