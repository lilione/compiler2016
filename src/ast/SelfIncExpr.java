package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;

import java.math.BigInteger;

public class SelfIncExpr extends SuffixExpr{
    public Expr expr;

    public SelfIncExpr(Expr expr) {
        this.expr = expr;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        Type ret = (Type)expr.round3(table, scope, repeat, returnType);
        if (ret instanceof IntType) return ret;
        System.out.println("Wrong SelfDecExpr!" + this.info.toString());
        System.exit(1);
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Address now = expr.getAddress(pos, table, scope, linearIR, up, down);
        Address ret = new Register();
        if (now instanceof Register) {
            linearIR.list.add(new Move(ret, now));
            linearIR.list.add(new ir.Expr(now, Op.ADD, now, new ir.IntConst(BigInteger.ONE)));
        }
        else {
            linearIR.list.add(new MemoryRead(ret, ((Memory)now).mem, ((Memory) now).offset));
            Register tmp = new Register();
            linearIR.list.add(new ir.Expr(tmp, Op.ADD, ret, new IntConst(BigInteger.ONE)));
            linearIR.list.add(new MemoryWrite(((Memory) now).mem, tmp, ((Memory) now).offset));
        }
        return ret;
    }
}
