package ast;

import Visitor.Visitor;

public class ArrayType extends Type {
	public Type type;

	public ArrayType(Type type) {
		this.type = type;
	}

	public boolean equal(Type now) {
		if (now instanceof BasicType) return false;
		now = ((ArrayType)now).type;
		if (((Type)type).equal(now)) return true;
		return false;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		return new ArrayType((Type)type.round3(table, scope, repeat, returnType));
	}
}