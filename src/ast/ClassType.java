package ast;

import Visitor.Visitor;

public class ClassType extends BasicType {
	public Symbol name;
	
	public ClassType(Symbol name) {
		this.name = name;
	}

	public boolean equal(Type now) {
		if (now instanceof ClassType && ((ClassType)now).name == name) return true;
		return false;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void round2(Table table, int scope) {
		if (table.get(name) == null) {
			System.out.println("Undefined Type!" + this.info.toString());
			System.exit(1);
		}
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		if (table.get(name) == null) {
			System.out.println("Undefined Type!" + this.info.toString());
			System.exit(1);
		}
		return this;
	}
}