package ast;

import Visitor.Visitor;
import ir.*;

import java.math.BigInteger;

public class ForStmt extends Stmt {
	public Expr stmt1, stmt2, stmt3;
	public Stmt body;
	
	public ForStmt(Expr stmt1, Expr stmt2, Expr stmt3, Stmt body) {
		this.stmt1 = stmt1;
		this.stmt2 = stmt2;
		this.stmt3 = stmt3;
		this.body = body;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		if (stmt1 != null) stmt1.round3(table, scope, repeat, returnType);
		if (stmt2 != null) {
			if (!(stmt2.round3(table, scope, repeat, returnType) instanceof BoolType)) {
				System.out.println("Wrond ForStmt!"  + this.info.toString());
				System.exit(1);
			}
		}
		if (stmt3 != null) stmt3.round3(table, scope, repeat, returnType);
		if (body != null) {
			table.beginScope();
			scope++;
			body.round3(table, scope, true, returnType);
			scope--;
			table.endScope();
		}
		return null;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Label l1 = new Label(), l2 = new Label(), l3 = new Label();
		if (stmt1 != null) {
			stmt1.translate(pos, table, scope, linearIR, up, down);
		}
		linearIR.list.add(new Jump(l1));
		linearIR.list.add(l1);
		if (stmt2 != null) {
			linearIR.list.add(new Branch(stmt2.translate(pos, table, scope, linearIR, up, down), l2, l3));
		}
		linearIR.list.add(l2);
		if (body != null) {
			pos.beginScope();
			table.beginScope(); scope++;
			body.translate(pos, table, scope, linearIR, l1, l3);
			scope--; table.endScope();
			pos.endScope();
		}
		if (stmt3 != null) {
			boolean ok = true;
			if (stmt3 instanceof UnaryExpr) {
				if (stmt3 instanceof SelfIncExpr) {
					ok = false;
					Address now = ((SelfIncExpr)stmt3).expr.getAddress(pos, table, scope, linearIR, up, down);
					if (now instanceof Register) {
						linearIR.list.add(new ir.Expr(now, Op.ADD, now, new ir.IntConst(BigInteger.ONE)));
					} else {
						Register tmp = new Register();
						linearIR.list.add(new MemoryRead(tmp, ((Memory) now).mem, ((Memory) now).offset));
						linearIR.list.add(new ir.Expr(tmp, Op.ADD, tmp, new ir.IntConst(BigInteger.ONE)));
						linearIR.list.add(new MemoryWrite(((Memory) now).mem, tmp, ((Memory) now).offset));
					}
				}
				else if (stmt3 instanceof SelfDecExpr) {
					ok = false;
					Address now = ((SelfDecExpr)stmt3).expr.getAddress(pos, table, scope, linearIR, up, down);
					if (now instanceof Register) {
						linearIR.list.add(new ir.Expr(now, Op.SUB, now, new ir.IntConst(BigInteger.ONE)));
					} else {
						Register tmp = new Register();
						linearIR.list.add(new MemoryRead(tmp, ((Memory) now).mem, ((Memory) now).offset));
						linearIR.list.add(new ir.Expr(tmp, Op.SUB, tmp, new ir.IntConst(BigInteger.ONE)));
						linearIR.list.add(new MemoryWrite(((Memory) now).mem, tmp, ((Memory) now).offset));
					}
				}
			}
			if (ok) stmt3.translate(pos, table, scope, linearIR, l1, l3);
		}
		linearIR.list.add(new Jump(l1));
		linearIR.list.add(l3);
		return null;
	}
}