package ast;

import Visitor.Visitor;

public abstract class PrmExpr extends SuffixExpr {
    public abstract void accept(Visitor visitor);
}
