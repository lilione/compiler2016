package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;
import org.antlr.v4.runtime.misc.Pair;

import java.math.BigInteger;

public class Creator extends Expr {
	public Type type;
	public CompoundStmt compoundStmt;
	
	public Creator(Type type) {
		this.type = type;
	}
	public Creator(Type type, CompoundStmt compoundStmt) {
		this.type = type;
		this.compoundStmt = compoundStmt;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		AST ret = type.round3(table, scope, repeat, returnType);
		if (compoundStmt != null) {
			if (!(compoundStmt.stmtList.get(0).round3(table, scope, repeat, returnType) instanceof IntType)) {
				System.out.println("Wrong Creator!" + this.info.toString());
				System.exit(1);
			}
			ret = new ArrayType((Type) ret);
			for (int i = 1; i < compoundStmt.stmtList.size(); i++) {
				if (!(compoundStmt.stmtList.get(i).round3(table, scope, repeat, returnType) instanceof NullType)) {
					System.out.println("Wrong Creator!" + this.info.toString());
					System.exit(1);
				}
				ret = new ArrayType((Type) ret);
			}
		}
		return ret;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Address size;
		Address tmp = new Register(), ret = new Register();
		if (compoundStmt == null) {
			Pair<Integer, AST> now = (Pair<Integer, AST>) table.get(((ClassType)type).name);
			size = new ir.IntConst(BigInteger.valueOf(((ClassDecl)now.b).var.listVarDecl.size()));
			linearIR.list.add(new Move(tmp, new IntConst(((IntConst)size).value.multiply(BigInteger.valueOf(4)))));
		}
		else {
			size = compoundStmt.stmtList.get(0).translate(pos, table, scope, linearIR, up, down);
			if (size instanceof IntConst) {
				Address now = new IntConst(((IntConst) size).value.add(BigInteger.ONE));
				linearIR.list.add(new Move(tmp, new IntConst(((IntConst) now).value.multiply(BigInteger.valueOf(4)))));
			}
			else {
				Address now = new Register();
				linearIR.list.add(new ir.Expr(now, Op.ADD, size, new IntConst(BigInteger.ONE)));
				linearIR.list.add(new ir.Expr(tmp, Op.MUL, now, new IntConst(BigInteger.valueOf(4))));
			}
		}
		linearIR.list.add(new Allocate(ret, tmp));
		if (!(type instanceof ClassType)) {
			if (size instanceof IntConst) {
				Register rep = new Register();
				linearIR.list.add(new Move(rep, size));
				linearIR.list.add(new MemoryWrite(ret, rep, new IntConst(BigInteger.ZERO)));
			}
			else {
				linearIR.list.add(new MemoryWrite(ret, size, new IntConst(BigInteger.ZERO)));
			}
		}
		return ret;
	}

}