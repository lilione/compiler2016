package ast;

import Visitor.Visitor;
import ir.*;

import java.util.*;

public class Prog extends AST{
    public LinkedList<Decl> decls;

    public Prog() {
        decls = new LinkedList<Decl>();
    }
    public Prog(LinkedList<Decl> decls) {
        this.decls = decls;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void round1(Table table, int scope) {
        for (int i = 0; i < decls.size(); i++) {
            if (decls.get(i) instanceof ClassDecl) decls.get(i).round1(table, scope);
        }
    }

    @Override
    public void round2(Table table, int scope) {
        for (int i = 0; i < decls.size(); i++) {
            decls.get(i).round2(table, scope);
        }
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        for (int i = 0; i < decls.size(); i++) {
            decls.get(i).round3(table, scope, repeat, returnType);
        }
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        StringAddress.init(linearIR);
        for (int i = 0; i < decls.size(); i++) {
            if (decls.get(i) instanceof VarDecl) {
                ((VarDecl)decls.get(i)).global(pos, table, scope, linearIR, up, down);
            }
        }
        for (int i = 0; i < linearIR.list.size(); i++) {
            linearIR.init.add(linearIR.list.get(i));
        }
        linearIR.list.clear();
        linearIR.size = Register.getRegisterNum();
        for (int i = 0; i < decls.size(); i++) {
            if (!(decls.get(i) instanceof VarDecl)) {
                decls.get(i).translate(pos, table, scope, linearIR, up, down);
            }
        }
        return null;
    }
}