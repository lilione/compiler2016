package ast;

import Visitor.Visitor;

public abstract class SuffixExpr extends UnaryExpr {
    public abstract void accept(Visitor visitor);
}
