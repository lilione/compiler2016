package ast;

import Visitor.Visitor;
import ir.*;

public class StringConst extends ConstExpr {
	public String value, origin;
	
	public StringConst(String now) {
		this.origin = "";
		for (int i = 1; i < now.length() - 1; i++) this.origin += now.charAt(i);
		this.value = "";
		for (int i = 0; i < this.origin.length(); i++) {
			if (this.origin.charAt(i) == '\\' && i < this.origin.length() - 1) {
				if (this.origin.charAt(i + 1) == 'n') {
					this.value = this.value + '\n';
					++i;
				}
				else if (this.origin.charAt(i + 1) == '\\') {
					this.value = this.value + '\\';
					++i;
				}
				else if (this.origin.charAt(i + 1) == '\"') {
					this.value = this.value + '\"';
					++i;
				}
				else
					this.value = this.value + this.origin.charAt(i);
			}
			else
				this.value = this.value + this.origin.charAt(i);
		}
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		return new StringType();
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		StringAddress tmp = new StringAddress(value, origin, linearIR);
		Register ret = new Register();
		linearIR.list.add(new ir.Move(ret, tmp));
		return ret;
	}
}