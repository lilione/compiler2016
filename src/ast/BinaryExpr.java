package ast;

import Visitor.Visitor;
import ir.*;
import ir.IntConst;

import java.math.BigInteger;
import java.util.LinkedList;

public class BinaryExpr extends Expr {
	public Expr lvalue, rvalue;
	public BinaryOp op;
	Type ltype, rtype;

	public BinaryExpr(Expr lvalue, BinaryOp op, Expr rvalue) {
		this.lvalue = lvalue;
		this.op = op;
		this.rvalue = rvalue;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		ltype = (Type)lvalue.round3(table, scope, repeat, returnType);
		rtype = (Type)rvalue.round3(table, scope, repeat, returnType);
		Type l = ltype, r = rtype;
		if (r instanceof NullType) {
			if (op == BinaryOp.ASSIGN) {
				if (!l.Lvalue) {
					System.out.println("Invalid BinaryExpr" + this.info.toString());
					System.exit(1);
				}
				if (l instanceof BasicType && !(l instanceof ClassType)) {
					System.out.println("Invalid BinaryExpr" + this.info.toString());
					System.exit(1);
				}
			}
			else if (op == BinaryOp.EQUAL || op == BinaryOp.NOTEQUAL) {
				if (l instanceof BasicType && !(l instanceof ClassType)) {
					System.out.println("Invalid BinaryExpr" + this.info.toString());
					System.exit(1);
				}
				return new BoolType();
			}
			else  {
				System.out.println("Invalid BinaryExpr" + this.info.toString());
				System.exit(1);
			}
		}
		else if (!l.equal(r)) {
			System.out.println("Invalid BinaryExpr" + this.info.toString());
			System.exit(1);
		}
		else if (op == BinaryOp.ASSIGN) {
			if (!l.Lvalue) {
				System.out.println("Invalid BinaryExpr" + this.info.toString());
				System.exit(1);
			}
		}
		else {
			Type now = l;
			while (now instanceof ArrayType) now = ((ArrayType) now).type;
			if (now instanceof IntType) {
				if (op == BinaryOp.ANDAND || op == BinaryOp.OROR) {
					System.out.println("Invalid BinaryExpr" + this.info.toString());
					System.exit(1);
				} else if (op == BinaryOp.EQUAL || op == BinaryOp.NOTEQUAL ||
						op == BinaryOp.LESSTHANOREQUAL || op == BinaryOp.GREATERTHANOREQUAL || op == BinaryOp.LESSTHAN || op == BinaryOp.GREATERTHAN) {
					return new BoolType();
				} else return new IntType();
			} else if (now instanceof BoolType) {
				if (op == BinaryOp.ANDAND || op == BinaryOp.OROR || op == BinaryOp.EQUAL || op == BinaryOp.NOTEQUAL)
					return new BoolType();
				else {
					System.out.println("Invalid BinaryExpr" + this.info.toString());
					System.exit(1);
				}
			} else if (now instanceof StringType) {
				if (op == BinaryOp.PLUS) return new StringType();
				else if (op == BinaryOp.EQUAL || op == BinaryOp.NOTEQUAL ||
						op == BinaryOp.LESSTHANOREQUAL || op == BinaryOp.GREATERTHANOREQUAL || op == BinaryOp.LESSTHAN || op == BinaryOp.GREATERTHAN) {
					return new BoolType();
				}
			}
			System.out.println("Invalid BinaryExpr" + this.info.toString());
			System.exit(1);
		}
		return r;
	}

	public Address shortroad(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		if (this.op != BinaryOp.ANDAND) return this.translate(pos, table, scope, linearIR, up, down);
		Address l, r;
		if (lvalue instanceof BinaryExpr && ((BinaryExpr)lvalue).op == BinaryOp.ANDAND) {
			l = ((BinaryExpr) lvalue).shortroad(pos, table, scope, linearIR, up, down);
		}
		else l = lvalue.translate(pos, table, scope, linearIR, up, down);
		Label label = new Label();
		linearIR.list.add(new Branch(l, label, down));
		linearIR.list.add(label);
		r = rvalue.translate(pos, table, scope, linearIR, up, down);
		return r;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		if (op == BinaryOp.ANDAND) {
			Address l = lvalue.translate(pos, table, scope, linearIR, up, down);
			if (l instanceof IntConst) {
				if (((IntConst)l).value == BigInteger.ZERO) {
					return new IntConst(BigInteger.ZERO);
				}
				else {
					return rvalue.translate(pos, table, scope, linearIR, up, down);
				}
			}
			else {
				Address ret = new Register();
				Label begin = new Label(), yes = new Label(), no = new Label(), end = new Label();
				linearIR.list.add(new Branch(l, begin, no));
				linearIR.list.add(begin);
				Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
				linearIR.list.add(new Branch(r, yes, no));
				linearIR.list.add(yes);
				linearIR.list.add(new Move(ret, new IntConst(BigInteger.ONE)));
				linearIR.list.add(new Jump(end));
				linearIR.list.add(no);
				linearIR.list.add(new Move(ret, new IntConst(BigInteger.ZERO)));
				linearIR.list.add(new Jump(end));
				linearIR.list.add(end);
				return ret;
			}
		}
		else if (op == BinaryOp.OROR) {
			Address l = lvalue.translate(pos, table, scope, linearIR, up, down);
			if (l instanceof IntConst) {
				if (((IntConst)l).value == BigInteger.ONE) {
					return new IntConst(BigInteger.ONE);
				}
				else {
					return rvalue.translate(pos, table, scope, linearIR, up, down);
				}
			}
			else {
				Address ret = new Register();
				Label begin = new Label(), yes = new Label(), no = new Label(), end = new Label();
				linearIR.list.add(new Branch(l, no, begin));
				linearIR.list.add(begin);
				Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
				linearIR.list.add(new Branch(r, no, yes));
				linearIR.list.add(yes);
				linearIR.list.add(new Move(ret, new IntConst(BigInteger.ZERO)));
				linearIR.list.add(new Jump(end));
				linearIR.list.add(no);
				linearIR.list.add(new Move(ret, new IntConst(BigInteger.ONE)));
				linearIR.list.add(new Jump(end));
				linearIR.list.add(end);
				return ret;
			}
		}
		//Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
		Address ret = null;
		if (op == BinaryOp.ASSIGN) {
			Address l = lvalue.getAddress(pos, table, scope, linearIR, up, down);
			if (l instanceof Register) {
				if (rvalue instanceof BinaryExpr) {
					if (((BinaryExpr) rvalue).op == BinaryOp.PLUS) {
						Address a = ((BinaryExpr) rvalue).lvalue.translate(pos, table, scope, linearIR, up, down);
						Address b = ((BinaryExpr) rvalue).rvalue.translate(pos, table, scope, linearIR, up, down);
						linearIR.list.add(new ir.Expr(l, Op.ADD, a, b));
					}
					else if (((BinaryExpr) rvalue).op == BinaryOp.MINUS) {
						Address a = ((BinaryExpr) rvalue).lvalue.translate(pos, table, scope, linearIR, up, down);
						Address b = ((BinaryExpr) rvalue).rvalue.translate(pos, table, scope, linearIR, up, down);
						linearIR.list.add(new ir.Expr(l, Op.SUB, a, b));
					}
					else {
						Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
						linearIR.list.add(new Move(l, r));
					}
				}
				else {
					Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
					linearIR.list.add(new Move(l, r));
				}
				ret = l;
			}
			else {
				Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
				if (r instanceof IntConst) {
					Register tmp = new Register();
					linearIR.list.add(new Move(tmp, r));
					linearIR.list.add(new MemoryWrite(((Memory) l).mem, tmp, ((Memory) l).offset));
					ret = tmp;
				}
				else {
					linearIR.list.add(new MemoryWrite(((Memory) l).mem, r, ((Memory) l).offset));
					ret = r;
				}
			}
		}
		else {
			Address l = lvalue.translate(pos, table, scope, linearIR, up, down);
			Address r = rvalue.translate(pos, table, scope, linearIR, up, down);
			if (op == BinaryOp.AND) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.and(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.AND, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.AND, l, r));
					}
				}
			}
			else if (op == BinaryOp.OR) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.or(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.OR, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.OR, l, r));
					}
				}
			}
			else if (op == BinaryOp.XOR) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.xor(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.XOR, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.XOR, l, r));
					}
				}
			}
			else if (op == BinaryOp.EQUAL){
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringEquals", arg));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (((IntConst) l).value.equals(((IntConst) r).value)) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SEQ, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SEQ, l, r));
					}
				}
			}
			else if (op == BinaryOp.NOTEQUAL) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringEquals", arg));
					linearIR.list.add(new ir.Expr(ret, Op.XOR, ret, new ir.IntConst(BigInteger.ONE)));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (!(((IntConst) l).value.equals(((IntConst) r).value))) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SNE, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SNE, l, r));
					}
				}
			}
			else if (op == BinaryOp.LESSTHANOREQUAL) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringLessThanOrEquals", arg));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (((IntConst) l).value.compareTo(((IntConst) r).value) != 1) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SGT, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SLE, l, r));
					}
				}
			}
			else if (op == BinaryOp.GREATERTHANOREQUAL) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringLessThan", arg));
					linearIR.list.add(new ir.Expr(ret, Op.XOR, ret, new IntConst(BigInteger.ONE)));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (((IntConst) l).value.compareTo(((IntConst) r).value) != -1) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SLT, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SGE, l, r));
					}
				}
			}
			else if (op == BinaryOp.LESSTHAN) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringLessThan", arg));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (((IntConst) l).value.compareTo(((IntConst) r).value) == -1) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SGE, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SLT, l, r));
					}
				}
			}
			else if (op == BinaryOp.GREATERTHAN) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringLessThanOrEquals", arg));
					linearIR.list.add(new ir.Expr(ret, Op.XOR, ret, new IntConst(BigInteger.ONE)));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					if (((IntConst) l).value.compareTo(((IntConst) r).value) != 1) {
						ret = new IntConst(BigInteger.ONE);
					}
					else {
						ret = new IntConst(BigInteger.ZERO);
					}
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.SLE, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SGT, l, r));
					}
				}
			}
			else if (op == BinaryOp.SHIFTLEFT) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.shiftLeft(((IntConst) r).value.intValue()));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						Register tmp = new Register();
						linearIR.list.add(new Move(tmp, l));
						linearIR.list.add(new ir.Expr(ret, Op.SHL, tmp, r));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SHL, l, r));
					}
				}
			}
			else if (op == BinaryOp.SHIFTRIGHT) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.shiftRight(((IntConst) r).value.intValue()));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						Register tmp = new Register();
						linearIR.list.add(new Move(tmp, l));
						linearIR.list.add(new ir.Expr(ret, Op.SHR, tmp, r));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SHR, l, r));
					}
				}
			}
			else if (op == BinaryOp.PLUS) {
				if (ltype instanceof StringType) {
					LinkedList<Address>arg = new LinkedList<Address>();
					arg.add(l);
					arg.add(r);
					ret = new Register();
					linearIR.list.add(new BuiltInCall(ret, "stringAdd", arg));
				}
				else if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.add(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.ADD, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.ADD, l, r));
					}
				}
			}
			else if (op == BinaryOp.MINUS) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.subtract(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						Register tmp = new Register();
						linearIR.list.add(new Move(tmp, l));
						linearIR.list.add(new ir.Expr(ret, Op.SUB, tmp, r));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.SUB, l, r));
					}
				}
			}
			else if (op == BinaryOp.STAR) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.multiply(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						linearIR.list.add(new ir.Expr(ret, Op.MUL, r, l));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.MUL, l, r));
					}
				}
			}
			else if (op == BinaryOp.DIVIDE) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.divide(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						Register tmp = new Register();
						linearIR.list.add(new Move(tmp, l));
						linearIR.list.add(new ir.Expr(ret, Op.DIV, tmp, r));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.DIV, l, r));
					}
				}
			}
			else if (op == BinaryOp.MOD) {
				if (l instanceof IntConst && r instanceof IntConst) {
					ret = new IntConst(((IntConst) l).value.mod(((IntConst) r).value));
				}
				else {
					ret = new Register();
					if (l instanceof IntConst) {
						Register tmp = new Register();
						linearIR.list.add(new Move(tmp, l));
						linearIR.list.add(new ir.Expr(ret, Op.REM, tmp, r));
					}
					else {
						linearIR.list.add(new ir.Expr(ret, Op.REM, l, r));
					}
				}
			}
		}
		return ret;
	}
}