package ast;

import Visitor.Visitor;

public abstract class Type extends AST{

    public boolean Lvalue = false;

    public abstract boolean equal(Type rhs);

    public abstract void accept(Visitor visitor);
}