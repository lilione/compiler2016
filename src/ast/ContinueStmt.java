package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Jump;
import ir.Label;
import ir.LinearIR;

public class ContinueStmt extends Stmt {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        if (!repeat) {
            System.out.println("Wrong ContinueStm" + this.info.toString());
            System.exit(1);
        }
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        linearIR.list.add(new Jump(up));
        return null;
    }
}