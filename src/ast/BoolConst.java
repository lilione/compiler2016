package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

import java.math.BigInteger;

public class BoolConst extends ConstExpr {
	public boolean value;
	
	public BoolConst(boolean value) {
		this.value = value;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		return new BoolType();
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		if (value) return new ir.IntConst(BigInteger.ONE);
		else return new ir.IntConst(BigInteger.ZERO);
	}
}