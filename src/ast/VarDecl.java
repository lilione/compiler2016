package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;
import ir.Register;
import org.antlr.v4.runtime.misc.Pair;

public class VarDecl extends Decl {
	public Type type;
	public Symbol name;
	public Expr init;

	public VarDecl(Type type) {
		this.type = type;
	}
	public VarDecl(Type type, Symbol name) {
		this.type = type;
		this.name = name;
	}
	public VarDecl(Type type, Symbol name, Expr init) {
		this.type = type;
		this.name = name;
		this.init = init;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		type.round3(table, scope, repeat, returnType);
		Pair<Integer, AST> now = (Pair<Integer, AST>) table.get(name);
		if (now != null) {
			if (now.a == scope || !(now.b instanceof Type)) {
				System.out.println("Redefined Var!" + this.info.toString());
				System.exit(1);
			}
		}
		table.put(name, new Pair<>(scope, type));
		if (init != null) {
			Type tmp = (Type) init.round3(table, scope, repeat, returnType);
			if (tmp instanceof NullType) {
				if (type instanceof BasicType && !(type instanceof ClassType)) {
					System.out.println("Wrong VarDecl!" + this.info.toString());
					System.exit(1);
				}
			}
			else if (!type.equal(tmp)) {
				System.out.println("Wrong VarDecl!" + this.info.toString());
				System.exit(1);
			}
		}
		return null;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Address ret = new Register();
		pos.put(name, ret);
		table.put(name, new Pair<>(scope, type));
		if (init != null) {
			linearIR.list.add(new ir.Move(ret, init.translate(pos, table, scope, linearIR, up, down)));
		}
		return ret;
	}

	public Address global(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Address ret = new Register(name.toString());
		linearIR.globalList.add((Register)ret);
		pos.put(name, ret);
		table.put(name, new Pair<>(scope, type));
		if (init != null) {
			linearIR.list.add(new ir.Move(ret, init.translate(pos, table, scope, linearIR, up, down)));
		}
		return ret;
	}
}