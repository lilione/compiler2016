package ast;

import Visitor.Visitor;

public abstract class ConstExpr extends PrmExpr {
    public abstract void accept(Visitor visitor);
}
