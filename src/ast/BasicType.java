package ast;

import Visitor.Visitor;

public abstract class BasicType extends Type {
    public abstract void accept(Visitor visitor);
}