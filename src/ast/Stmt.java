package ast;

import Visitor.Visitor;

public abstract class Stmt extends AST {
    public abstract void accept(Visitor visitor);

}