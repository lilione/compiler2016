package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

import java.math.BigInteger;

public class NullConst extends ConstExpr {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public AST round3(Table table, int scope, boolean repeat, AST returnType){
        return new NullType();
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        return new ir.IntConst(BigInteger.ZERO);
    }
}