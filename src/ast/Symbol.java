package ast;

import Visitor.Visitor;

public class Symbol extends AST{
    private String name;

    private Symbol(String name) {
        this.name = name;
    }

    private static java.util.Map<String, Symbol> dict = new java.util.HashMap<String, Symbol>();
    public static Symbol get(String s) {
        String t = s.intern();
        Symbol ret = dict.get(t);
        if (ret == null) {
            ret = new Symbol(t);
            dict.put(t, ret);
        }
        return ret;
    }
    public String toString() {
        return name;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
