package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

public abstract class Expr extends Stmt {
    public abstract void accept(Visitor visitor);

    public Address getAddress(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {return null;}
}