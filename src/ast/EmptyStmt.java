package ast;

import Visitor.Visitor;

public class EmptyStmt extends Expr {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        return null;
    }
}