package ast;

import Visitor.Visitor;
import ir.Address;
import ir.Label;
import ir.LinearIR;

public class VarDeclStmt extends Stmt {
	public VarDecl varDecl;
	
	public VarDeclStmt(VarDecl varDecl) {
		this.varDecl = varDecl;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		return varDecl.round3(table, scope, repeat, returnType);
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		varDecl.translate(pos, table, scope, linearIR, up, down);
		return null;
	}
}