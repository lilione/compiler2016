package ast;

import Visitor.Visitor;
import ir.*;

public class IfStmt extends Stmt {
	public Expr condition;
	public Stmt thenStmt;
	public Stmt elseStmt;

	public IfStmt(Expr condition, Stmt thenStmt, Stmt elseStmt) {
		this.condition = condition;
		this.thenStmt = thenStmt;
		this.elseStmt = elseStmt;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		if (!(condition.round3(table, scope, repeat, returnType) instanceof BoolType)) {
			System.out.println("Wrong IfStmt!" + this.info.toString());
			System.exit(1);
		}
		if (thenStmt != null) {
			table.beginScope(); scope++;
			thenStmt.round3(table, scope, repeat, returnType);
			scope--; table.endScope();
		}
		if (elseStmt != null) {
			table.beginScope(); scope++;
			elseStmt.round3(table, scope, repeat, returnType);
			scope--; table.endScope();
		}
		return null;
	}

	public boolean check(AST now) {
		if (now instanceof BinaryExpr) {
			if (((BinaryExpr)now).op == BinaryOp.OROR) return false;
			if (!check(((BinaryExpr) now).lvalue)) return false;
			if (!check(((BinaryExpr) now).rvalue)) return false;
		}
		return true;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		Label l1 = new Label(), l2 = new Label(), l3 = new Label();
		if (condition instanceof BinaryExpr && check(condition)) {
			//((BinaryExpr)condition).shortroad(pos, table, scope, linearIR, l1, l2);
			//linearIR.list.add(new Jump(l1));
			linearIR.list.add(new Branch(((BinaryExpr)condition).shortroad(pos, table, scope, linearIR, l1, l2), l1, l2));
		}
		else {
			linearIR.list.add(new Branch(condition.translate(pos, table, scope, linearIR, up, down), l1, l2));
		}
		linearIR.list.add(l1);
		if (thenStmt != null) {
			pos.beginScope();
			table.beginScope(); scope++;
			thenStmt.translate(pos, table, scope, linearIR, up, down);
			scope--; table.endScope();
			pos.endScope();
			linearIR.list.add(new Jump(l3));
		}
		linearIR.list.add(l2);
		if (elseStmt != null) {
			pos.beginScope();
			table.beginScope(); scope++;
			elseStmt.translate(pos, table, scope, linearIR, up, down);
			scope--; table.endScope();
			pos.endScope();
			linearIR.list.add(new Jump(l3));
		}
		linearIR.list.add(l3);
		return null;
	}
}
