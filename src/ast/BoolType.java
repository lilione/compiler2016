package ast;

import Visitor.Visitor;

public class BoolType extends BasicType {

    public boolean equal(Type now) {
        if (now instanceof BoolType) return true;
        return false;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        return new BoolType();
    }
}