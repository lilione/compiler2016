package ast;

import Visitor.Visitor;
import ir.*;
import org.antlr.v4.runtime.misc.Pair;

import java.util.LinkedList;

public class FuncInvoke extends PrmExpr {
    public Symbol name;
    public CompoundStmt para;
    public LinkedList<Address> arg = new LinkedList<>();
    public AST first;

    public FuncInvoke(Symbol name) {
        this.name = name;
    }

    public FuncInvoke(Symbol name, CompoundStmt para) {
        this.name = name;
        this.para = para;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public AST round3(Table table, int scope, boolean repeat, AST returnType) {
        Pair<Integer, AST> tmp = (Pair<Integer, AST>) table.get(name);
        if (tmp != null) {
            if (tmp.b instanceof FuncDecl) {
                if (((FuncDecl) tmp.b).equal(this, table, scope, repeat, returnType)) {
                    if (para != null) {
                        first = para.stmtList.getFirst().round3(table, scope, repeat, returnType);
                    }
                    return ((FuncDecl) tmp.b).type;
                }
            }
        }
        System.out.println("Undefined Function!" + this.info.toString());
        System.exit(1);
        return null;
    }

    @Override
    public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
        Pair<Integer, AST> now = (Pair<Integer, AST>) table.get(name);
        Address ret = null;
        if (!(((FuncDecl)now.b).type instanceof VoidType)) {
            ret = new Register();
        }
        if (name.toString() == "print") {
            AST tmp = para.stmtList.get(0);
            LinkedList list = new LinkedList();
            while (tmp instanceof BinaryExpr && ((BinaryExpr)tmp).op == BinaryOp.PLUS) {
                if (((BinaryExpr) tmp).rvalue instanceof FuncInvoke && ((FuncInvoke) (((BinaryExpr) tmp).rvalue)).name.toString() == "toString") {
                    list.addFirst(new BuiltInCall(ret, "printInt", ((FuncInvoke) (((BinaryExpr) tmp).rvalue)).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
                }
                else {
                    list.addFirst(new BuiltInCall(ret, "print", ((BinaryExpr) tmp).rvalue.translate(pos, table, scope, linearIR, up, down)));
                }
                tmp = ((BinaryExpr) tmp).lvalue;
            }
            if (tmp instanceof FuncInvoke && ((FuncInvoke) tmp).name.toString() == "toString") {
                list.addFirst(new BuiltInCall(ret, "printInt", ((FuncInvoke) tmp).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
            }
            else {
                list.addFirst(new BuiltInCall(ret, "print", tmp.translate(pos, table, scope, linearIR, up, down)));
            }
            for (int i = 0; i < list.size(); i++) linearIR.list.add((Quadruple)list.get(i));
        }
        else if (name.toString() == "println") {
            boolean ok = true;
            AST tmp = para.stmtList.get(0);
            LinkedList list = new LinkedList();
            while (tmp instanceof BinaryExpr && ((BinaryExpr)tmp).op == BinaryOp.PLUS) {
                if (((BinaryExpr) tmp).rvalue instanceof FuncInvoke && ((FuncInvoke) (((BinaryExpr) tmp).rvalue)).name.toString() == "toString") {
                    if (ok) {
                        ok = false;
                        list.addFirst(new BuiltInCall(ret, "printlnInt", ((FuncInvoke) (((BinaryExpr) tmp).rvalue)).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
                    }
                    else {
                        list.addFirst(new BuiltInCall(ret, "printInt", ((FuncInvoke) (((BinaryExpr) tmp).rvalue)).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
                    }
                }
                else {
                    if (ok) {
                        ok = false;
                        list.addFirst(new BuiltInCall(ret, "println", ((BinaryExpr) tmp).rvalue.translate(pos, table, scope, linearIR, up, down)));
                    }
                    else {
                        list.addFirst(new BuiltInCall(ret, "print", ((BinaryExpr) tmp).rvalue.translate(pos, table, scope, linearIR, up, down)));
                    }
                }
                tmp = ((BinaryExpr) tmp).lvalue;
            }
            if (tmp instanceof FuncInvoke && ((FuncInvoke) tmp).name.toString() == "toString") {
                if (ok) {
                    list.addFirst(new BuiltInCall(ret, "printlnInt", ((FuncInvoke) tmp).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
                }
                else {
                    list.addFirst(new BuiltInCall(ret, "printInt", ((FuncInvoke) tmp).para.stmtList.get(0).translate(pos, table, scope, linearIR, up, down)));
                }
            }
            else {
                if (ok) {
                    list.addFirst(new BuiltInCall(ret, "println", tmp.translate(pos, table, scope, linearIR, up, down)));
                }
                else {
                    list.addFirst(new BuiltInCall(ret, "print", tmp.translate(pos, table, scope, linearIR, up, down)));
                }
            }
            for (int i = 0; i < list.size(); i++) linearIR.list.add((Quadruple)list.get(i));
        }
        else {
            if (para != null) {
                for (int i = 0; i < para.stmtList.size(); i++) {
                    arg.add(para.stmtList.get(i).translate(pos, table, scope, linearIR, up, down));
                }
            }
            /*if (name.toString() == "print" || name.toString() == "println") {
                linearIR.list.add(new BuiltInCall(ret, name.toString(), arg));
            }*/
            if (name.toString() == "getString" || name.toString() == "getInt" || name.toString() == "toString") {
                linearIR.list.add(new BuiltInCall(ret, name.toString(), arg));
            }
        /*else if (name.toString() == "println") {
            linearIR.list.add(new BuiltInCall(ret, "print", arg));
            Register tmp = new Register();
            linearIR.list.add(new Move(tmp, StringAddress.newLine));
            linearIR.list.add(new BuiltInCall(ret, "print", tmp));
        }*/
            else {
                linearIR.list.add(new Call(ret, ((FuncDecl) now.b).callee, arg));
            }
        }
        return ret;
    }
}
