package ast;

public enum PrefixOp {
    SELFPLUS, SELFMINUS, NOT, TILDE, MINUS
}
