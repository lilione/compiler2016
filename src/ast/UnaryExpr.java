package ast;

import Visitor.Visitor;

public abstract class UnaryExpr extends Expr {
    public abstract void accept(Visitor visitor);
}
