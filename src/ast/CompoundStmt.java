package ast;

import Visitor.Visitor;
import ir.*;

import java.math.BigInteger;
import java.util.LinkedList;

public class CompoundStmt extends Stmt {
	public LinkedList<Stmt> stmtList;
	
	public CompoundStmt() {
		this.stmtList = new LinkedList<>();
	}
	
	public CompoundStmt(LinkedList<Stmt> stmtList) {
		this.stmtList = stmtList;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public AST round3(Table table, int scope, boolean repeat, AST returnType) {
		table.beginScope(); scope++;
		for (int i = 0; i < stmtList.size(); i++) stmtList.get(i).round3(table, scope, repeat, returnType);
		scope--; table.endScope();
		return null;
	}

	@Override
	public Address translate(Table pos, Table table, int scope, LinearIR linearIR, Label up, Label down) {
		pos.beginScope();
		table.beginScope(); scope++;
		for (int i = 0; i < stmtList.size(); i++) {
			boolean ok = true;
			if (stmtList.get(i) instanceof UnaryExpr) {
				if (stmtList.get(i) instanceof SelfIncExpr) {
					ok = false;
					Address now = ((SelfIncExpr)stmtList.get(i)).expr.getAddress(pos, table, scope, linearIR, up, down);
					if (now instanceof Register) {
						linearIR.list.add(new ir.Expr(now, Op.ADD, now, new ir.IntConst(BigInteger.ONE)));
					} else {
						Register tmp = new Register();
						linearIR.list.add(new MemoryRead(tmp, ((Memory) now).mem, ((Memory) now).offset));
						linearIR.list.add(new ir.Expr(tmp, Op.ADD, tmp, new ir.IntConst(BigInteger.ONE)));
						linearIR.list.add(new MemoryWrite(((Memory) now).mem, tmp, ((Memory) now).offset));
					}
				}
				else if (stmtList.get(i) instanceof SelfDecExpr) {
					ok = false;
					Address now = ((SelfDecExpr)stmtList.get(i)).expr.getAddress(pos, table, scope, linearIR, up, down);
					if (now instanceof Register) {
						linearIR.list.add(new ir.Expr(now, Op.SUB, now, new ir.IntConst(BigInteger.ONE)));
					} else {
						Register tmp = new Register();
						linearIR.list.add(new MemoryRead(tmp, ((Memory) now).mem, ((Memory) now).offset));
						linearIR.list.add(new ir.Expr(tmp, Op.SUB, tmp, new ir.IntConst(BigInteger.ONE)));
						linearIR.list.add(new MemoryWrite(((Memory) now).mem, tmp, ((Memory) now).offset));
					}
				}
			}
			if (ok) stmtList.get(i).translate(pos, table, scope, linearIR, up, down);
		}
		scope--;  table.endScope();
		pos.endScope();
		return null;
	}
}
