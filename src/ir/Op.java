package ir;

public enum Op {
    ADD, SUB, MUL, DIV, REM, NEG,
    SHL, SHR, AND, XOR, OR, NOT,
    SLT, SGT, SLE, SGE, SEQ, SNE
}
