package ir;

import java.util.LinkedList;

public class BuiltInCall extends Quadruple {
    public Address returnValue;
    public String name;
    public LinkedList<Address> para = new LinkedList<>();

    public BuiltInCall(Address returnValue, String name, LinkedList<Address> para) {
        this.returnValue = returnValue;
        this.name = name;
        this.para = para;
    }

    public BuiltInCall(Address returnValue, String name, Address addr) {
        this.returnValue = returnValue;
        this.name = name;
        this.para = new LinkedList<>();
        para.add(addr);
    }

    @Override
    public String print() {
        String ret = "";
        if (returnValue != null) ret += returnValue.print() + "= ";
        ret += name;
        for (int i = 0; i < para.size(); i++) ret += " " + para.get(i).print();
        return ret;
    }

    @Override
    public String translate() {
        String ret = "";
        int delta = 32 * 4;
        ret += "subu $sp, $sp, " + delta + "\n";
        ret += "sw $ra, " + (delta - 4) + "($sp)\n";
        for (int i = 0; i < para.size(); i++) {
            if (para.get(i) instanceof IntConst) {
                ret += "li $a" + i + ", " + ((IntConst) para.get(i)).value + "\n";
            }
            else {
                if (((Register)para.get(i)).name != null) {
                    ret += "lw $a" + i + ", var_" + ((Register)para.get(i)).name + "\n";
                }
                else {
                    ret += "lw $a" + i + ", " + (((Register) para.get(i)).num * 4 + delta) + "($sp)\n";
                }
            }
        }
        ret += "jal " + name + "\n";
        ret += "lw $ra, " + (delta - 4) + "($sp)\n";
        ret += "addu $sp, $sp, " + delta + "\n";
        if (returnValue != null) {
            if (((Register)returnValue).name != null) {
                ret += "sw $v0, var_" + ((Register)returnValue).name + "\n";
            }
            else {
                ret += "sw $v0, " + ((Register) returnValue).num * 4 + "($sp)\n";
            }
        }
        return ret;
    }
}
