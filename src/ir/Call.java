package ir;

import java.util.LinkedList;

public class Call extends Quadruple{
    public Address returnValue;
    public Function callee, caller;
    public LinkedList<Address> para;

    public Call(Address returnValue, Function callee, LinkedList para) {
        this.returnValue = returnValue;
        this.callee = callee;
        this.para = para;
    }

    @Override
    public String print() {
        String ret = "";
        if (returnValue != null) ret = ret + returnValue.print() + " = ";
        ret = ret + "call" + " " + callee.name;
        for (int i = 0; i < para.size(); i++) {
            ret = ret + " " + para.get(i).print();
        }
        return ret;
    }

    @Override
    public String translate() {
        String ret = "", now1 = "", now2 = "";
        int delta = (callee.size + 1) * 4;
        ret += "subu $sp, $sp, " + delta + "\n";
        if (para != null) {
            for (int i = 0; i < para.size(); i++) {
                /*if (para.get(i) instanceof IntConst) {
                    ret += "li $a0, " + ((IntConst)para.get(i)).value + "\n";
                    now1 = "$a0";
                }
                else {
                    Pair<String, String> tmp = ((Register)para.get(i)).get("w");
                    ret += tmp.b;
                    now1 = tmp.a;
                }
                {
                    Pair<String, String> tmp = ((Register)callee.para.get(i)).get("w");
                    ret += tmp.b;
                    now2 = tmp.a;
                }
                ret += "move " + now2 + ", " + now1 + "\n";*/
                if (para.get(i) instanceof IntConst) {
                    ret += "li $a0, " + ((IntConst)para.get(i)).value + "\n";
                }
                else {
                    if (((Register)para.get(i)).name != null) {
                        ret += "lw $a0, var_" + ((Register)para.get(i)).name + "\n";
                    }
                    else {
                        ret += "lw $a0, " + (((Register)para.get(i)).num * 4 + delta) + "($sp)\n";
                    }
                }
                if (((Register)callee.para.get(i)).name != null) {
                    ret += "sw $a0, var_" + ((Register)callee.para.get(i)).name + "\n";
                }
                else {
                    ret += "sw $a0, " + ((Register)callee.para.get(i)).num * 4 + "($sp)\n";
                }
            }
        }
        ret += "sw $ra, " + callee.size * 4 + "($sp)\n";
        ret += "jal " + callee.label.trans() + "\n";
        ret += "lw $ra, " + callee.size * 4 + "($sp)\n";
        ret += "addu $sp, $sp, " + delta + "\n";
        if (returnValue != null) {
            /*Pair<String, String> tmp = ((Register)returnValue).get("w");
            ret += tmp.b;
            ret += "move " + tmp.a + ", $v0\n";*/
            if (((Register)returnValue).name != null) {
                ret += "sw $v0, var_" + ((Register)returnValue).name + "\n";
            }
            else {
                ret += "sw $v0, " + ((Register)returnValue).num * 4 + "($sp)\n";
            }
        }
        return ret;
    }
}
