package ir;

public class Move extends Quadruple{
    public Address dest, src;

    public Move (Address dest, Address src) {
        this.dest = dest;
        this.src = src;
    }

    @Override
    public String print() {
        return dest.print() + " = " + "move" + " " + src.print();
    }

    @Override
    public String translate() {
        String ret = "";
        if (src instanceof StringAddress) {
            ret += "la $a0, str_" +  ((StringAddress) src).num + "\n";
        }
        else if (src instanceof IntConst) {
            ret += "li $a0, " + ((IntConst) src).value + "\n";
        }
        else {
            if (((Register)src).name != null) {
                ret += "lw $a0, var_" + ((Register) src).name + "\n";
            }
            else {
                ret += "lw $a0, " + ((Register) src).num * 4 + "($sp)\n";
            }
        }
        if (((Register)dest).name != null) {
            ret += "sw $a0, var_" + ((Register) dest).name + "\n";
        }
        else {
            ret += "sw $a0, " + ((Register) dest).num * 4 + "($sp)\n";
        }
        return ret;
    }
}
