package ir;

public class Allocate extends Quadruple{
    public Address des, size;

    public Allocate(Address des, Address size) {
        this.des = des;
        this.size = size;
    }

    @Override
    public String print() {
        return des.print() + " = alloc " + size.print();
    }

    @Override
    public String translate() {
        String ret = "";
        ret += "li $v0, 9\n";
        if (size instanceof IntConst) {
            ret += "li $a0, " + ((IntConst) size).value + "\n";
        }
        else {
            if (((Register)size).name != null) {
                ret += "lw $a0, var_" + ((Register) size).name + "\n";
            }
            else {
                ret += "lw $a0, " + ((Register)size).num * 4 + "($sp)\n";
            }
        }
        ret += "syscall\n";
        if (((Register)des).name != null) {
            ret += "sw $v0, var_" + ((Register) des).name + "\n";
        }
        else {
            ret += "sw $v0, " + ((Register) des).num * 4 + "($sp)\n";
        }
        return ret;
    }
}
