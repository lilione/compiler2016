package ir;

import org.antlr.v4.runtime.misc.Pair;

public class Register extends Address{
    private static int RegisterNum = 0;

    public int num;
    public String name;

    public Register() {
        this.num = RegisterNum++;
        this.name = null;
    }

    public Register(String name) {
        this.num = RegisterNum++;
        this.name = name;
    }

    public static void RegisterClear() {
        RegisterNum = 0;
    }

    public static int getRegisterNum() {
        return RegisterNum;
    }

    @Override
    public String print() {
        return "$" + num;
    }

    public String translate() {
        String ret = "";
        ret += "var_" + name + ":\n";
        ret += "\t.word 0\n";
        return ret;
    }

    public static Register[] used = new Register[30];
    public static int[] time = new int[30];
    public static String[] ope = new String[30];

    public static String getName(int i) {
        if (i < 10) {
            return "$t" + i;
        }
        else {
            return "$s" + (i - 10);
        }
    }

    public static String clear() {
        String ret = "";
        for (int i = 0; i < 18; i++) {
            if (time[i] != 0 && used[i].name != null) {
                ret += "la $a0, var_" + used[i].name + "\n";
                ret += "sw " + getName(i) + ", ($a0)\n";
            }
            used[i] = null;
            time[i] = 0;
            ope[i] = "";
        }
        return ret;
    }

    public Pair<String, String> get(String op) {
        String ret = "";
        for (int i = 0; i < 18; i++) {
            if (used[i] == this) return new Pair<>(getName(i), ret);
        }
        int pos = -1;
        for (int i = 0; i < 18; i++) {
            if (time[i] == 0) {
                pos = i;
                break;
            }
        }
        if (pos == -1) {
            int Max = -1;
            for (int i = 0; i < 18; i++) {
                if (time[i] > Max) {
                    Max = time[i];
                    pos = i;
                }
            }
        }
        if (time[pos] != 0) {
            if (used[pos].name != null) {
                ret += "la $a0, var_" + used[pos].name + "\n";
                ret += "s" + ope[pos] + " " + getName(pos) + ", ($a0)\n";
            }
            else {
                ret += "s" + ope[pos] + " " + getName(pos) + ", " + used[pos].num * 4 + "($sp)\n";
            }
        }
        if (name != null) {
            ret += "la $a0, var_" + name + "\n";
            ret += "l" + op + " " + getName(pos) + ", ($a0)\n";
        }
        else {
            ret += "l" + op + " " + getName(pos) + ", " + num * 4 + "($sp)\n";

        }
        used[pos] = this; time[pos] = 1; ope[pos] = op;
        for (int i = 0; i < 18; i++) {
            if (time[i] != 0) time[i]++;
        }
        return new Pair<>(getName(pos), ret);
    }
}
