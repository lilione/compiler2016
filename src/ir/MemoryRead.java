package ir;

import java.math.BigInteger;

public class MemoryRead extends Quadruple{
    public Address des, src;
    public IntConst offset;
    public int size = 4;

    public MemoryRead(Address des, Address src, IntConst offset) {
        this.des = des;
        this.src = src;
        this.offset = offset;
    }

    public MemoryRead(Address des, Address src, IntConst offset, int size) {
        this.des = des;
        this.src = src;
        this.offset = offset;
        this.size = size;
    }

    @Override
    public String print() {
        return des.print() + " = load " + src.print() + " " + offset.value;
    }

    @Override
    public String translate() {
        String ret = "", op;//, op = "", now1 = "", now2 = "";
        if (size == 4) op = "w";
        else if (size == 2) op = "h";
        else op = "b";
        /*{
            Pair<String, String> tmp = ((Register)src).get(op);
            ret += tmp.b;
            ret += "add $a0, " + tmp.a + ", " + offset.value.intValue() + "\n";
            now1 = "$a0";
        }
        {
            Pair<String, String> tmp = ((Register)des).get(op);
            ret += tmp.b;
            now2 = tmp.a;
        }
        ret += "l" + op + " " + now2 + ", (" + now1 + ")\n";*/
        if (((Register)src).name != null) {
            if (offset.value != BigInteger.ZERO) {
                ret += "lw $a2, var_" + ((Register) src).name + "\n";
                ret += "add $a1, $a2, " + offset.value.intValue() + "\n";
            }
            else {
                ret += "lw $a1, var_" + ((Register) src).name + "\n";
            }
            ret += "l" + op + " $a0, ($a1)\n";
        }
        else {
            if (offset.value != BigInteger.ZERO) {
                ret += "lw $a2, " + ((Register) src).num * 4 + "($sp)\n";
                ret += "add $a1, $a2, " + offset.value.intValue() + "\n";
            }
            else {
                ret += "lw $a1, " + ((Register) src).num * 4 + "($sp)\n";
            }
            ret += "l" + op + " $a0, ($a1)\n";
        }
        if (((Register)des).name != null) {
            ret += "sw $a0, var_" + ((Register) des).name + "\n";
        }
        else {
            ret += "sw  $a0, " + ((Register) des).num * 4 + "($sp)\n";
        }
        return ret;
    }
}
