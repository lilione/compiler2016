package ir;

public abstract class Quadruple {
    abstract public String print();

    abstract public String translate();
}
