package ir;

public class Branch extends Quadruple{
    public Address target;
    public Label ifTrue, ifFalse;

    public Branch (Address target, Label ifTrue, Label ifFalse) {
        this.target = target;
        this.ifTrue = ifTrue;
        this.ifFalse = ifFalse;
    }

    @Override
    public String print() {
        return "br " + target.print() + " " + ifTrue.print() + " " + ifFalse.print();
    }

    @Override
    public String translate() {
        String ret = "";
        String now = "$a0";
        if (target instanceof IntConst) {
            ret += "li $a0, " + ((IntConst)target).value + "\n";
        }
        else {
            if (((Register)target).name != null) {
                ret += "lw $a0, var_" + ((Register) target).name + "\n";
            }
            else {
                ret += "lw $a0, " + ((Register) target).num * 4 + "($sp)\n";
            }
        }
        if (ifTrue.num > ifFalse.num) ret = ret + "beq " + now + ", 1, " + ifTrue.trans() + "\n";
        ret = ret + "bne " + now + ", 1, " + ifFalse.trans() + "\n";
        return ret;
    }
}
