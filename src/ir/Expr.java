package ir;

public class Expr extends Quadruple{
    public Op op;
    public Address dest, src1, src2;

    public Expr(Address dest, Op op, Address src1, Address src2) {
        this.dest = dest;
        this.op = op;
        this.src1 = src1;
        this.src2 = src2;
    }

    @Override
    public String print() {
        String ret = dest.print() + " " + "=" + " ";
        switch (op) {
            case NEG: ret = ret + "neg"; break;
            case ADD: ret = ret + "add"; break;
            case SUB: ret = ret + "sub"; break;
            case MUL: ret = ret + "mul"; break;
            case DIV: ret = ret + "div"; break;
            case REM: ret = ret + "rem"; break;

            case SHL: ret = ret + "shl"; break;
            case SHR: ret = ret + "shr"; break;
            case AND: ret = ret + "and"; break;
            case XOR: ret = ret + "xor"; break;
            case OR: ret = ret + "or"; break;
            case NOT: ret = ret + "not"; break;

            case SLT: ret = ret + "slt"; break;
            case SGT: ret = ret + "sgt"; break;
            case SLE: ret = ret + "sle"; break;
            case SGE: ret = ret + "sge"; break;
            case SEQ: ret = ret + "seq"; break;
            case SNE: ret = ret + "sne"; break;
        }
        ret = ret + " " + src1.print();
        if (src2 != null) {
            ret = ret + " " + src2.print();
        }
        return ret;
    }

    @Override
    public String translate() {
        String ret = "";
        if (((Register)src1).name != null) {
            ret += "lw $a0, var_" + ((Register)src1).name + "\n";
        }
        else {
            ret += "lw $a0, " + ((Register)src1).num * 4 + "($sp)\n";
        }
        if (src2 != null) {
            if (src2 instanceof IntConst) {
                ret += "li $a1, " + ((IntConst) src2).value + "\n";
            }
            else if (((Register)src2).name != null) {
                ret += "lw $a1, var_" + ((Register)src2).name + "\n";
            }
            else {
                ret += "lw $a1, " + ((Register)src2).num * 4 + "($sp)\n";
            }
        }
        switch (op) {
            case NEG: ret = ret + "neg"; break;
            case ADD: ret = ret + "add"; break;
            case SUB: ret = ret + "sub"; break;
            case MUL: ret = ret + "mul"; break;
            case DIV: ret = ret + "div"; break;
            case REM: ret = ret + "rem"; break;

            case SHL: ret = ret + "shl"; break;
            case SHR: ret = ret + "shr"; break;
            case AND: ret = ret + "and"; break;
            case XOR: ret = ret + "xor"; break;
            case OR: ret = ret + "or"; break;
            case NOT: ret = ret + "not"; break;

            case SLT: ret = ret + "slt"; break;
            case SGT: ret = ret + "sgt"; break;
            case SLE: ret = ret + "sle"; break;
            case SGE: ret = ret + "sge"; break;
            case SEQ: ret = ret + "seq"; break;
            case SNE: ret = ret + "sne"; break;
        }
        ret += " $a2, $a0";
        if (src2 != null) {
            ret += ", $a1";
        }
        ret += "\n";
        if (((Register)dest).name != null) {
            ret += "sw $a2, var_" + ((Register)dest).name + "\n";
        }
        else {
            ret += "sw $a2, " + ((Register)dest).num * 4 + "($sp)\n";
        }
        return ret;
    }
}
