package ir;

public class Return extends Quadruple {
    public Address src;

    public Return (Address src) {
        this.src = src;
    }

    @Override
    public String print() {
        String ret = "ret";
        if (src != null) ret = ret + " " + src.print();
        return ret;
    }

    @Override
    public String translate() {
        String ret = "";
        if (src != null) {
            if (src instanceof IntConst) {
                ret += "li $v0, " + ((IntConst) src).value + "\n";
            } else {
            /*Pair<String, String> tmp = ((Register)src).get("w");
            ret += tmp.b;
            ret += "move $v0, " + tmp.a + "\n";*/
                if (((Register) src).name != null) {
                    ret += "lw $v0, var_" + ((Register) src).name + "\n";
                } else {
                    ret += "lw $v0, " + ((Register) src).num * 4 + "($sp)\n";
                }
            }
        }
        ret += "jr $ra\n";
        return ret;
    }
}
