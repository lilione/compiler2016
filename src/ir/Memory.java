package ir;

public class Memory extends Address{
    public Address mem;
    public IntConst offset;

    public Memory(Address mem, IntConst offset) {
        this.mem = mem;
        this.offset = offset;
    }

    @Override
    public String print() {
        return "";
    }
}
