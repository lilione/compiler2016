package ir;

import java.util.LinkedList;

public class LinearIR {
    public LinkedList<Quadruple> list;
    public LinkedList<Register> globalList;
    public LinkedList<StringAddress> stringList;
    public LinkedList<Quadruple> init;
    public int size, mainsize;

    public LinearIR() {
        this.list = new LinkedList<>();
        this.globalList = new LinkedList<>();
        this.stringList = new LinkedList<>();
        this.init = new LinkedList<>();
    }

    public String print() {
        String ret = "";
        for (int i = 0; i < init.size(); i++) {
            ret += init.get(i).print() + "\n";
        }
        for (int i = 0; i < list.size(); i++) {
            ret = ret + list.get(i).print() + "\n";
        }
        return ret;
    }

    public String built_in() {
        String ret = "";

        //printInt
        ret += 	"printInt:\n" +
                "li $v0, 1\n" +
                "syscall\n" +
                "jr $ra\n";

        //printlnInt
        ret += 	"printlnInt:\n" +
                "li $v0, 1\n" +
                "syscall\n" +
                "li $v0, 4\n" +
                "la $a0, str_0\n" +
                "add $a0, $a0, 4\n" +
                "syscall\n" +
                "jr $ra\n";

        //print
        ret += "print:\n" +
                "li\t$v0, 4\n" +
                "addu\t$a0, $a0, 4\n" +
                "syscall\n" +
                "jr\t$ra\n";

        //println
        ret += "println:\n" +
                "add $a0, $a0, 4\n" +
                "li $v0, 4\n" +
                "syscall\n" +
                "la $a0, str_0\n" +
                "add $a0, $a0, 4\n" +
                "syscall\n" +
                "jr $ra\n";

        //getString
        ret += "getString:\n" +
                "li\t$v0, 9\n" +
                "li\t$a0, 8192\n" +
                "syscall\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a1, 8188\n" +
                "li\t$v0, 8\n" +
                "syscall\n" +
                "subu\t$v0, $a0, 4\n" +
                "getString_label:\n" +
                "lb\t$a1, 0($a0)\n" +
                "addu\t$a0, $a0, 1\n" +
                "bnez\t$a1, getString_label\n" +
                "subu\t$a0, $a0, $v0\n" +
                "subu\t$a0, $a0, 5\n" +
                "sw\t$a0, 0($v0)\n" +
                "jr\t$ra\n";

        //getInt
        ret += "getInt:\n" +
                "li\t$a1, 0\n" +
                "getInt_label1:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "beq\t$v0, 45, getInt_label2\n" +
                "bge\t$v0, 48, getInt_label3\n" +
                "j\tgetInt_label1\n" +
                "getInt_label2:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "li\t$a1, 1\n" +
                "getInt_label3:\n" +
                "sub\t$a0, $v0, 48\n" +
                "getInt_label6:\n" +
                "li\t$v0, 12\n" +
                "syscall\n" +
                "blt\t$v0, 48, getInt_label4\n" +
                "sub\t$v0, $v0, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $v0\n" +
                "j getInt_label6\n" +
                "getInt_label4:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, getInt_label5\n" +
                "neg\t$v0, $v0\n" +
                "getInt_label5:\n" +
                "jr\t$ra\n";

        //toString
        ret += "toString:\n" +
                "move\t$a1, $a0\n" +
                "li\t$a2, 0\n" +
                "toString_label1:\n" +
                "div\t$a1, $a1, 10\n" +
                "addu\t$a2, $a2, 1\n" +
                "bnez\t$a1, toString_label1\n" +
                "move\t$a1, $a0\n" +
                "move\t$a0, $a2\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "bltz\t$a1, toString_label2\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a0, $a0, $a2\n" +
                "j\ttoString_label3\n" +
                "toString_label2:\n" +
                "abs\t$a1, $a1\n" +
                "addu\t$a2, $a2, 1\n" +
                "sw\t$a2, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "li\t$a3, 45\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a0, $a0, $a2\n" +
                "toString_label3:\n" +
                "li\t$a2, 0\n" +
                "sb\t$a2, 0($a0)\n" +
                "toString_label4:\n" +
                "subu\t$a0, $a0, 1\n" +
                "rem\t$a3, $a1, 10\n" +
                "addu $a3, $a3, 48\n" +
                "sb\t$a3, 0($a0)\n" +
                "div\t$a1, $a1, 10\n" +
                "bnez\t$a1, toString_label4\n" +
                "jr\t$ra\n";
        //substring
        ret += "substring:\n" +
                "addu\t$a1, $a1, 4\n" +
                "addu\t$a1, $a1, $a0\n" +
                "addu\t$a2, $a2, 4\n" +
                "addu\t$a2, $a2, $a0\n" +
                "li\t$v0, 9\n" +
                "subu\t$a0, $a2, $a1\n" +
                "addu\t$a0, $a0, 9\n" +
                "divu\t$a0, $a0, 4\n" +
                "mulou\t$a0, $a0, 4\n" +
                "syscall\n" +
                "subu\t$a3, $a2, $a1\n" +
                "addu\t$a3, $a3, 1\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "substring_label1:\n" +
                "bgt\t$a1, $a2, substring_label2\n" +
                "lb\t$a3, 0($a1)\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j substring_label1\n" +
                "substring_label2:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "jr\t$ra\n";

        //parseInt
        ret += "parseInt:\n" +
                "li\t$a1, 0\n" +
                "addu\t$a2, $a0, 4\n" +
                "lb\t$a3, 0($a2)\n" +
                "bge\t$a3, 48, parseInt_label1\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "li\t$a1, 1\n" +
                "parseInt_label1:\n" +
                "sub\t$a0, $a3, 48\n" +
                "parseInt_label2:\n" +
                "addu\t$a2, $a2, 1\n" +
                "lb\t$a3, 0($a2)\n" +
                "blt\t$a3, 48, parseInt_label3\n" +
                "bgt\t$a3, 57, parseInt_label3\n" +
                "sub\t$a3, $a3, 48\n" +
                "mul\t$a0, $a0, 10\n" +
                "add\t$a0, $a0, $a3\n" +
                "j parseInt_label2\n" +
                "parseInt_label3:\n" +
                "move\t$v0, $a0\n" +
                "beq\t$a1, 0, parseInt_label4\n" +
                "neg\t$v0, $v0\n" +
                "parseInt_label4:\n" +
                "jr\t$ra\n";

        //stringAdd
        ret += "stringAdd:\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "add\t$a3, $a2, $a3\n" +
                "move\t$a2, $a0\n" +
                "move\t$a0, $a3\n" +
                "add\t$a0, $a0, 8\n" +
                "div\t$a0, $a0, 4\n" +
                "mul\t$a0, $a0, 4\n" +
                "li\t$v0, 9\n" +
                "syscall\n" +
                "sw\t$a3, 0($v0)\n" +
                "addu\t$a0, $v0, 4\n" +
                "addu\t$a2, $a2, 4\n" +
                "stringAdd_label1:\n" +
                "lb\t$a3, 0($a2)\n" +
                "beqz\t$a3, stringAdd_label2\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a2, $a2, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label1\n" +
                "stringAdd_label2:\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringAdd_label3:\n" +
                "lb\t$a3, 0($a1)\n" +
                "beqz\t$a3, stringAdd_label4\n" +
                "sb\t$a3, 0($a0)\n" +
                "addu\t$a1, $a1, 1\n" +
                "addu\t$a0, $a0, 1\n" +
                "j\tstringAdd_label3\n" +
                "stringAdd_label4:\n" +
                "li\t$a3, 0\n" +
                "sb\t$a3, 0($a0)\n" +
                "jr\t$ra\n";

        //stringEquals
        ret += "stringEquals:\n" +
                "lw\t$a2, 0($a0)\n" +
                "lw\t$a3, 0($a1)\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "bne\t$a2, $a3, stringEquals_neq\n" +
                "beq\t$a2, 0, stringEquals_eq\n" +
                "j\tstringEquals_start\n" +
                "stringEquals_eq:\n" +
                "li\t$v0, 1\n" +
                "j stringEquals_end\n" +
                "stringEquals_neq:\n" +
                "li\t$v0, 0\n" +
                "stringEquals_end:\n" +
                "jr\t$ra\n";

        //stringLessThan
        ret += "stringLessThan:\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThan_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThan_no\n" +
                "beq\t$a2, 0, stringLessThan_yes\n" +
                "beq\t$a3, 0, stringLessThan_no\n" +
                "blt\t$a2, $a3, stringLessThan_yes\n" +
                "bgt\t$a2, $a3, stringLessThan_no\n" +
                "j\tstringLessThan_start\n" +
                "stringLessThan_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThan_end\n" +
                "stringLessThan_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThan_end:\n" +
                "jr\t$ra\n";

        //stringLessThanOrEquals
        ret += "stringLessThanOrEquals:\n" +
                "addu\t$a0, $a0, 4\n" +
                "addu\t$a1, $a1, 4\n" +
                "stringLessThanOrEquals_start:\n" +
                "lb\t$a2, 0($a0)\n" +
                "lb\t$a3, 0($a1)\n" +
                "addu\t$a0, $a0, 1\n" +
                "addu\t$a1, $a1, 1\n" +
                "add\t$v0, $a2, $a3\n" +
                "beq\t$v0, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a2, 0, stringLessThanOrEquals_yes\n" +
                "beq\t$a3, 0, stringLessThanOrEquals_no\n" +
                "blt\t$a2, $a3, stringLessThanOrEquals_yes\n" +
                "bgt\t$a2, $a3, stringLessThanOrEquals_no\n" +
                "j\tstringLessThanOrEquals_start\n" +
                "stringLessThanOrEquals_yes:\n" +
                "li\t$v0, 1\n" +
                "j stringLessThanOrEquals_end\n" +
                "stringLessThanOrEquals_no:\n" +
                "li\t$v0, 0\n" +
                "stringLessThanOrEquals_end:\n" +
                "jr\t$ra\n";

        return ret;
    }

    public String translate() {
        String ret = "";
        ret += ".data\n";

        //GlobalVariable
        for (int i = 0; i < globalList.size(); i++) {
            ret += globalList.get(i).translate();
        }

        //StringAddress
        for (int i = 0; i < stringList.size(); i++) {
            ret += stringList.get(i).translate();
        }

        ret += "\t.text\n";
        ret += "\t.globl main\n";

        ret += "main:\n";
        ret += "subu $sp, $sp, " + (size + 1) * 4 + "\n";
        ret += "sw $ra, " + size * 4 + "($sp)\n";
        for (int i = 0; i < init.size(); i++) {
            ret += init.get(i).translate();
        }

        ret += "subu $sp, $sp, " + (mainsize + 1) * 4 + "\n";
        ret += "sw $ra, " + mainsize * 4 + "($sp)\n";
        ret += "jal main_enter\n";
        ret += "lw $ra, " + mainsize * 4 + "($sp)\n";
        ret += "addu $sp, $sp, " + (mainsize + 1) * 4 + "\n";

        ret += "lw $ra, " + size * 4 + "($sp)\n";
        ret += "addu $sp, $sp, " + (size + 1) * 4 + "\n";
        ret += "jr $ra\n";

        ret += built_in();

        for (int i = 0; i < list.size(); i++) {
            ret += list.get(i).translate();
        }
        return ret;
    }
}
