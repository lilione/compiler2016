package ir;

import java.util.LinkedList;

public class Function extends Quadruple{
    public String name;
    public LinkedList<Address> para;
    public LinkedList<Quadruple> body;
    public int size;
    public Label label;

    public Function(String name, Label label) {
        this.name = name;
        this.para = new LinkedList<>();
        this.body = new LinkedList<>();
        this.size = 0;
        this.label = label;
    }

    public Function(String name, LinkedList<Address> para, LinkedList<Quadruple> body, int size, Label label) {
        this.name = name;
        this.para = para;
        this.body = body;
        this.size = size;
        this.label = label;
    }

    @Override
    public String print() {
        String ret = "func " + name;
        if (para != null) {
            for (int i = 0; i < para.size(); i++) {
                ret += " " + para.get(i).print();
            }
        }
        ret += " " + "{" + "\n";
        if (body != null) {
            for (int i = 0; i < body.size(); i++) {
                if (!(body.get(i) instanceof Label)) ret += "\t";
                ret += body.get(i).print();
                if (body.get(i) instanceof Label) ret += ":";
                ret += "\n";
            }
        }
        ret += "}";
        return ret;
    }

    @Override
    public String translate() {
        String ret = "";
        ret += label.translate();
        ret += Register.clear();
        for (int i = 0; i < body.size(); i++) {
            ret += body.get(i).translate();
        }
        if (!(body.getLast() instanceof Return)) ret += "jr $ra\n";
        return ret;
    }
}
