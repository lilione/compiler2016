package ir;

public class Label extends Quadruple{
    public static int LabelNum = 0;

    public int num;
    public String name;

    public Label () {
        num = LabelNum++;
        name = null;
    }

    public Label(String name) {
        this.name = name;
    }

    @Override
    public String print() {
        return "%" + num;
    }

    @Override
    public String translate() {
        String ret = "";
        ret += trans() + ":\n";
        return ret;
    }

    public String trans() {
        if (name == null) return "l_" + String.valueOf(num);
        else return name;
    }
}
