package ir;

public class StringAddress extends Address {
    private static int RegisterNum = 0;

    public String value, origin;
    public int num;
    public static StringAddress newLine;

    public static void init(LinearIR linearIR) {
        newLine = new StringAddress("\n", "\\n", linearIR);
    }

    public StringAddress(String value, String origin, LinearIR linearIR) {
        this.value = value;
        this.origin = origin;
        this.num = RegisterNum++;
        linearIR.stringList.add(this);
    }

    @Override
    public String print() {
        return value;
    }

    public String translate() {
        String ret = "";
        ret += "str_" + num + ":\n";
        ret += "\t.word " + value.getBytes().length + "\n";
        ret += "\t.align 2\n";
        ret += "\t.asciiz \"" + origin + "\"\n";
        return ret;
    }
}
