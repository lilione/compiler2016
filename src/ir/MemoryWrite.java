package ir;

import java.math.BigInteger;

public class MemoryWrite extends Quadruple{
    public Address des, src;
    public IntConst offset;

    public MemoryWrite(Address des, Address src, IntConst offset) {
        this.des = des;
        this.src = src;
        this.offset = offset;
    }

    @Override
    public String print() {
        return "store " + 4 + " " + des.print() + " " + src.print() + " " + offset.value;
    }

    @Override
    public String translate() {
        String ret = "";
        if (((Register)src).name != null) {
            ret += "lw $a0, var_" + ((Register) src).name + "\n";
        }
        else {
            ret += "lw $a0, " + ((Register) src).num * 4 + "($sp)\n";
        }
        if (((Register)des).name != null) {
            if (offset.value != BigInteger.ZERO) {
                ret += "lw $a2, var_" + ((Register) des).name + "\n";
                ret += "add $a1, $a2, " + offset.value.intValue() + "\n";
            }
            else {
                ret += "lw $a1, var_" + ((Register) des).name + "\n";
            }
            ret += "sw $a0, ($a1)\n";
        }
        else {
            if (offset.value != BigInteger.ZERO) {
                ret += "lw $a2, " + ((Register) des).num * 4 + "($sp)\n";
                ret += "add $a1, $a2, " + offset.value.intValue() + "\n";
            }
            else {
                ret += "lw $a1, " + ((Register) des).num * 4 + "($sp)\n";
            }
            ret += "sw $a0, ($a1)\n";
        }
        return ret;
    }
}
