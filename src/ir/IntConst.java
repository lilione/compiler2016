package ir;

import java.math.BigInteger;

public class IntConst extends Address{
    public BigInteger value;

    public IntConst (BigInteger value) {
        this.value = value;
    }

    @Override
    public String print() {
        return String.valueOf(value);
    }
}
