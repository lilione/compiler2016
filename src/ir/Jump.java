package ir;

public class Jump extends Quadruple{
    public Label target;

    public Jump (Label target) {
        this.target = target;
    }

    @Override
    public String print() {
        return "jump " + target.print();
    }

    @Override
    public String translate() {
        String ret = "b " + target.trans() + "\n";
        return ret;
    }
}
