import java.io.*;
import java.util.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import parser.*;
import ast.*;
import Visitor.Visitor;

public class Printer implements Visitor {
    int indent = -1;

    void print(String out) {
        for (int i = 0; i < indent; i++) System.out.print('\t');
        System.out.print(out + '\n');
    }


    @Override public void visit (AST ctx) {
        visit((Prog)ctx);
    }

    @Override public void visit (Type ctx) {
        if (ctx instanceof BasicType) visit((BasicType)ctx);
        else visit((ArrayType)ctx);
    }
    @Override public void visit (BasicType ctx) {
        indent++;
        if (ctx instanceof ClassType) visit((ClassType) ctx);
        else if (ctx instanceof VoidType) visit((VoidType)ctx);
        else if (ctx instanceof BoolType) visit((BoolType)ctx);
        else if (ctx instanceof IntType) visit((IntType)ctx);
        else visit((StringType)ctx);
        indent--;
    }
    @Override  public void visit (ClassType ctx) {
        print("ClassType");
    }
    @Override public void visit (VoidType ctx) {
        print("VoidType");
    }
    @Override public void visit (BoolType ctx) {
        print("BoolType");
    }
    @Override public void visit (IntType ctx) {
        print("IntType");
    }
    @Override public void visit (StringType ctx) {
        print("StringType");
    }
    @Override public void visit (ArrayType ctx) {
        indent++;
        print("[]");
        visit((Type)ctx.type);
        indent--;
    }

    @Override public void visit (Decl ctx) {
        if (ctx instanceof FuncDecl) visit((FuncDecl)ctx);
        else if (ctx instanceof ClassDecl) visit((ClassDecl)ctx);
        else if (ctx instanceof VarDecl) visit((VarDecl)ctx);
        else visit((VarDeclList)ctx);
    }
    @Override public void visit (FuncDecl ctx) {
        indent++;
        print("FuncDecl");
        visit((Type)ctx.type);
        visit((Symbol)ctx.name);
        if (ctx.prm != null) visit((VarDeclList)ctx.prm);
        visit((Stmt)ctx.body);
        indent--;
    }
    @Override public void visit (ClassDecl ctx) {}
    @Override public void visit (VarDecl ctx) {
        print("VarDecl");
        indent++;
        visit((Type)ctx.type);
        visit((Symbol)ctx.name);
        if (ctx.init != null) visit((Expr)ctx.init);
        indent--;
    }
    @Override public void visit (VarDeclList ctx) {
        indent++;
        print("VarDeclList");
        for (int i = 0; i < ctx.listVarDecl.size(); i++) {
            visit((VarDecl)ctx.listVarDecl.get(i));
        }
        indent--;
    }

    @Override public void visit (Stmt ctx) {
        indent++;
        if (ctx instanceof EmptyStmt) visit((EmptyStmt) ctx);
        else if (ctx instanceof ReturnStmt) visit((ReturnStmt) ctx);
        else if (ctx instanceof BreakStmt) visit((BreakStmt) ctx);
        else if (ctx instanceof CompoundStmt) visit((CompoundStmt) ctx);
        else if (ctx instanceof VarDeclStmt) visit((VarDeclStmt) ctx);
        else if (ctx instanceof WhileStmt) visit((WhileStmt) ctx);
        else if (ctx instanceof IfStmt) visit((IfStmt) ctx);
        else if (ctx instanceof ContinueStmt) visit((ContinueStmt) ctx);
        else if (ctx instanceof ForStmt) visit((ForStmt) ctx);
        else {
            indent--;
            visit((Expr)ctx);
            indent++;
        }
        indent--;
    }
    @Override public void visit (EmptyStmt ctx) {
        print("EmptyStmt");
    }
    @Override public void visit (ReturnStmt ctx) {
        print("ReturnStmt");
        if (ctx.expr != null) visit((Expr)ctx.expr);
    }
    @Override public void visit (BreakStmt ctx) {
        print("BreakStmt");
    }
    @Override public void visit (CompoundStmt ctx) {
        print("CompoundStmt");
        for (int i = 0; i < ctx.stmtList.size(); i++) {
            visit((Stmt)ctx.stmtList.get(i));
        }
    }
    @Override public void visit (VarDeclStmt ctx) {
        visit((VarDecl)ctx.varDecl);
    }
    @Override public void visit (WhileStmt ctx) {
        print("WhileStmt");
        indent++;
        print("while_condition");
        visit((Expr)ctx.condition);
        print("while_body");
        visit((Stmt)ctx.body);
        indent--;

    }
    @Override public void visit (IfStmt ctx) {
        print("IfStmt");
        indent++;
        print("If_condition");
        visit((Expr)ctx.condition);
        print("thenStmt");
        visit((Stmt)ctx.thenStmt);
        if (ctx.elseStmt != null) {
            print("elseStmt");
            visit((Stmt)ctx.elseStmt);
        }
        indent--;
    }
    @Override public void visit (ForStmt ctx) {
        print("ForStmt");
        indent++;
        print("For_stmt1");
        if (ctx.stmt1 != null) visit((Expr)ctx.stmt1);
        print("For_stmt2");
        if (ctx.stmt2 != null) visit((Expr)ctx.stmt2);
        print("For_stmt3");
        if (ctx.stmt3 != null) visit((Expr)ctx.stmt3);
        print("For_body");
        visit((Stmt)ctx.body);
        indent--;
    }
    @Override public void visit (ContinueStmt ctx) {
        print("ContinueStmt");
    }

    @Override public void visit (Expr ctx) {
        if (ctx instanceof UnaryExpr) visit((UnaryExpr)ctx);
        else if (ctx instanceof Creator) visit((Creator)ctx);
        else visit((BinaryExpr)ctx);
    }
    @Override public void visit (BinaryExpr ctx) {
        indent++;
        print("lvalue");
        visit((Expr)ctx.lvalue);
        print(ctx.op.toString());
        print("rvalue");
        visit((Expr)ctx.rvalue);
        indent--;
    }
    @Override public void visit (Creator ctx) {
        indent++;
        print("Creator");
        visit((Type)ctx.type);
        if (ctx.compoundStmt != null) visit((Stmt)ctx.compoundStmt);
        indent--;
    }
    @Override public void visit (UnaryExpr ctx) {
        print("UnaryExpr");
        if (ctx instanceof PrefixExpr) visit((PrefixExpr)ctx);
        else visit((SuffixExpr)ctx);
    }
    @Override public void visit (PrefixExpr ctx) {
        indent++;
        print("PrefixExpr");
        indent++;
        print(ctx.op.toString());
        print("Expr");
        visit((Expr)ctx.expr);
        indent--;
        indent--;
    }
    @Override public void visit (SuffixExpr ctx) {
        if (ctx instanceof SelfIncExpr) visit((SelfIncExpr)ctx);
        else if (ctx instanceof SelfDecExpr) visit((SelfDecExpr)ctx);
        else if (ctx instanceof ArrayAcess) visit((ArrayAcess)ctx);
        else if (ctx instanceof ClassAcess) visit((ClassAcess) ctx);
        else visit((PrmExpr)ctx);
    }
    @Override public void visit (SelfIncExpr ctx) {
        indent++;
        print("SelfIncExpr");
        indent++;
        visit((Expr)ctx.expr);
        print("++");
        indent--;
        indent--;
    }
    @Override public void visit (SelfDecExpr ctx) {
        indent++;
        print("SelfDecExpr");
        indent++;
        visit((Expr)ctx.expr);
        print("--");
        indent--;
        indent--;
    }
    @Override public void visit (ArrayAcess ctx) {
        indent++;
        visit((Expr)ctx.name);
        if (ctx.para != null) visit((Expr)ctx.para);
        indent--;
    }
    @Override public void visit (ClassAcess ctx) {
        indent++;
        print("ClassAcess");
        visit((Expr)ctx.name);
        indent++;
        print(".");
        indent--;
        visit((Expr)ctx.sub);
        indent--;
    }
    @Override public void visit (PrmExpr ctx) {
        if (ctx instanceof IdExpr) visit((IdExpr)ctx);
        else if (ctx instanceof FuncInvoke) visit((FuncInvoke)ctx);
        else visit((ConstExpr) ctx);
    }
    @Override public void visit (IdExpr ctx) {
        indent++;
        print("IdExpr");
        visit((Symbol)ctx.name);
        indent--;
    }
    @Override public void visit (FuncInvoke ctx) {
        indent++;
        print("FuncInvoke");
        visit((Symbol)ctx.name);
        if (ctx.para != null) visit((Stmt)ctx.para);
        indent--;
    }
    @Override public void visit (ConstExpr ctx) {
        if (ctx instanceof StringConst) visit((StringConst)ctx);
        else if (ctx instanceof IntConst) visit((IntConst)ctx);
        else if (ctx instanceof BoolConst) visit((BoolConst)ctx);
        else if (ctx instanceof NullConst) visit((NullConst)ctx);
        else visit((EmptyExpr)ctx);
    }
    @Override public void visit (StringConst ctx) {
        indent++;
        print("StringAddress");
        indent--;
    }
    @Override public void visit (IntConst ctx) {
        indent++;
        print(String.valueOf(ctx.value));
        indent--;
    }
    @Override public void visit (BoolConst ctx) {
        indent++;
        print(String.valueOf(ctx.value));
        indent--;
    }
    @Override public void visit (NullConst ctx) {
        indent++;
        print("NullConst");
        indent--;
    }
    public void visit (EmptyExpr ctx) {
        indent++;
        print("Empty");
        indent--;
    }

    @Override public void visit(Symbol ctx) {
        indent++;
        print(ctx.toString());
        indent--;
    }

    @Override public void visit(Prog ctx) {
        indent++;
        print("Prog");
        for (int i = 0; i < ctx.decls.size(); i++) {
            visit((Decl)ctx.decls.get(i));
        }
        indent--;
    }

}
