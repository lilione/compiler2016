import Visitor.Visitor;
import ast.*;

public class Printer implements Visitor {
    int indent = 0;

    void print(String out) {
        System.out.print(out);
    }

    void print() {
        for (int i = 0; i < indent; i++) print("\t");
    }

    void print(BinaryOp op) {
        switch (op) {
            case ASSIGN : print("="); break;
            case ANDAND : print("&&"); break;
            case OROR : print("||"); break;
            case AND: print("&"); break;
            case XOR : print("^"); break;
            case OR : print("|"); break;
            case EQUAL : print("=="); break;
            case NOTEQUAL : print("!="); break;
            case LESSTHANOREQUAL : print("<="); break;
            case GREATERTHANOREQUAL : print(">="); break;
            case LESSTHAN : print("<"); break;
            case GREATERTHAN : print(">"); break;
            case SHIFTLEFT : print("<<"); break;
            case SHIFTRIGHT : print(">>"); break;
            case PLUS : print("+"); break;
            case MINUS : print("-"); break;
            case STAR : print("*"); break;
            case DIVIDE : print("/"); break;
            case MOD : print("%"); break;
        }
    }

    void print(PrefixOp op) {
        switch (op) {
            case SELFPLUS : print("++"); break;
            case SELFMINUS : print("--"); break;
            case NOT : print("!"); break;
            case TILDE : print("~"); break;
            case MINUS : print("-"); break;
        }
    }


    @Override public void visit (AST ctx) {
        visit((Prog)ctx);
    }

    @Override public void visit (Type ctx) {
        if (ctx instanceof BasicType) visit((BasicType)ctx);
        else visit((ArrayType)ctx);
    }
    @Override public void visit (BasicType ctx) {
        if (ctx instanceof ClassType) visit((ClassType) ctx);
        else if (ctx instanceof VoidType) visit((VoidType)ctx);
        else if (ctx instanceof BoolType) visit((BoolType)ctx);
        else if (ctx instanceof IntType) visit((IntType)ctx);
        else visit((StringType)ctx);
    }
    @Override  public void visit (ClassType ctx) {
        print(ctx.name.toString());
    }
    @Override public void visit (VoidType ctx) {
        print("void");
    }
    @Override public void visit (BoolType ctx) {
        print("bool");
    }
    @Override public void visit (IntType ctx) {
        print("int");
    }
    @Override public void visit (StringType ctx) {
        print("string");
    }
    @Override public void visit (ArrayType ctx) {
        visit(ctx.type);
        print("[]");
    }

    @Override public void visit (Decl ctx) {
        if (ctx instanceof FuncDecl) visit((FuncDecl)ctx);
        else if (ctx instanceof ClassDecl) visit((ClassDecl)ctx);
        else if (ctx instanceof VarDecl) visit((VarDecl)ctx);
        else visit((VarDeclList)ctx);
    }
    @Override public void visit (FuncDecl ctx) {
        visit(ctx.type); print(" ");
        visit(ctx.name);
        print("(");
        if (ctx.prm != null) for (int i = 0; i < ctx.prm.listVarDecl.size(); i++) {
            if (i > 0) print(", ");
            visit(ctx.prm.listVarDecl.get(i).type);
            visit(ctx.prm.listVarDecl.get(i).name);
        }
        print(")");
        print("\n");
        visit((Stmt)ctx.body);
    }
    @Override public void visit (ClassDecl ctx) {
        print("class ");
        visit(ctx.name);
        visit(ctx.var);
    }
    @Override public void visit (VarDecl ctx) {
        visit(ctx.type);
        print(" ");
        visit(ctx.name);
        if (ctx.init != null) {
            print(" = ");
            visit(ctx.init);
        }
        print(";\n");
    }
    @Override public void visit (VarDeclList ctx) {
        print("{\n");
        indent++;
        for (int i = 0; i < ctx.listVarDecl.size(); i++) {
            visit((VarDecl)ctx.listVarDecl.get(i));
        }
        indent--;
        print();
        print("}");
    }

    @Override public void visit (Stmt ctx) {
        print();
        if (ctx instanceof EmptyStmt) {
            visit((EmptyStmt) ctx);
            print("\n");
        }
        else if (ctx instanceof ReturnStmt) {
            visit((ReturnStmt) ctx);
            print("\n");
        }
        else if (ctx instanceof BreakStmt) {
            visit((BreakStmt) ctx);
            print("\n");
        }
        else if (ctx instanceof CompoundStmt) {
            visit((CompoundStmt) ctx);
            print("\n");
        }
        else if (ctx instanceof VarDeclStmt) visit((VarDeclStmt) ctx);
        else if (ctx instanceof WhileStmt) visit((WhileStmt) ctx);
        else if (ctx instanceof IfStmt) visit((IfStmt) ctx);
        else if (ctx instanceof ContinueStmt) {
            visit((ContinueStmt) ctx);
            print("\n");
        }
        else if (ctx instanceof ForStmt) visit((ForStmt) ctx);
        else {
            visit((Expr)ctx);
            print(";\n");
        }
    }
    @Override public void visit (EmptyStmt ctx){ }
    @Override public void visit (ReturnStmt ctx) {
        print("return ");
        if (ctx.expr != null) visit((Expr)ctx.expr);
        print(";");
    }
    @Override public void visit (BreakStmt ctx) {
        print("break;");
    }
    @Override public void visit (CompoundStmt ctx) {
        print("{\n");
        indent++;
        for (int i = 0; i < ctx.stmtList.size(); i++) {
            visit((Stmt)ctx.stmtList.get(i));
        }
        indent--;
        print();
        print("}");
    }
    @Override public void visit (VarDeclStmt ctx) {
        visit((VarDecl)ctx.varDecl);
    }
    @Override public void visit (WhileStmt ctx) {
        print("while (");
        visit(ctx.condition);
        print(")");
        print("\n");
        visit((Stmt)ctx.body);
    }
    @Override public void visit (IfStmt ctx) {
        print("if (");
        visit(ctx.condition);
        print(")");
        print("\n");
        visit(ctx.thenStmt);
        if (ctx.elseStmt != null) {
            print("else\n");
            visit(ctx.elseStmt);
        }
    }
    @Override public void visit (ForStmt ctx) {
        print("for (");
        if (ctx.stmt1 != null) visit(ctx.stmt1);
        print("; ");
        if (ctx.stmt2 != null) visit(ctx.stmt2);
        print("; ");
        if (ctx.stmt3 != null) visit(ctx.stmt3);
        print(") ");
        print("\n");
        if (!(ctx.body instanceof CompoundStmt)) indent++;
        visit(ctx.body);
        if (!(ctx.body instanceof CompoundStmt)) indent--;
    }
    @Override public void visit (ContinueStmt ctx) {
        print("continue;");
    }

    @Override public void visit (Expr ctx) {
        if (ctx instanceof UnaryExpr) visit((UnaryExpr)ctx);
        else if (ctx instanceof Creator) visit((Creator)ctx);
        else visit((BinaryExpr)ctx);
    }
    @Override public void visit (BinaryExpr ctx) {
        visit((Expr)ctx.lvalue);
        print(" ");
        print(ctx.op);
        print(" ");
        visit((Expr)ctx.rvalue);
    }
    @Override public void visit (Creator ctx) {
        print("new ");
        visit((Type)ctx.type);
        if (ctx.compoundStmt != null) {
            for (int i = 0; i < ctx.compoundStmt.stmtList.size(); i++) {
                print("[");
                visit((Expr)ctx.compoundStmt.stmtList.get(i));
                print("]");
            }
        }
    }
    @Override public void visit (UnaryExpr ctx) {
        if (ctx instanceof PrefixExpr) visit((PrefixExpr)ctx);
        else visit((SuffixExpr)ctx);
    }
    @Override public void visit (PrefixExpr ctx) {
        print(ctx.op);
        print(" ");
        visit((Expr)ctx.expr);
    }
    @Override public void visit (SuffixExpr ctx) {
        if (ctx instanceof SelfIncExpr) visit((SelfIncExpr)ctx);
        else if (ctx instanceof SelfDecExpr) visit((SelfDecExpr)ctx);
        else if (ctx instanceof ArrayAcess) visit((ArrayAcess)ctx);
        else if (ctx instanceof ClassAcess) visit((ClassAcess) ctx);
        else visit((PrmExpr)ctx);
    }
    @Override public void visit (SelfIncExpr ctx) {
        visit((Expr)ctx.expr);
        print(" ++");
    }
    @Override public void visit (SelfDecExpr ctx) {
        visit((Expr)ctx.expr);
        print( "--");
    }
    @Override public void visit (ArrayAcess ctx) {
        visit((Expr)ctx.name);
        if (ctx.para != null) {
            print("[");
            visit((Expr)ctx.para);
            print("]");
        }
    }
    @Override public void visit (ClassAcess ctx) {
        visit((Expr)ctx.name);
        print(".");
        visit((Expr)ctx.sub);
    }
    @Override public void visit (PrmExpr ctx) {
        if (ctx instanceof IdExpr) visit((IdExpr)ctx);
        else if (ctx instanceof FuncInvoke) visit((FuncInvoke)ctx);
        else visit((ConstExpr) ctx);
    }
    @Override public void visit (IdExpr ctx) {
        visit((Symbol)ctx.name);
    }
    @Override public void visit (FuncInvoke ctx) {
        visit((Symbol)ctx.name);
        print("(");
        if (ctx.para != null) {
            for (int i = 0; i < ctx.para.stmtList.size(); i++) {
                if (i > 0) print(", ");
                visit((Expr)ctx.para.stmtList.get(i));
            }
        }
        print(")");
    }
    @Override public void visit (ConstExpr ctx) {
        if (ctx instanceof StringConst) visit((StringConst)ctx);
        else if (ctx instanceof IntConst) visit((IntConst)ctx);
        else if (ctx instanceof BoolConst) visit((BoolConst)ctx);
        else if (ctx instanceof NullConst) visit((NullConst)ctx);
        else visit((EmptyExpr)ctx);
    }
    @Override public void visit (StringConst ctx) {
        print("\"" + ctx.origin + "\"");
    }
    @Override public void visit (IntConst ctx) {
        print(String.valueOf(ctx.value));
    }
    @Override public void visit (BoolConst ctx) {
        if (ctx.value == true) print("true");
        else print("false");
    }
    @Override public void visit (NullConst ctx) {
        print("null");
    }
    public void visit (EmptyExpr ctx) {
    }

    @Override public void visit(Symbol ctx) {
        print(ctx.toString());
    }

    @Override public void visit(Prog ctx) {
        for (int i = 0; i < ctx.decls.size(); i++) {
            visit((Decl)ctx.decls.get(i));
        }
    }

}
