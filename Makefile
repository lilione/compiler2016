all: clean
	@mkdir -p ./bin
	@cd ./src/parser && java -jar "../../lib/antlr-4.5-complete.jar"  -Dlanguage=Java -listener -no-visitor -lib . MX.g4
	@cd ./src && javac -cp "../lib/antlr-4.5-complete.jar" \
		./*/*.java \
		./*.java \
		-d ../bin
	@cp ./lib/antlr-4.5-complete.jar ./bin
	@cd ./bin && jar xf ./antlr-4.5-complete.jar\
			 && rm -rf ./META-INF \
			 && jar cef Main ahaha.jar ./ \
			 && rm -rf ./antlr-4.5-complete.jar ./ast ./parser *.class ./Visitor ./org  ./st4hidden
clean:
	rm -rf ./bin
