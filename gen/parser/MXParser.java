// Generated from G:/study/常规/大二/大二下/编译器/compiler2016/src/parser\MX.g4 by ANTLR 4.5.1
package parser;

package parser;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MXParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		BoolConstant=1, IntegerConstant=2, StringConstant=3, LSQUARE=4, RSQUARE=5, 
		LPAREN=6, RPAREN=7, LBRACE=8, RBRACE=9, LESSTHANOREQUAL=10, GREATERTHANOREQUAL=11, 
		LESSTHAN=12, GREATERTHAN=13, SHIFTLEFT=14, SHIFTRIGHT=15, SELFPLUS=16, 
		SELFMINUS=17, PLUS=18, MINUS=19, STAR=20, DIVIDE=21, MOD=22, ANDAND=23, 
		OROR=24, AND=25, XOR=26, OR=27, NOT=28, TILDE=29, EQUAL=30, NOTEQUAL=31, 
		ASSIGNEQUAL=32, DOT=33, COLON=34, COMMA=35, SEMICOLON=36, DOUBLEQUATATION=37, 
		INT=38, BOOL=39, STRING=40, IF=41, ELSE=42, FOR=43, WHILE=44, BREAK=45, 
		CONTINUE=46, RETURN=47, CLASS=48, VOID=49, NULL=50, NEW=51, TRUE=52, FALSE=53, 
		WS=54, COMMENT=55, LINE_COMMENT=56, ID=57, LETTER=58, DIGIT=59, NONZERODIGIT=60, 
		NUM=61, UNDERLINE=62, CHAR=63;
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_variableDeclaration = 2, 
		RULE_singleParameter = 3, RULE_variableType = 4, RULE_basicType = 5, RULE_primitiveType = 6, 
		RULE_variableInitializer = 7, RULE_variableCreator = 8, RULE_functionDefinition = 9, 
		RULE_functionType = 10, RULE_functionBasicType = 11, RULE_parameter = 12, 
		RULE_parameterList = 13, RULE_funcParameter = 14, RULE_block = 15, RULE_statement = 16, 
		RULE_primitiveStatement = 17, RULE_expressionStatement = 18, RULE_ifStatement = 19, 
		RULE_forStatement = 20, RULE_forExpr0 = 21, RULE_forExpr1 = 22, RULE_forExpr2 = 23, 
		RULE_whileStatement = 24, RULE_breakStatement = 25, RULE_continueStatement = 26, 
		RULE_returnStatement = 27, RULE_expression = 28, RULE_binaryExpression = 29, 
		RULE_binaryRelationExpression = 30, RULE_binaryCalculusExpression = 31, 
		RULE_equalRelationExpression = 32, RULE_relationExpression = 33, RULE_shiftExpression = 34, 
		RULE_additiveExpression = 35, RULE_multiplicativeExpression = 36, RULE_unaryExpression = 37, 
		RULE_prefixOperator = 38, RULE_suffixExpression = 39, RULE_suffixOperator = 40, 
		RULE_primaryExpression = 41, RULE_funcInvoke = 42, RULE_constant = 43, 
		RULE_creator = 44, RULE_arrayCreator = 45, RULE_classDefinition = 46, 
		RULE_memberDeclaration = 47, RULE_classMemberDeclaration = 48, RULE_binaryRelationOperator = 49, 
		RULE_binaryCalculusOperator = 50, RULE_equalRelationOperator = 51, RULE_relationOperator = 52, 
		RULE_shiftOperator = 53, RULE_additiveOperator = 54, RULE_multiplicativeOperator = 55;
	public static final String[] ruleNames = {
		"program", "declaration", "variableDeclaration", "singleParameter", "variableType", 
		"basicType", "primitiveType", "variableInitializer", "variableCreator", 
		"functionDefinition", "functionType", "functionBasicType", "parameter", 
		"parameterList", "funcParameter", "block", "statement", "primitiveStatement", 
		"expressionStatement", "ifStatement", "forStatement", "forExpr0", "forExpr1", 
		"forExpr2", "whileStatement", "breakStatement", "continueStatement", "returnStatement", 
		"expression", "binaryExpression", "binaryRelationExpression", "binaryCalculusExpression", 
		"equalRelationExpression", "relationExpression", "shiftExpression", "additiveExpression", 
		"multiplicativeExpression", "unaryExpression", "prefixOperator", "suffixExpression", 
		"suffixOperator", "primaryExpression", "funcInvoke", "constant", "creator", 
		"arrayCreator", "classDefinition", "memberDeclaration", "classMemberDeclaration", 
		"binaryRelationOperator", "binaryCalculusOperator", "equalRelationOperator", 
		"relationOperator", "shiftOperator", "additiveOperator", "multiplicativeOperator"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, null, "'['", "']'", "'('", "')'", "'{'", "'}'", "'<='", 
		"'>='", "'<'", "'>'", "'<<'", "'>>'", "'++'", "'--'", "'+'", "'-'", "'*'", 
		"'/'", "'%'", "'&&'", "'||'", "'&'", "'^'", "'|'", "'!'", "'~'", "'=='", 
		"'!='", "'='", "'.'", "':'", "','", "';'", "'\"'", "'int'", "'bool'", 
		"'string'", "'if'", "'else'", "'for'", "'while'", "'break'", "'continue'", 
		"'return'", "'class'", "'void'", "'null'", "'new'", "'true'", "'false'", 
		null, null, null, null, null, null, null, null, "'_'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "BoolConstant", "IntegerConstant", "StringConstant", "LSQUARE", 
		"RSQUARE", "LPAREN", "RPAREN", "LBRACE", "RBRACE", "LESSTHANOREQUAL", 
		"GREATERTHANOREQUAL", "LESSTHAN", "GREATERTHAN", "SHIFTLEFT", "SHIFTRIGHT", 
		"SELFPLUS", "SELFMINUS", "PLUS", "MINUS", "STAR", "DIVIDE", "MOD", "ANDAND", 
		"OROR", "AND", "XOR", "OR", "NOT", "TILDE", "EQUAL", "NOTEQUAL", "ASSIGNEQUAL", 
		"DOT", "COLON", "COMMA", "SEMICOLON", "DOUBLEQUATATION", "INT", "BOOL", 
		"STRING", "IF", "ELSE", "FOR", "WHILE", "BREAK", "CONTINUE", "RETURN", 
		"CLASS", "VOID", "NULL", "NEW", "TRUE", "FALSE", "WS", "COMMENT", "LINE_COMMENT", 
		"ID", "LETTER", "DIGIT", "NONZERODIGIT", "NUM", "UNDERLINE", "CHAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "MX.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MXParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(MXParser.EOF, 0); }
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(115);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << BOOL) | (1L << STRING) | (1L << CLASS) | (1L << VOID) | (1L << ID))) != 0)) {
				{
				{
				setState(112);
				declaration();
				}
				}
				setState(117);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(118);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
	 
		public DeclarationContext() { }
		public void copyFrom(DeclarationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VariableDeclaration_declarationContext extends DeclarationContext {
		public VariableDeclarationContext variableDeclaration() {
			return getRuleContext(VariableDeclarationContext.class,0);
		}
		public VariableDeclaration_declarationContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterVariableDeclaration_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitVariableDeclaration_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitVariableDeclaration_declaration(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassDefinition_declarationContext extends DeclarationContext {
		public ClassDefinitionContext classDefinition() {
			return getRuleContext(ClassDefinitionContext.class,0);
		}
		public ClassDefinition_declarationContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterClassDefinition_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitClassDefinition_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitClassDefinition_declaration(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionDefinition_declarationContext extends DeclarationContext {
		public FunctionDefinitionContext functionDefinition() {
			return getRuleContext(FunctionDefinitionContext.class,0);
		}
		public FunctionDefinition_declarationContext(DeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterFunctionDefinition_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitFunctionDefinition_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitFunctionDefinition_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			setState(123);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				_localctx = new VariableDeclaration_declarationContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(120);
				variableDeclaration();
				}
				break;
			case 2:
				_localctx = new FunctionDefinition_declarationContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(121);
				functionDefinition();
				}
				break;
			case 3:
				_localctx = new ClassDefinition_declarationContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(122);
				classDefinition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public SingleParameterContext singleParameter() {
			return getRuleContext(SingleParameterContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public VariableInitializerContext variableInitializer() {
			return getRuleContext(VariableInitializerContext.class,0);
		}
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitVariableDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitVariableDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_variableDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			singleParameter();
			setState(127);
			_la = _input.LA(1);
			if (_la==ASSIGNEQUAL) {
				{
				setState(126);
				variableInitializer();
				}
			}

			setState(129);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SingleParameterContext extends ParserRuleContext {
		public VariableTypeContext variableType() {
			return getRuleContext(VariableTypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public SingleParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_singleParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSingleParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSingleParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSingleParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SingleParameterContext singleParameter() throws RecognitionException {
		SingleParameterContext _localctx = new SingleParameterContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_singleParameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			variableType(0);
			setState(132);
			match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableTypeContext extends ParserRuleContext {
		public VariableTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableType; }
	 
		public VariableTypeContext() { }
		public void copyFrom(VariableTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BasicType_variableTypeContext extends VariableTypeContext {
		public BasicTypeContext basicType() {
			return getRuleContext(BasicTypeContext.class,0);
		}
		public BasicType_variableTypeContext(VariableTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBasicType_variableType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBasicType_variableType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBasicType_variableType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_variableTypeContext extends VariableTypeContext {
		public VariableTypeContext variableType() {
			return getRuleContext(VariableTypeContext.class,0);
		}
		public TerminalNode LSQUARE() { return getToken(MXParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(MXParser.RSQUARE, 0); }
		public Compound_variableTypeContext(VariableTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_variableType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_variableType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_variableType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableTypeContext variableType() throws RecognitionException {
		return variableType(0);
	}

	private VariableTypeContext variableType(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		VariableTypeContext _localctx = new VariableTypeContext(_ctx, _parentState);
		VariableTypeContext _prevctx = _localctx;
		int _startState = 8;
		enterRecursionRule(_localctx, 8, RULE_variableType, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new BasicType_variableTypeContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(135);
			basicType();
			}
			_ctx.stop = _input.LT(-1);
			setState(142);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_variableTypeContext(new VariableTypeContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_variableType);
					setState(137);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(138);
					match(LSQUARE);
					setState(139);
					match(RSQUARE);
					}
					} 
				}
				setState(144);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,3,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BasicTypeContext extends ParserRuleContext {
		public BasicTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_basicType; }
	 
		public BasicTypeContext() { }
		public void copyFrom(BasicTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrimitiveType_variableTypeContext extends BasicTypeContext {
		public PrimitiveTypeContext primitiveType() {
			return getRuleContext(PrimitiveTypeContext.class,0);
		}
		public PrimitiveType_variableTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPrimitiveType_variableType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPrimitiveType_variableType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPrimitiveType_variableType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ID_variableTypeContext extends BasicTypeContext {
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public ID_variableTypeContext(BasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterID_variableType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitID_variableType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitID_variableType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BasicTypeContext basicType() throws RecognitionException {
		BasicTypeContext _localctx = new BasicTypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_basicType);
		try {
			setState(147);
			switch (_input.LA(1)) {
			case INT:
			case BOOL:
			case STRING:
				_localctx = new PrimitiveType_variableTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(145);
				primitiveType();
				}
				break;
			case ID:
				_localctx = new ID_variableTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(146);
				match(ID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimitiveTypeContext extends ParserRuleContext {
		public PrimitiveTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitiveType; }
	 
		public PrimitiveTypeContext() { }
		public void copyFrom(PrimitiveTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class STRING_primitiveTypeContext extends PrimitiveTypeContext {
		public TerminalNode STRING() { return getToken(MXParser.STRING, 0); }
		public STRING_primitiveTypeContext(PrimitiveTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSTRING_primitiveType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSTRING_primitiveType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSTRING_primitiveType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BOOL_primitiveTypeContext extends PrimitiveTypeContext {
		public TerminalNode BOOL() { return getToken(MXParser.BOOL, 0); }
		public BOOL_primitiveTypeContext(PrimitiveTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBOOL_primitiveType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBOOL_primitiveType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBOOL_primitiveType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class INT_primitiveTypeContext extends PrimitiveTypeContext {
		public TerminalNode INT() { return getToken(MXParser.INT, 0); }
		public INT_primitiveTypeContext(PrimitiveTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterINT_primitiveType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitINT_primitiveType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitINT_primitiveType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimitiveTypeContext primitiveType() throws RecognitionException {
		PrimitiveTypeContext _localctx = new PrimitiveTypeContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_primitiveType);
		try {
			setState(152);
			switch (_input.LA(1)) {
			case INT:
				_localctx = new INT_primitiveTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(149);
				match(INT);
				}
				break;
			case BOOL:
				_localctx = new BOOL_primitiveTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(150);
				match(BOOL);
				}
				break;
			case STRING:
				_localctx = new STRING_primitiveTypeContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(151);
				match(STRING);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableInitializerContext extends ParserRuleContext {
		public TerminalNode ASSIGNEQUAL() { return getToken(MXParser.ASSIGNEQUAL, 0); }
		public VariableCreatorContext variableCreator() {
			return getRuleContext(VariableCreatorContext.class,0);
		}
		public VariableInitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableInitializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterVariableInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitVariableInitializer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitVariableInitializer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableInitializerContext variableInitializer() throws RecognitionException {
		VariableInitializerContext _localctx = new VariableInitializerContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_variableInitializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(154);
			match(ASSIGNEQUAL);
			setState(155);
			variableCreator();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableCreatorContext extends ParserRuleContext {
		public VariableCreatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableCreator; }
	 
		public VariableCreatorContext() { }
		public void copyFrom(VariableCreatorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Expression_variableCreatorContext extends VariableCreatorContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression_variableCreatorContext(VariableCreatorContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterExpression_variableCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitExpression_variableCreator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitExpression_variableCreator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableCreatorContext variableCreator() throws RecognitionException {
		VariableCreatorContext _localctx = new VariableCreatorContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_variableCreator);
		try {
			_localctx = new Expression_variableCreatorContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitFunctionDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitFunctionDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_functionDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(159);
			functionType(0);
			setState(160);
			match(ID);
			setState(161);
			parameter();
			setState(162);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionTypeContext extends ParserRuleContext {
		public FunctionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionType; }
	 
		public FunctionTypeContext() { }
		public void copyFrom(FunctionTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_functionTypeContext extends FunctionTypeContext {
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public TerminalNode LSQUARE() { return getToken(MXParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(MXParser.RSQUARE, 0); }
		public Compound_functionTypeContext(FunctionTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_functionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_functionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_functionType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FunctionBasicType_functionTypeContext extends FunctionTypeContext {
		public FunctionBasicTypeContext functionBasicType() {
			return getRuleContext(FunctionBasicTypeContext.class,0);
		}
		public FunctionBasicType_functionTypeContext(FunctionTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterFunctionBasicType_functionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitFunctionBasicType_functionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitFunctionBasicType_functionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionTypeContext functionType() throws RecognitionException {
		return functionType(0);
	}

	private FunctionTypeContext functionType(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		FunctionTypeContext _localctx = new FunctionTypeContext(_ctx, _parentState);
		FunctionTypeContext _prevctx = _localctx;
		int _startState = 20;
		enterRecursionRule(_localctx, 20, RULE_functionType, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new FunctionBasicType_functionTypeContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(165);
			functionBasicType();
			}
			_ctx.stop = _input.LT(-1);
			setState(172);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_functionTypeContext(new FunctionTypeContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_functionType);
					setState(167);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(168);
					match(LSQUARE);
					setState(169);
					match(RSQUARE);
					}
					} 
				}
				setState(174);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FunctionBasicTypeContext extends ParserRuleContext {
		public FunctionBasicTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionBasicType; }
	 
		public FunctionBasicTypeContext() { }
		public void copyFrom(FunctionBasicTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BasicType_functionTypeContext extends FunctionBasicTypeContext {
		public BasicTypeContext basicType() {
			return getRuleContext(BasicTypeContext.class,0);
		}
		public BasicType_functionTypeContext(FunctionBasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBasicType_functionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBasicType_functionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBasicType_functionType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VOID_functionTypeContext extends FunctionBasicTypeContext {
		public TerminalNode VOID() { return getToken(MXParser.VOID, 0); }
		public VOID_functionTypeContext(FunctionBasicTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterVOID_functionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitVOID_functionType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitVOID_functionType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionBasicTypeContext functionBasicType() throws RecognitionException {
		FunctionBasicTypeContext _localctx = new FunctionBasicTypeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_functionBasicType);
		try {
			setState(177);
			switch (_input.LA(1)) {
			case INT:
			case BOOL:
			case STRING:
			case ID:
				_localctx = new BasicType_functionTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(175);
				basicType();
				}
				break;
			case VOID:
				_localctx = new VOID_functionTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(176);
				match(VOID);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_parameter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(179);
			match(LPAREN);
			setState(181);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << BOOL) | (1L << STRING) | (1L << ID))) != 0)) {
				{
				setState(180);
				parameterList(0);
				}
			}

			setState(183);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterListContext extends ParserRuleContext {
		public ParameterListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameterList; }
	 
		public ParameterListContext() { }
		public void copyFrom(ParameterListContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SingleParameter_parameterListContext extends ParameterListContext {
		public FuncParameterContext funcParameter() {
			return getRuleContext(FuncParameterContext.class,0);
		}
		public SingleParameter_parameterListContext(ParameterListContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSingleParameter_parameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSingleParameter_parameterList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSingleParameter_parameterList(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_parameterListContext extends ParameterListContext {
		public ParameterListContext parameterList() {
			return getRuleContext(ParameterListContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(MXParser.COMMA, 0); }
		public FuncParameterContext funcParameter() {
			return getRuleContext(FuncParameterContext.class,0);
		}
		public Compound_parameterListContext(ParameterListContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_parameterList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_parameterList(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_parameterList(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterListContext parameterList() throws RecognitionException {
		return parameterList(0);
	}

	private ParameterListContext parameterList(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ParameterListContext _localctx = new ParameterListContext(_ctx, _parentState);
		ParameterListContext _prevctx = _localctx;
		int _startState = 26;
		enterRecursionRule(_localctx, 26, RULE_parameterList, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new SingleParameter_parameterListContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(186);
			funcParameter();
			}
			_ctx.stop = _input.LT(-1);
			setState(193);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_parameterListContext(new ParameterListContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_parameterList);
					setState(188);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(189);
					match(COMMA);
					setState(190);
					funcParameter();
					}
					} 
				}
				setState(195);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class FuncParameterContext extends ParserRuleContext {
		public SingleParameterContext singleParameter() {
			return getRuleContext(SingleParameterContext.class,0);
		}
		public FuncParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcParameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterFuncParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitFuncParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitFuncParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncParameterContext funcParameter() throws RecognitionException {
		FuncParameterContext _localctx = new FuncParameterContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_funcParameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			singleParameter();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(MXParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(MXParser.RBRACE, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(198);
			match(LBRACE);
			setState(200);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << LBRACE) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << SEMICOLON) | (1L << INT) | (1L << BOOL) | (1L << STRING) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(199);
				statement(0);
				}
			}

			setState(202);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
	 
		public StatementContext() { }
		public void copyFrom(StatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_statementContext extends StatementContext {
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public PrimitiveStatementContext primitiveStatement() {
			return getRuleContext(PrimitiveStatementContext.class,0);
		}
		public Compound_statementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrimitiveStatement_statementContext extends StatementContext {
		public PrimitiveStatementContext primitiveStatement() {
			return getRuleContext(PrimitiveStatementContext.class,0);
		}
		public PrimitiveStatement_statementContext(StatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPrimitiveStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPrimitiveStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPrimitiveStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		return statement(0);
	}

	private StatementContext statement(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		StatementContext _localctx = new StatementContext(_ctx, _parentState);
		StatementContext _prevctx = _localctx;
		int _startState = 32;
		enterRecursionRule(_localctx, 32, RULE_statement, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new PrimitiveStatement_statementContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(205);
			primitiveStatement();
			}
			_ctx.stop = _input.LT(-1);
			setState(211);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_statementContext(new StatementContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_statement);
					setState(207);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(208);
					primitiveStatement();
					}
					} 
				}
				setState(213);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class PrimitiveStatementContext extends ParserRuleContext {
		public PrimitiveStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitiveStatement; }
	 
		public PrimitiveStatementContext() { }
		public void copyFrom(PrimitiveStatementContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReturnStatement_statementContext extends PrimitiveStatementContext {
		public ReturnStatementContext returnStatement() {
			return getRuleContext(ReturnStatementContext.class,0);
		}
		public ReturnStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterReturnStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitReturnStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitReturnStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VariableDeclaration_statementContext extends PrimitiveStatementContext {
		public VariableDeclarationContext variableDeclaration() {
			return getRuleContext(VariableDeclarationContext.class,0);
		}
		public VariableDeclaration_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterVariableDeclaration_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitVariableDeclaration_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitVariableDeclaration_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfStatement_statementContext extends PrimitiveStatementContext {
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public IfStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterIfStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitIfStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitIfStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BreakStatement_statementContext extends PrimitiveStatementContext {
		public BreakStatementContext breakStatement() {
			return getRuleContext(BreakStatementContext.class,0);
		}
		public BreakStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBreakStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBreakStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBreakStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhileStatement_statementContext extends PrimitiveStatementContext {
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public WhileStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterWhileStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitWhileStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitWhileStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Block_statementContext extends PrimitiveStatementContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public Block_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBlock_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBlock_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBlock_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ExpressionStatement_statementContext extends PrimitiveStatementContext {
		public ExpressionStatementContext expressionStatement() {
			return getRuleContext(ExpressionStatementContext.class,0);
		}
		public ExpressionStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterExpressionStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitExpressionStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitExpressionStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SEMICOLON_statementContext extends PrimitiveStatementContext {
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public SEMICOLON_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSEMICOLON_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSEMICOLON_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSEMICOLON_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ForStatement_statementContext extends PrimitiveStatementContext {
		public ForStatementContext forStatement() {
			return getRuleContext(ForStatementContext.class,0);
		}
		public ForStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterForStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitForStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitForStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ContinueStatement_statementContext extends PrimitiveStatementContext {
		public ContinueStatementContext continueStatement() {
			return getRuleContext(ContinueStatementContext.class,0);
		}
		public ContinueStatement_statementContext(PrimitiveStatementContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterContinueStatement_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitContinueStatement_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitContinueStatement_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimitiveStatementContext primitiveStatement() throws RecognitionException {
		PrimitiveStatementContext _localctx = new PrimitiveStatementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_primitiveStatement);
		try {
			setState(224);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				_localctx = new Block_statementContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(214);
				block();
				}
				break;
			case 2:
				_localctx = new ExpressionStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(215);
				expressionStatement();
				}
				break;
			case 3:
				_localctx = new VariableDeclaration_statementContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(216);
				variableDeclaration();
				}
				break;
			case 4:
				_localctx = new IfStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(217);
				ifStatement();
				}
				break;
			case 5:
				_localctx = new ForStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(218);
				forStatement();
				}
				break;
			case 6:
				_localctx = new WhileStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(219);
				whileStatement();
				}
				break;
			case 7:
				_localctx = new BreakStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(220);
				breakStatement();
				}
				break;
			case 8:
				_localctx = new ContinueStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(221);
				continueStatement();
				}
				break;
			case 9:
				_localctx = new ReturnStatement_statementContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(222);
				returnStatement();
				}
				break;
			case 10:
				_localctx = new SEMICOLON_statementContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(223);
				match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionStatementContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public ExpressionStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterExpressionStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitExpressionStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitExpressionStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionStatementContext expressionStatement() throws RecognitionException {
		ExpressionStatementContext _localctx = new ExpressionStatementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_expressionStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			expression();
			setState(227);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(MXParser.IF, 0); }
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public List<PrimitiveStatementContext> primitiveStatement() {
			return getRuleContexts(PrimitiveStatementContext.class);
		}
		public PrimitiveStatementContext primitiveStatement(int i) {
			return getRuleContext(PrimitiveStatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(MXParser.ELSE, 0); }
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitIfStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitIfStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_ifStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			match(IF);
			setState(230);
			match(LPAREN);
			setState(231);
			expression();
			setState(232);
			match(RPAREN);
			setState(233);
			primitiveStatement();
			setState(236);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(234);
				match(ELSE);
				setState(235);
				primitiveStatement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatementContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(MXParser.FOR, 0); }
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public List<TerminalNode> SEMICOLON() { return getTokens(MXParser.SEMICOLON); }
		public TerminalNode SEMICOLON(int i) {
			return getToken(MXParser.SEMICOLON, i);
		}
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public PrimitiveStatementContext primitiveStatement() {
			return getRuleContext(PrimitiveStatementContext.class,0);
		}
		public ForExpr0Context forExpr0() {
			return getRuleContext(ForExpr0Context.class,0);
		}
		public ForExpr1Context forExpr1() {
			return getRuleContext(ForExpr1Context.class,0);
		}
		public ForExpr2Context forExpr2() {
			return getRuleContext(ForExpr2Context.class,0);
		}
		public ForStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitForStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitForStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForStatementContext forStatement() throws RecognitionException {
		ForStatementContext _localctx = new ForStatementContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_forStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			match(FOR);
			setState(239);
			match(LPAREN);
			setState(241);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(240);
				forExpr0();
				}
			}

			setState(243);
			match(SEMICOLON);
			setState(245);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(244);
				forExpr1();
				}
			}

			setState(247);
			match(SEMICOLON);
			setState(249);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(248);
				forExpr2();
				}
			}

			setState(251);
			match(RPAREN);
			setState(252);
			primitiveStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForExpr0Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForExpr0Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forExpr0; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterForExpr0(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitForExpr0(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitForExpr0(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForExpr0Context forExpr0() throws RecognitionException {
		ForExpr0Context _localctx = new ForExpr0Context(_ctx, getState());
		enterRule(_localctx, 42, RULE_forExpr0);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(254);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForExpr1Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForExpr1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forExpr1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterForExpr1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitForExpr1(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitForExpr1(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForExpr1Context forExpr1() throws RecognitionException {
		ForExpr1Context _localctx = new ForExpr1Context(_ctx, getState());
		enterRule(_localctx, 44, RULE_forExpr1);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForExpr2Context extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ForExpr2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forExpr2; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterForExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitForExpr2(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitForExpr2(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ForExpr2Context forExpr2() throws RecognitionException {
		ForExpr2Context _localctx = new ForExpr2Context(_ctx, getState());
		enterRule(_localctx, 46, RULE_forExpr2);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(MXParser.WHILE, 0); }
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public PrimitiveStatementContext primitiveStatement() {
			return getRuleContext(PrimitiveStatementContext.class,0);
		}
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitWhileStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitWhileStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_whileStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(260);
			match(WHILE);
			setState(261);
			match(LPAREN);
			setState(262);
			expression();
			setState(263);
			match(RPAREN);
			setState(264);
			primitiveStatement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakStatementContext extends ParserRuleContext {
		public TerminalNode BREAK() { return getToken(MXParser.BREAK, 0); }
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public BreakStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBreakStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBreakStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBreakStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BreakStatementContext breakStatement() throws RecognitionException {
		BreakStatementContext _localctx = new BreakStatementContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_breakStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266);
			match(BREAK);
			setState(267);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueStatementContext extends ParserRuleContext {
		public TerminalNode CONTINUE() { return getToken(MXParser.CONTINUE, 0); }
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public ContinueStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterContinueStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitContinueStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitContinueStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ContinueStatementContext continueStatement() throws RecognitionException {
		ContinueStatementContext _localctx = new ContinueStatementContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_continueStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(269);
			match(CONTINUE);
			setState(270);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatementContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(MXParser.RETURN, 0); }
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ReturnStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterReturnStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitReturnStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitReturnStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnStatementContext returnStatement() throws RecognitionException {
		ReturnStatementContext _localctx = new ReturnStatementContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_returnStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			match(RETURN);
			setState(274);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(273);
				expression();
				}
			}

			setState(276);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
	 
		public ExpressionContext() { }
		public void copyFrom(ExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class BinaryExpression_expressionContext extends ExpressionContext {
		public BinaryExpressionContext binaryExpression() {
			return getRuleContext(BinaryExpressionContext.class,0);
		}
		public BinaryExpression_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBinaryExpression_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBinaryExpression_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBinaryExpression_expression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_expressionContext extends ExpressionContext {
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public TerminalNode ASSIGNEQUAL() { return getToken(MXParser.ASSIGNEQUAL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Compound_expressionContext(ExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_expression);
		try {
			setState(283);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				_localctx = new BinaryExpression_expressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(278);
				binaryExpression();
				}
				break;
			case 2:
				_localctx = new Compound_expressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(279);
				unaryExpression();
				setState(280);
				match(ASSIGNEQUAL);
				setState(281);
				expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryExpressionContext extends ParserRuleContext {
		public BinaryRelationExpressionContext binaryRelationExpression() {
			return getRuleContext(BinaryRelationExpressionContext.class,0);
		}
		public BinaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBinaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBinaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBinaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryExpressionContext binaryExpression() throws RecognitionException {
		BinaryExpressionContext _localctx = new BinaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_binaryExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(285);
			binaryRelationExpression(0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryRelationExpressionContext extends ParserRuleContext {
		public BinaryRelationExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryRelationExpression; }
	 
		public BinaryRelationExpressionContext() { }
		public void copyFrom(BinaryRelationExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_binaryRelationExpressionContext extends BinaryRelationExpressionContext {
		public BinaryRelationExpressionContext binaryRelationExpression() {
			return getRuleContext(BinaryRelationExpressionContext.class,0);
		}
		public BinaryRelationOperatorContext binaryRelationOperator() {
			return getRuleContext(BinaryRelationOperatorContext.class,0);
		}
		public BinaryCalculusExpressionContext binaryCalculusExpression() {
			return getRuleContext(BinaryCalculusExpressionContext.class,0);
		}
		public Compound_binaryRelationExpressionContext(BinaryRelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_binaryRelationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_binaryRelationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_binaryRelationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BinaryCalculusExpression_binaryRelationExpressionContext extends BinaryRelationExpressionContext {
		public BinaryCalculusExpressionContext binaryCalculusExpression() {
			return getRuleContext(BinaryCalculusExpressionContext.class,0);
		}
		public BinaryCalculusExpression_binaryRelationExpressionContext(BinaryRelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBinaryCalculusExpression_binaryRelationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBinaryCalculusExpression_binaryRelationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBinaryCalculusExpression_binaryRelationExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryRelationExpressionContext binaryRelationExpression() throws RecognitionException {
		return binaryRelationExpression(0);
	}

	private BinaryRelationExpressionContext binaryRelationExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BinaryRelationExpressionContext _localctx = new BinaryRelationExpressionContext(_ctx, _parentState);
		BinaryRelationExpressionContext _prevctx = _localctx;
		int _startState = 60;
		enterRecursionRule(_localctx, 60, RULE_binaryRelationExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new BinaryCalculusExpression_binaryRelationExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(288);
			binaryCalculusExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(296);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_binaryRelationExpressionContext(new BinaryRelationExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_binaryRelationExpression);
					setState(290);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(291);
					binaryRelationOperator();
					setState(292);
					binaryCalculusExpression(0);
					}
					} 
				}
				setState(298);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BinaryCalculusExpressionContext extends ParserRuleContext {
		public BinaryCalculusExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryCalculusExpression; }
	 
		public BinaryCalculusExpressionContext() { }
		public void copyFrom(BinaryCalculusExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_binaryCalculusExpressionContext extends BinaryCalculusExpressionContext {
		public BinaryCalculusExpressionContext binaryCalculusExpression() {
			return getRuleContext(BinaryCalculusExpressionContext.class,0);
		}
		public BinaryCalculusOperatorContext binaryCalculusOperator() {
			return getRuleContext(BinaryCalculusOperatorContext.class,0);
		}
		public EqualRelationExpressionContext equalRelationExpression() {
			return getRuleContext(EqualRelationExpressionContext.class,0);
		}
		public Compound_binaryCalculusExpressionContext(BinaryCalculusExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_binaryCalculusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_binaryCalculusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_binaryCalculusExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class EqualRelationExpression_binaryCalculusExpressionContext extends BinaryCalculusExpressionContext {
		public EqualRelationExpressionContext equalRelationExpression() {
			return getRuleContext(EqualRelationExpressionContext.class,0);
		}
		public EqualRelationExpression_binaryCalculusExpressionContext(BinaryCalculusExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterEqualRelationExpression_binaryCalculusExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitEqualRelationExpression_binaryCalculusExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitEqualRelationExpression_binaryCalculusExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryCalculusExpressionContext binaryCalculusExpression() throws RecognitionException {
		return binaryCalculusExpression(0);
	}

	private BinaryCalculusExpressionContext binaryCalculusExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		BinaryCalculusExpressionContext _localctx = new BinaryCalculusExpressionContext(_ctx, _parentState);
		BinaryCalculusExpressionContext _prevctx = _localctx;
		int _startState = 62;
		enterRecursionRule(_localctx, 62, RULE_binaryCalculusExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new EqualRelationExpression_binaryCalculusExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(300);
			equalRelationExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(308);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_binaryCalculusExpressionContext(new BinaryCalculusExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_binaryCalculusExpression);
					setState(302);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(303);
					binaryCalculusOperator();
					setState(304);
					equalRelationExpression(0);
					}
					} 
				}
				setState(310);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,20,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class EqualRelationExpressionContext extends ParserRuleContext {
		public EqualRelationExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalRelationExpression; }
	 
		public EqualRelationExpressionContext() { }
		public void copyFrom(EqualRelationExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RelationExpression_equalRelationExpressionContext extends EqualRelationExpressionContext {
		public RelationExpressionContext relationExpression() {
			return getRuleContext(RelationExpressionContext.class,0);
		}
		public RelationExpression_equalRelationExpressionContext(EqualRelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterRelationExpression_equalRelationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitRelationExpression_equalRelationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitRelationExpression_equalRelationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_equalRelationExpressionContext extends EqualRelationExpressionContext {
		public EqualRelationExpressionContext equalRelationExpression() {
			return getRuleContext(EqualRelationExpressionContext.class,0);
		}
		public EqualRelationOperatorContext equalRelationOperator() {
			return getRuleContext(EqualRelationOperatorContext.class,0);
		}
		public RelationExpressionContext relationExpression() {
			return getRuleContext(RelationExpressionContext.class,0);
		}
		public Compound_equalRelationExpressionContext(EqualRelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_equalRelationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_equalRelationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_equalRelationExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualRelationExpressionContext equalRelationExpression() throws RecognitionException {
		return equalRelationExpression(0);
	}

	private EqualRelationExpressionContext equalRelationExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		EqualRelationExpressionContext _localctx = new EqualRelationExpressionContext(_ctx, _parentState);
		EqualRelationExpressionContext _prevctx = _localctx;
		int _startState = 64;
		enterRecursionRule(_localctx, 64, RULE_equalRelationExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new RelationExpression_equalRelationExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(312);
			relationExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(320);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_equalRelationExpressionContext(new EqualRelationExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_equalRelationExpression);
					setState(314);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(315);
					equalRelationOperator();
					setState(316);
					relationExpression(0);
					}
					} 
				}
				setState(322);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class RelationExpressionContext extends ParserRuleContext {
		public RelationExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationExpression; }
	 
		public RelationExpressionContext() { }
		public void copyFrom(RelationExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ShiftExpression_relationExpressionContext extends RelationExpressionContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public ShiftExpression_relationExpressionContext(RelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterShiftExpression_relationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitShiftExpression_relationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitShiftExpression_relationExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_relationExpressionContext extends RelationExpressionContext {
		public RelationExpressionContext relationExpression() {
			return getRuleContext(RelationExpressionContext.class,0);
		}
		public RelationOperatorContext relationOperator() {
			return getRuleContext(RelationOperatorContext.class,0);
		}
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public Compound_relationExpressionContext(RelationExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_relationExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_relationExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_relationExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationExpressionContext relationExpression() throws RecognitionException {
		return relationExpression(0);
	}

	private RelationExpressionContext relationExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		RelationExpressionContext _localctx = new RelationExpressionContext(_ctx, _parentState);
		RelationExpressionContext _prevctx = _localctx;
		int _startState = 66;
		enterRecursionRule(_localctx, 66, RULE_relationExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ShiftExpression_relationExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(324);
			shiftExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(332);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_relationExpressionContext(new RelationExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_relationExpression);
					setState(326);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(327);
					relationOperator();
					setState(328);
					shiftExpression(0);
					}
					} 
				}
				setState(334);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ShiftExpressionContext extends ParserRuleContext {
		public ShiftExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftExpression; }
	 
		public ShiftExpressionContext() { }
		public void copyFrom(ShiftExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_shiftExpressionContext extends ShiftExpressionContext {
		public ShiftExpressionContext shiftExpression() {
			return getRuleContext(ShiftExpressionContext.class,0);
		}
		public ShiftOperatorContext shiftOperator() {
			return getRuleContext(ShiftOperatorContext.class,0);
		}
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public Compound_shiftExpressionContext(ShiftExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_shiftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_shiftExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_shiftExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AdditiveExpression_shiftExpressionContext extends ShiftExpressionContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public AdditiveExpression_shiftExpressionContext(ShiftExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterAdditiveExpression_shiftExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitAdditiveExpression_shiftExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitAdditiveExpression_shiftExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShiftExpressionContext shiftExpression() throws RecognitionException {
		return shiftExpression(0);
	}

	private ShiftExpressionContext shiftExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ShiftExpressionContext _localctx = new ShiftExpressionContext(_ctx, _parentState);
		ShiftExpressionContext _prevctx = _localctx;
		int _startState = 68;
		enterRecursionRule(_localctx, 68, RULE_shiftExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new AdditiveExpression_shiftExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(336);
			additiveExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(344);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_shiftExpressionContext(new ShiftExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_shiftExpression);
					setState(338);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(339);
					shiftOperator();
					setState(340);
					additiveExpression(0);
					}
					} 
				}
				setState(346);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
	 
		public AdditiveExpressionContext() { }
		public void copyFrom(AdditiveExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class MultiplicativeExpression_additiveExpressionContext extends AdditiveExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public MultiplicativeExpression_additiveExpressionContext(AdditiveExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterMultiplicativeExpression_additiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitMultiplicativeExpression_additiveExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitMultiplicativeExpression_additiveExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_additiveExpressionContext extends AdditiveExpressionContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public AdditiveOperatorContext additiveOperator() {
			return getRuleContext(AdditiveOperatorContext.class,0);
		}
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public Compound_additiveExpressionContext(AdditiveExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_additiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_additiveExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_additiveExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		return additiveExpression(0);
	}

	private AdditiveExpressionContext additiveExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, _parentState);
		AdditiveExpressionContext _prevctx = _localctx;
		int _startState = 70;
		enterRecursionRule(_localctx, 70, RULE_additiveExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new MultiplicativeExpression_additiveExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(348);
			multiplicativeExpression(0);
			}
			_ctx.stop = _input.LT(-1);
			setState(356);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_additiveExpressionContext(new AdditiveExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_additiveExpression);
					setState(350);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(351);
					additiveOperator();
					setState(352);
					multiplicativeExpression(0);
					}
					} 
				}
				setState(358);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
	 
		public MultiplicativeExpressionContext() { }
		public void copyFrom(MultiplicativeExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class UnaryExpressio_multiplicativeExpressionContext extends MultiplicativeExpressionContext {
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public UnaryExpressio_multiplicativeExpressionContext(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterUnaryExpressio_multiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitUnaryExpressio_multiplicativeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitUnaryExpressio_multiplicativeExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_multiplicativeExpressionContext extends MultiplicativeExpressionContext {
		public MultiplicativeExpressionContext multiplicativeExpression() {
			return getRuleContext(MultiplicativeExpressionContext.class,0);
		}
		public MultiplicativeOperatorContext multiplicativeOperator() {
			return getRuleContext(MultiplicativeOperatorContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public Compound_multiplicativeExpressionContext(MultiplicativeExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_multiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_multiplicativeExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_multiplicativeExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		return multiplicativeExpression(0);
	}

	private MultiplicativeExpressionContext multiplicativeExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, _parentState);
		MultiplicativeExpressionContext _prevctx = _localctx;
		int _startState = 72;
		enterRecursionRule(_localctx, 72, RULE_multiplicativeExpression, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new UnaryExpressio_multiplicativeExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(360);
			unaryExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(368);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_multiplicativeExpressionContext(new MultiplicativeExpressionContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_multiplicativeExpression);
					setState(362);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(363);
					multiplicativeOperator();
					setState(364);
					unaryExpression();
					}
					} 
				}
				setState(370);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
	 
		public UnaryExpressionContext() { }
		public void copyFrom(UnaryExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SuffixExpression_unaryExpressionContext extends UnaryExpressionContext {
		public SuffixExpressionContext suffixExpression() {
			return getRuleContext(SuffixExpressionContext.class,0);
		}
		public SuffixExpression_unaryExpressionContext(UnaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSuffixExpression_unaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSuffixExpression_unaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSuffixExpression_unaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NEWcreator_unaryExpressionContext extends UnaryExpressionContext {
		public TerminalNode NEW() { return getToken(MXParser.NEW, 0); }
		public CreatorContext creator() {
			return getRuleContext(CreatorContext.class,0);
		}
		public NEWcreator_unaryExpressionContext(UnaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterNEWcreator_unaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitNEWcreator_unaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitNEWcreator_unaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Prefix_unaryExpressionContext extends UnaryExpressionContext {
		public PrefixOperatorContext prefixOperator() {
			return getRuleContext(PrefixOperatorContext.class,0);
		}
		public UnaryExpressionContext unaryExpression() {
			return getRuleContext(UnaryExpressionContext.class,0);
		}
		public Prefix_unaryExpressionContext(UnaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPrefix_unaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPrefix_unaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPrefix_unaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_unaryExpression);
		try {
			setState(377);
			switch (_input.LA(1)) {
			case BoolConstant:
			case IntegerConstant:
			case StringConstant:
			case LPAREN:
			case NULL:
			case ID:
				_localctx = new SuffixExpression_unaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(371);
				suffixExpression(0);
				}
				break;
			case NEW:
				_localctx = new NEWcreator_unaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(372);
				match(NEW);
				setState(373);
				creator();
				}
				break;
			case SELFPLUS:
			case SELFMINUS:
			case MINUS:
			case NOT:
			case TILDE:
				_localctx = new Prefix_unaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(374);
				prefixOperator();
				setState(375);
				unaryExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixOperatorContext extends ParserRuleContext {
		public TerminalNode SELFPLUS() { return getToken(MXParser.SELFPLUS, 0); }
		public TerminalNode SELFMINUS() { return getToken(MXParser.SELFMINUS, 0); }
		public TerminalNode NOT() { return getToken(MXParser.NOT, 0); }
		public TerminalNode TILDE() { return getToken(MXParser.TILDE, 0); }
		public TerminalNode MINUS() { return getToken(MXParser.MINUS, 0); }
		public PrefixOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPrefixOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPrefixOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPrefixOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrefixOperatorContext prefixOperator() throws RecognitionException {
		PrefixOperatorContext _localctx = new PrefixOperatorContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_prefixOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(379);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SuffixExpressionContext extends ParserRuleContext {
		public SuffixExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_suffixExpression; }
	 
		public SuffixExpressionContext() { }
		public void copyFrom(SuffixExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PrimaryExpression_suffixExpressionContext extends SuffixExpressionContext {
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public PrimaryExpression_suffixExpressionContext(SuffixExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPrimaryExpression_suffixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPrimaryExpression_suffixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPrimaryExpression_suffixExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayAcess_suffixExpressionContext extends SuffixExpressionContext {
		public SuffixExpressionContext suffixExpression() {
			return getRuleContext(SuffixExpressionContext.class,0);
		}
		public TerminalNode LSQUARE() { return getToken(MXParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(MXParser.RSQUARE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ArrayAcess_suffixExpressionContext(SuffixExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterArrayAcess_suffixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitArrayAcess_suffixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitArrayAcess_suffixExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Suffix_suffixExpressionContext extends SuffixExpressionContext {
		public SuffixExpressionContext suffixExpression() {
			return getRuleContext(SuffixExpressionContext.class,0);
		}
		public SuffixOperatorContext suffixOperator() {
			return getRuleContext(SuffixOperatorContext.class,0);
		}
		public Suffix_suffixExpressionContext(SuffixExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSuffix_suffixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSuffix_suffixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSuffix_suffixExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ClassAcess_suffixExpressionContext extends SuffixExpressionContext {
		public SuffixExpressionContext suffixExpression() {
			return getRuleContext(SuffixExpressionContext.class,0);
		}
		public TerminalNode DOT() { return getToken(MXParser.DOT, 0); }
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public ClassAcess_suffixExpressionContext(SuffixExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterClassAcess_suffixExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitClassAcess_suffixExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitClassAcess_suffixExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SuffixExpressionContext suffixExpression() throws RecognitionException {
		return suffixExpression(0);
	}

	private SuffixExpressionContext suffixExpression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		SuffixExpressionContext _localctx = new SuffixExpressionContext(_ctx, _parentState);
		SuffixExpressionContext _prevctx = _localctx;
		int _startState = 78;
		enterRecursionRule(_localctx, 78, RULE_suffixExpression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new PrimaryExpression_suffixExpressionContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(382);
			primaryExpression();
			}
			_ctx.stop = _input.LT(-1);
			setState(397);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(395);
					switch ( getInterpreter().adaptivePredict(_input,28,_ctx) ) {
					case 1:
						{
						_localctx = new ArrayAcess_suffixExpressionContext(new SuffixExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_suffixExpression);
						setState(384);
						if (!(precpred(_ctx, 3))) throw new FailedPredicateException(this, "precpred(_ctx, 3)");
						setState(385);
						match(LSQUARE);
						setState(387);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
							{
							setState(386);
							expression();
							}
						}

						setState(389);
						match(RSQUARE);
						}
						break;
					case 2:
						{
						_localctx = new ClassAcess_suffixExpressionContext(new SuffixExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_suffixExpression);
						setState(390);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(391);
						match(DOT);
						setState(392);
						primaryExpression();
						}
						break;
					case 3:
						{
						_localctx = new Suffix_suffixExpressionContext(new SuffixExpressionContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_suffixExpression);
						setState(393);
						if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
						setState(394);
						suffixOperator();
						}
						break;
					}
					} 
				}
				setState(399);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class SuffixOperatorContext extends ParserRuleContext {
		public SuffixOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_suffixOperator; }
	 
		public SuffixOperatorContext() { }
		public void copyFrom(SuffixOperatorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SELFPLUS_suffixOperatorContext extends SuffixOperatorContext {
		public TerminalNode SELFPLUS() { return getToken(MXParser.SELFPLUS, 0); }
		public SELFPLUS_suffixOperatorContext(SuffixOperatorContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSELFPLUS_suffixOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSELFPLUS_suffixOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSELFPLUS_suffixOperator(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SELFMINUS_suffixOperatorContext extends SuffixOperatorContext {
		public TerminalNode SELFMINUS() { return getToken(MXParser.SELFMINUS, 0); }
		public SELFMINUS_suffixOperatorContext(SuffixOperatorContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSELFMINUS_suffixOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSELFMINUS_suffixOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSELFMINUS_suffixOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SuffixOperatorContext suffixOperator() throws RecognitionException {
		SuffixOperatorContext _localctx = new SuffixOperatorContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_suffixOperator);
		try {
			setState(402);
			switch (_input.LA(1)) {
			case SELFPLUS:
				_localctx = new SELFPLUS_suffixOperatorContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(400);
				match(SELFPLUS);
				}
				break;
			case SELFMINUS:
				_localctx = new SELFMINUS_suffixOperatorContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(401);
				match(SELFMINUS);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExpressionContext extends ParserRuleContext {
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpression; }
	 
		public PrimaryExpressionContext() { }
		public void copyFrom(PrimaryExpressionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FuncInvoke_primaryExpressionContext extends PrimaryExpressionContext {
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public FuncInvokeContext funcInvoke() {
			return getRuleContext(FuncInvokeContext.class,0);
		}
		public FuncInvoke_primaryExpressionContext(PrimaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterFuncInvoke_primaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitFuncInvoke_primaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitFuncInvoke_primaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ID_primaryExpressionContext extends PrimaryExpressionContext {
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public ID_primaryExpressionContext(PrimaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterID_primaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitID_primaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitID_primaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Constant_primaryExpressionContext extends PrimaryExpressionContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public Constant_primaryExpressionContext(PrimaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterConstant_primaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitConstant_primaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitConstant_primaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PAREN_primaryExpressionContext extends PrimaryExpressionContext {
		public TerminalNode LPAREN() { return getToken(MXParser.LPAREN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(MXParser.RPAREN, 0); }
		public PAREN_primaryExpressionContext(PrimaryExpressionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterPAREN_primaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitPAREN_primaryExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitPAREN_primaryExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimaryExpressionContext primaryExpression() throws RecognitionException {
		PrimaryExpressionContext _localctx = new PrimaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_primaryExpression);
		int _la;
		try {
			setState(416);
			switch ( getInterpreter().adaptivePredict(_input,32,_ctx) ) {
			case 1:
				_localctx = new ID_primaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(404);
				match(ID);
				}
				break;
			case 2:
				_localctx = new Constant_primaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(405);
				constant();
				}
				break;
			case 3:
				_localctx = new PAREN_primaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(406);
				match(LPAREN);
				setState(407);
				expression();
				setState(408);
				match(RPAREN);
				}
				break;
			case 4:
				_localctx = new FuncInvoke_primaryExpressionContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(410);
				match(ID);
				setState(411);
				match(LPAREN);
				setState(413);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
					{
					setState(412);
					funcInvoke(0);
					}
				}

				setState(415);
				match(RPAREN);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncInvokeContext extends ParserRuleContext {
		public FuncInvokeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcInvoke; }
	 
		public FuncInvokeContext() { }
		public void copyFrom(FuncInvokeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Compound_funcInvokeContext extends FuncInvokeContext {
		public FuncInvokeContext funcInvoke() {
			return getRuleContext(FuncInvokeContext.class,0);
		}
		public TerminalNode COMMA() { return getToken(MXParser.COMMA, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Compound_funcInvokeContext(FuncInvokeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_funcInvoke(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_funcInvoke(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_funcInvoke(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Expression_funcInvokeContext extends FuncInvokeContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Expression_funcInvokeContext(FuncInvokeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterExpression_funcInvoke(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitExpression_funcInvoke(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitExpression_funcInvoke(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncInvokeContext funcInvoke() throws RecognitionException {
		return funcInvoke(0);
	}

	private FuncInvokeContext funcInvoke(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		FuncInvokeContext _localctx = new FuncInvokeContext(_ctx, _parentState);
		FuncInvokeContext _prevctx = _localctx;
		int _startState = 84;
		enterRecursionRule(_localctx, 84, RULE_funcInvoke, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Expression_funcInvokeContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(419);
			expression();
			}
			_ctx.stop = _input.LT(-1);
			setState(426);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_funcInvokeContext(new FuncInvokeContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_funcInvoke);
					setState(421);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(422);
					match(COMMA);
					setState(423);
					expression();
					}
					} 
				}
				setState(428);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
	 
		public ConstantContext() { }
		public void copyFrom(ConstantContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class IntegerConstant_constantContext extends ConstantContext {
		public TerminalNode IntegerConstant() { return getToken(MXParser.IntegerConstant, 0); }
		public IntegerConstant_constantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterIntegerConstant_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitIntegerConstant_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitIntegerConstant_constant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringConstant_constantContext extends ConstantContext {
		public TerminalNode StringConstant() { return getToken(MXParser.StringConstant, 0); }
		public StringConstant_constantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterStringConstant_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitStringConstant_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitStringConstant_constant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolConstant_constantContext extends ConstantContext {
		public TerminalNode BoolConstant() { return getToken(MXParser.BoolConstant, 0); }
		public BoolConstant_constantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBoolConstant_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBoolConstant_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBoolConstant_constant(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NULL_constantContext extends ConstantContext {
		public TerminalNode NULL() { return getToken(MXParser.NULL, 0); }
		public NULL_constantContext(ConstantContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterNULL_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitNULL_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitNULL_constant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_constant);
		try {
			setState(433);
			switch (_input.LA(1)) {
			case IntegerConstant:
				_localctx = new IntegerConstant_constantContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(429);
				match(IntegerConstant);
				}
				break;
			case BoolConstant:
				_localctx = new BoolConstant_constantContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(430);
				match(BoolConstant);
				}
				break;
			case StringConstant:
				_localctx = new StringConstant_constantContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(431);
				match(StringConstant);
				}
				break;
			case NULL:
				_localctx = new NULL_constantContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(432);
				match(NULL);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreatorContext extends ParserRuleContext {
		public BasicTypeContext basicType() {
			return getRuleContext(BasicTypeContext.class,0);
		}
		public ArrayCreatorContext arrayCreator() {
			return getRuleContext(ArrayCreatorContext.class,0);
		}
		public CreatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_creator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCreator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCreator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CreatorContext creator() throws RecognitionException {
		CreatorContext _localctx = new CreatorContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_creator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(435);
			basicType();
			setState(437);
			switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
			case 1:
				{
				setState(436);
				arrayCreator(0);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayCreatorContext extends ParserRuleContext {
		public ArrayCreatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayCreator; }
	 
		public ArrayCreatorContext() { }
		public void copyFrom(ArrayCreatorContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class Single_arrayCreatorContext extends ArrayCreatorContext {
		public TerminalNode LSQUARE() { return getToken(MXParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(MXParser.RSQUARE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Single_arrayCreatorContext(ArrayCreatorContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterSingle_arrayCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitSingle_arrayCreator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitSingle_arrayCreator(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_arrayCreatorContext extends ArrayCreatorContext {
		public ArrayCreatorContext arrayCreator() {
			return getRuleContext(ArrayCreatorContext.class,0);
		}
		public TerminalNode LSQUARE() { return getToken(MXParser.LSQUARE, 0); }
		public TerminalNode RSQUARE() { return getToken(MXParser.RSQUARE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Compound_arrayCreatorContext(ArrayCreatorContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_arrayCreator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_arrayCreator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_arrayCreator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayCreatorContext arrayCreator() throws RecognitionException {
		return arrayCreator(0);
	}

	private ArrayCreatorContext arrayCreator(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ArrayCreatorContext _localctx = new ArrayCreatorContext(_ctx, _parentState);
		ArrayCreatorContext _prevctx = _localctx;
		int _startState = 90;
		enterRecursionRule(_localctx, 90, RULE_arrayCreator, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new Single_arrayCreatorContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(440);
			match(LSQUARE);
			setState(442);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
				{
				setState(441);
				expression();
				}
			}

			setState(444);
			match(RSQUARE);
			}
			_ctx.stop = _input.LT(-1);
			setState(454);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_arrayCreatorContext(new ArrayCreatorContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_arrayCreator);
					setState(446);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(447);
					match(LSQUARE);
					setState(449);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BoolConstant) | (1L << IntegerConstant) | (1L << StringConstant) | (1L << LPAREN) | (1L << SELFPLUS) | (1L << SELFMINUS) | (1L << MINUS) | (1L << NOT) | (1L << TILDE) | (1L << NULL) | (1L << NEW) | (1L << ID))) != 0)) {
						{
						setState(448);
						expression();
						}
					}

					setState(451);
					match(RSQUARE);
					}
					} 
				}
				setState(456);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ClassDefinitionContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(MXParser.CLASS, 0); }
		public TerminalNode ID() { return getToken(MXParser.ID, 0); }
		public TerminalNode LBRACE() { return getToken(MXParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(MXParser.RBRACE, 0); }
		public MemberDeclarationContext memberDeclaration() {
			return getRuleContext(MemberDeclarationContext.class,0);
		}
		public ClassDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterClassDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitClassDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitClassDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassDefinitionContext classDefinition() throws RecognitionException {
		ClassDefinitionContext _localctx = new ClassDefinitionContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_classDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(457);
			match(CLASS);
			setState(458);
			match(ID);
			setState(459);
			match(LBRACE);
			setState(461);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << BOOL) | (1L << STRING) | (1L << ID))) != 0)) {
				{
				setState(460);
				memberDeclaration(0);
				}
			}

			setState(463);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MemberDeclarationContext extends ParserRuleContext {
		public MemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_memberDeclaration; }
	 
		public MemberDeclarationContext() { }
		public void copyFrom(MemberDeclarationContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ClassMemberDeclaration_memberDeclarationContext extends MemberDeclarationContext {
		public ClassMemberDeclarationContext classMemberDeclaration() {
			return getRuleContext(ClassMemberDeclarationContext.class,0);
		}
		public ClassMemberDeclaration_memberDeclarationContext(MemberDeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterClassMemberDeclaration_memberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitClassMemberDeclaration_memberDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitClassMemberDeclaration_memberDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class Compound_memberDeclarationContext extends MemberDeclarationContext {
		public MemberDeclarationContext memberDeclaration() {
			return getRuleContext(MemberDeclarationContext.class,0);
		}
		public ClassMemberDeclarationContext classMemberDeclaration() {
			return getRuleContext(ClassMemberDeclarationContext.class,0);
		}
		public Compound_memberDeclarationContext(MemberDeclarationContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterCompound_memberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitCompound_memberDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitCompound_memberDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MemberDeclarationContext memberDeclaration() throws RecognitionException {
		return memberDeclaration(0);
	}

	private MemberDeclarationContext memberDeclaration(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		MemberDeclarationContext _localctx = new MemberDeclarationContext(_ctx, _parentState);
		MemberDeclarationContext _prevctx = _localctx;
		int _startState = 94;
		enterRecursionRule(_localctx, 94, RULE_memberDeclaration, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			_localctx = new ClassMemberDeclaration_memberDeclarationContext(_localctx);
			_ctx = _localctx;
			_prevctx = _localctx;

			setState(466);
			classMemberDeclaration();
			}
			_ctx.stop = _input.LT(-1);
			setState(472);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new Compound_memberDeclarationContext(new MemberDeclarationContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_memberDeclaration);
					setState(468);
					if (!(precpred(_ctx, 1))) throw new FailedPredicateException(this, "precpred(_ctx, 1)");
					setState(469);
					classMemberDeclaration();
					}
					} 
				}
				setState(474);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class ClassMemberDeclarationContext extends ParserRuleContext {
		public SingleParameterContext singleParameter() {
			return getRuleContext(SingleParameterContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(MXParser.SEMICOLON, 0); }
		public ClassMemberDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classMemberDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterClassMemberDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitClassMemberDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitClassMemberDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ClassMemberDeclarationContext classMemberDeclaration() throws RecognitionException {
		ClassMemberDeclarationContext _localctx = new ClassMemberDeclarationContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_classMemberDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(475);
			singleParameter();
			setState(476);
			match(SEMICOLON);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryRelationOperatorContext extends ParserRuleContext {
		public TerminalNode ANDAND() { return getToken(MXParser.ANDAND, 0); }
		public TerminalNode OROR() { return getToken(MXParser.OROR, 0); }
		public BinaryRelationOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryRelationOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBinaryRelationOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBinaryRelationOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBinaryRelationOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryRelationOperatorContext binaryRelationOperator() throws RecognitionException {
		BinaryRelationOperatorContext _localctx = new BinaryRelationOperatorContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_binaryRelationOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(478);
			_la = _input.LA(1);
			if ( !(_la==ANDAND || _la==OROR) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BinaryCalculusOperatorContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(MXParser.AND, 0); }
		public TerminalNode XOR() { return getToken(MXParser.XOR, 0); }
		public TerminalNode OR() { return getToken(MXParser.OR, 0); }
		public BinaryCalculusOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_binaryCalculusOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterBinaryCalculusOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitBinaryCalculusOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitBinaryCalculusOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BinaryCalculusOperatorContext binaryCalculusOperator() throws RecognitionException {
		BinaryCalculusOperatorContext _localctx = new BinaryCalculusOperatorContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_binaryCalculusOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(480);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << AND) | (1L << XOR) | (1L << OR))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualRelationOperatorContext extends ParserRuleContext {
		public TerminalNode EQUAL() { return getToken(MXParser.EQUAL, 0); }
		public TerminalNode NOTEQUAL() { return getToken(MXParser.NOTEQUAL, 0); }
		public EqualRelationOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equalRelationOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterEqualRelationOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitEqualRelationOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitEqualRelationOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualRelationOperatorContext equalRelationOperator() throws RecognitionException {
		EqualRelationOperatorContext _localctx = new EqualRelationOperatorContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_equalRelationOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(482);
			_la = _input.LA(1);
			if ( !(_la==EQUAL || _la==NOTEQUAL) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationOperatorContext extends ParserRuleContext {
		public TerminalNode LESSTHANOREQUAL() { return getToken(MXParser.LESSTHANOREQUAL, 0); }
		public TerminalNode GREATERTHANOREQUAL() { return getToken(MXParser.GREATERTHANOREQUAL, 0); }
		public TerminalNode LESSTHAN() { return getToken(MXParser.LESSTHAN, 0); }
		public TerminalNode GREATERTHAN() { return getToken(MXParser.GREATERTHAN, 0); }
		public RelationOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterRelationOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitRelationOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitRelationOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RelationOperatorContext relationOperator() throws RecognitionException {
		RelationOperatorContext _localctx = new RelationOperatorContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_relationOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(484);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << LESSTHANOREQUAL) | (1L << GREATERTHANOREQUAL) | (1L << LESSTHAN) | (1L << GREATERTHAN))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ShiftOperatorContext extends ParserRuleContext {
		public TerminalNode SHIFTLEFT() { return getToken(MXParser.SHIFTLEFT, 0); }
		public TerminalNode SHIFTRIGHT() { return getToken(MXParser.SHIFTRIGHT, 0); }
		public ShiftOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shiftOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterShiftOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitShiftOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitShiftOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ShiftOperatorContext shiftOperator() throws RecognitionException {
		ShiftOperatorContext _localctx = new ShiftOperatorContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_shiftOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(486);
			_la = _input.LA(1);
			if ( !(_la==SHIFTLEFT || _la==SHIFTRIGHT) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdditiveOperatorContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(MXParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(MXParser.MINUS, 0); }
		public AdditiveOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterAdditiveOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitAdditiveOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitAdditiveOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AdditiveOperatorContext additiveOperator() throws RecognitionException {
		AdditiveOperatorContext _localctx = new AdditiveOperatorContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_additiveOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(488);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicativeOperatorContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(MXParser.STAR, 0); }
		public TerminalNode DIVIDE() { return getToken(MXParser.DIVIDE, 0); }
		public TerminalNode MOD() { return getToken(MXParser.MOD, 0); }
		public MultiplicativeOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).enterMultiplicativeOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MXListener ) ((MXListener)listener).exitMultiplicativeOperator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MXVisitor ) return ((MXVisitor<? extends T>)visitor).visitMultiplicativeOperator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplicativeOperatorContext multiplicativeOperator() throws RecognitionException {
		MultiplicativeOperatorContext _localctx = new MultiplicativeOperatorContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_multiplicativeOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(490);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << STAR) | (1L << DIVIDE) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 4:
			return variableType_sempred((VariableTypeContext)_localctx, predIndex);
		case 10:
			return functionType_sempred((FunctionTypeContext)_localctx, predIndex);
		case 13:
			return parameterList_sempred((ParameterListContext)_localctx, predIndex);
		case 16:
			return statement_sempred((StatementContext)_localctx, predIndex);
		case 30:
			return binaryRelationExpression_sempred((BinaryRelationExpressionContext)_localctx, predIndex);
		case 31:
			return binaryCalculusExpression_sempred((BinaryCalculusExpressionContext)_localctx, predIndex);
		case 32:
			return equalRelationExpression_sempred((EqualRelationExpressionContext)_localctx, predIndex);
		case 33:
			return relationExpression_sempred((RelationExpressionContext)_localctx, predIndex);
		case 34:
			return shiftExpression_sempred((ShiftExpressionContext)_localctx, predIndex);
		case 35:
			return additiveExpression_sempred((AdditiveExpressionContext)_localctx, predIndex);
		case 36:
			return multiplicativeExpression_sempred((MultiplicativeExpressionContext)_localctx, predIndex);
		case 39:
			return suffixExpression_sempred((SuffixExpressionContext)_localctx, predIndex);
		case 42:
			return funcInvoke_sempred((FuncInvokeContext)_localctx, predIndex);
		case 45:
			return arrayCreator_sempred((ArrayCreatorContext)_localctx, predIndex);
		case 47:
			return memberDeclaration_sempred((MemberDeclarationContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean variableType_sempred(VariableTypeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean functionType_sempred(FunctionTypeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 1:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean parameterList_sempred(ParameterListContext _localctx, int predIndex) {
		switch (predIndex) {
		case 2:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean statement_sempred(StatementContext _localctx, int predIndex) {
		switch (predIndex) {
		case 3:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean binaryRelationExpression_sempred(BinaryRelationExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 4:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean binaryCalculusExpression_sempred(BinaryCalculusExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 5:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean equalRelationExpression_sempred(EqualRelationExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 6:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean relationExpression_sempred(RelationExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 7:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean shiftExpression_sempred(ShiftExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 8:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean additiveExpression_sempred(AdditiveExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 9:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean multiplicativeExpression_sempred(MultiplicativeExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 10:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean suffixExpression_sempred(SuffixExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 11:
			return precpred(_ctx, 3);
		case 12:
			return precpred(_ctx, 2);
		case 13:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean funcInvoke_sempred(FuncInvokeContext _localctx, int predIndex) {
		switch (predIndex) {
		case 14:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean arrayCreator_sempred(ArrayCreatorContext _localctx, int predIndex) {
		switch (predIndex) {
		case 15:
			return precpred(_ctx, 1);
		}
		return true;
	}
	private boolean memberDeclaration_sempred(MemberDeclarationContext _localctx, int predIndex) {
		switch (predIndex) {
		case 16:
			return precpred(_ctx, 1);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3A\u01ef\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\3\2\7\2t\n\2\f\2\16\2w\13"+
		"\2\3\2\3\2\3\3\3\3\3\3\5\3~\n\3\3\4\3\4\5\4\u0082\n\4\3\4\3\4\3\5\3\5"+
		"\3\5\3\6\3\6\3\6\3\6\3\6\3\6\7\6\u008f\n\6\f\6\16\6\u0092\13\6\3\7\3\7"+
		"\5\7\u0096\n\7\3\b\3\b\3\b\5\b\u009b\n\b\3\t\3\t\3\t\3\n\3\n\3\13\3\13"+
		"\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00ad\n\f\f\f\16\f\u00b0\13"+
		"\f\3\r\3\r\5\r\u00b4\n\r\3\16\3\16\5\16\u00b8\n\16\3\16\3\16\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\7\17\u00c2\n\17\f\17\16\17\u00c5\13\17\3\20\3\20"+
		"\3\21\3\21\5\21\u00cb\n\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\7\22\u00d4"+
		"\n\22\f\22\16\22\u00d7\13\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3"+
		"\23\3\23\5\23\u00e3\n\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25"+
		"\3\25\5\25\u00ef\n\25\3\26\3\26\3\26\5\26\u00f4\n\26\3\26\3\26\5\26\u00f8"+
		"\n\26\3\26\3\26\5\26\u00fc\n\26\3\26\3\26\3\26\3\27\3\27\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\34\3\34\3\34\3\35"+
		"\3\35\5\35\u0115\n\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\5\36\u011e\n"+
		"\36\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \7 \u0129\n \f \16 \u012c\13 \3!\3!"+
		"\3!\3!\3!\3!\3!\7!\u0135\n!\f!\16!\u0138\13!\3\"\3\"\3\"\3\"\3\"\3\"\3"+
		"\"\7\"\u0141\n\"\f\"\16\"\u0144\13\"\3#\3#\3#\3#\3#\3#\3#\7#\u014d\n#"+
		"\f#\16#\u0150\13#\3$\3$\3$\3$\3$\3$\3$\7$\u0159\n$\f$\16$\u015c\13$\3"+
		"%\3%\3%\3%\3%\3%\3%\7%\u0165\n%\f%\16%\u0168\13%\3&\3&\3&\3&\3&\3&\3&"+
		"\7&\u0171\n&\f&\16&\u0174\13&\3\'\3\'\3\'\3\'\3\'\3\'\5\'\u017c\n\'\3"+
		"(\3(\3)\3)\3)\3)\3)\3)\5)\u0186\n)\3)\3)\3)\3)\3)\3)\7)\u018e\n)\f)\16"+
		")\u0191\13)\3*\3*\5*\u0195\n*\3+\3+\3+\3+\3+\3+\3+\3+\3+\5+\u01a0\n+\3"+
		"+\5+\u01a3\n+\3,\3,\3,\3,\3,\3,\7,\u01ab\n,\f,\16,\u01ae\13,\3-\3-\3-"+
		"\3-\5-\u01b4\n-\3.\3.\5.\u01b8\n.\3/\3/\3/\5/\u01bd\n/\3/\3/\3/\3/\3/"+
		"\5/\u01c4\n/\3/\7/\u01c7\n/\f/\16/\u01ca\13/\3\60\3\60\3\60\3\60\5\60"+
		"\u01d0\n\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\7\61\u01d9\n\61\f\61\16"+
		"\61\u01dc\13\61\3\62\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66"+
		"\3\67\3\67\38\38\39\39\39\2\21\n\26\34\">@BDFHJPV\\`:\2\4\6\b\n\f\16\20"+
		"\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhj"+
		"lnp\2\n\5\2\22\23\25\25\36\37\3\2\31\32\3\2\33\35\3\2 !\3\2\f\17\3\2\20"+
		"\21\3\2\24\25\3\2\26\30\u01ef\2u\3\2\2\2\4}\3\2\2\2\6\177\3\2\2\2\b\u0085"+
		"\3\2\2\2\n\u0088\3\2\2\2\f\u0095\3\2\2\2\16\u009a\3\2\2\2\20\u009c\3\2"+
		"\2\2\22\u009f\3\2\2\2\24\u00a1\3\2\2\2\26\u00a6\3\2\2\2\30\u00b3\3\2\2"+
		"\2\32\u00b5\3\2\2\2\34\u00bb\3\2\2\2\36\u00c6\3\2\2\2 \u00c8\3\2\2\2\""+
		"\u00ce\3\2\2\2$\u00e2\3\2\2\2&\u00e4\3\2\2\2(\u00e7\3\2\2\2*\u00f0\3\2"+
		"\2\2,\u0100\3\2\2\2.\u0102\3\2\2\2\60\u0104\3\2\2\2\62\u0106\3\2\2\2\64"+
		"\u010c\3\2\2\2\66\u010f\3\2\2\28\u0112\3\2\2\2:\u011d\3\2\2\2<\u011f\3"+
		"\2\2\2>\u0121\3\2\2\2@\u012d\3\2\2\2B\u0139\3\2\2\2D\u0145\3\2\2\2F\u0151"+
		"\3\2\2\2H\u015d\3\2\2\2J\u0169\3\2\2\2L\u017b\3\2\2\2N\u017d\3\2\2\2P"+
		"\u017f\3\2\2\2R\u0194\3\2\2\2T\u01a2\3\2\2\2V\u01a4\3\2\2\2X\u01b3\3\2"+
		"\2\2Z\u01b5\3\2\2\2\\\u01b9\3\2\2\2^\u01cb\3\2\2\2`\u01d3\3\2\2\2b\u01dd"+
		"\3\2\2\2d\u01e0\3\2\2\2f\u01e2\3\2\2\2h\u01e4\3\2\2\2j\u01e6\3\2\2\2l"+
		"\u01e8\3\2\2\2n\u01ea\3\2\2\2p\u01ec\3\2\2\2rt\5\4\3\2sr\3\2\2\2tw\3\2"+
		"\2\2us\3\2\2\2uv\3\2\2\2vx\3\2\2\2wu\3\2\2\2xy\7\2\2\3y\3\3\2\2\2z~\5"+
		"\6\4\2{~\5\24\13\2|~\5^\60\2}z\3\2\2\2}{\3\2\2\2}|\3\2\2\2~\5\3\2\2\2"+
		"\177\u0081\5\b\5\2\u0080\u0082\5\20\t\2\u0081\u0080\3\2\2\2\u0081\u0082"+
		"\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084\7&\2\2\u0084\7\3\2\2\2\u0085"+
		"\u0086\5\n\6\2\u0086\u0087\7;\2\2\u0087\t\3\2\2\2\u0088\u0089\b\6\1\2"+
		"\u0089\u008a\5\f\7\2\u008a\u0090\3\2\2\2\u008b\u008c\f\3\2\2\u008c\u008d"+
		"\7\6\2\2\u008d\u008f\7\7\2\2\u008e\u008b\3\2\2\2\u008f\u0092\3\2\2\2\u0090"+
		"\u008e\3\2\2\2\u0090\u0091\3\2\2\2\u0091\13\3\2\2\2\u0092\u0090\3\2\2"+
		"\2\u0093\u0096\5\16\b\2\u0094\u0096\7;\2\2\u0095\u0093\3\2\2\2\u0095\u0094"+
		"\3\2\2\2\u0096\r\3\2\2\2\u0097\u009b\7(\2\2\u0098\u009b\7)\2\2\u0099\u009b"+
		"\7*\2\2\u009a\u0097\3\2\2\2\u009a\u0098\3\2\2\2\u009a\u0099\3\2\2\2\u009b"+
		"\17\3\2\2\2\u009c\u009d\7\"\2\2\u009d\u009e\5\22\n\2\u009e\21\3\2\2\2"+
		"\u009f\u00a0\5:\36\2\u00a0\23\3\2\2\2\u00a1\u00a2\5\26\f\2\u00a2\u00a3"+
		"\7;\2\2\u00a3\u00a4\5\32\16\2\u00a4\u00a5\5 \21\2\u00a5\25\3\2\2\2\u00a6"+
		"\u00a7\b\f\1\2\u00a7\u00a8\5\30\r\2\u00a8\u00ae\3\2\2\2\u00a9\u00aa\f"+
		"\3\2\2\u00aa\u00ab\7\6\2\2\u00ab\u00ad\7\7\2\2\u00ac\u00a9\3\2\2\2\u00ad"+
		"\u00b0\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\27\3\2\2"+
		"\2\u00b0\u00ae\3\2\2\2\u00b1\u00b4\5\f\7\2\u00b2\u00b4\7\63\2\2\u00b3"+
		"\u00b1\3\2\2\2\u00b3\u00b2\3\2\2\2\u00b4\31\3\2\2\2\u00b5\u00b7\7\b\2"+
		"\2\u00b6\u00b8\5\34\17\2\u00b7\u00b6\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\u00b9\3\2\2\2\u00b9\u00ba\7\t\2\2\u00ba\33\3\2\2\2\u00bb\u00bc\b\17\1"+
		"\2\u00bc\u00bd\5\36\20\2\u00bd\u00c3\3\2\2\2\u00be\u00bf\f\3\2\2\u00bf"+
		"\u00c0\7%\2\2\u00c0\u00c2\5\36\20\2\u00c1\u00be\3\2\2\2\u00c2\u00c5\3"+
		"\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\35\3\2\2\2\u00c5"+
		"\u00c3\3\2\2\2\u00c6\u00c7\5\b\5\2\u00c7\37\3\2\2\2\u00c8\u00ca\7\n\2"+
		"\2\u00c9\u00cb\5\"\22\2\u00ca\u00c9\3\2\2\2\u00ca\u00cb\3\2\2\2\u00cb"+
		"\u00cc\3\2\2\2\u00cc\u00cd\7\13\2\2\u00cd!\3\2\2\2\u00ce\u00cf\b\22\1"+
		"\2\u00cf\u00d0\5$\23\2\u00d0\u00d5\3\2\2\2\u00d1\u00d2\f\3\2\2\u00d2\u00d4"+
		"\5$\23\2\u00d3\u00d1\3\2\2\2\u00d4\u00d7\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d5"+
		"\u00d6\3\2\2\2\u00d6#\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d8\u00e3\5 \21\2"+
		"\u00d9\u00e3\5&\24\2\u00da\u00e3\5\6\4\2\u00db\u00e3\5(\25\2\u00dc\u00e3"+
		"\5*\26\2\u00dd\u00e3\5\62\32\2\u00de\u00e3\5\64\33\2\u00df\u00e3\5\66"+
		"\34\2\u00e0\u00e3\58\35\2\u00e1\u00e3\7&\2\2\u00e2\u00d8\3\2\2\2\u00e2"+
		"\u00d9\3\2\2\2\u00e2\u00da\3\2\2\2\u00e2\u00db\3\2\2\2\u00e2\u00dc\3\2"+
		"\2\2\u00e2\u00dd\3\2\2\2\u00e2\u00de\3\2\2\2\u00e2\u00df\3\2\2\2\u00e2"+
		"\u00e0\3\2\2\2\u00e2\u00e1\3\2\2\2\u00e3%\3\2\2\2\u00e4\u00e5\5:\36\2"+
		"\u00e5\u00e6\7&\2\2\u00e6\'\3\2\2\2\u00e7\u00e8\7+\2\2\u00e8\u00e9\7\b"+
		"\2\2\u00e9\u00ea\5:\36\2\u00ea\u00eb\7\t\2\2\u00eb\u00ee\5$\23\2\u00ec"+
		"\u00ed\7,\2\2\u00ed\u00ef\5$\23\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2"+
		"\2\2\u00ef)\3\2\2\2\u00f0\u00f1\7-\2\2\u00f1\u00f3\7\b\2\2\u00f2\u00f4"+
		"\5,\27\2\u00f3\u00f2\3\2\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5"+
		"\u00f7\7&\2\2\u00f6\u00f8\5.\30\2\u00f7\u00f6\3\2\2\2\u00f7\u00f8\3\2"+
		"\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fb\7&\2\2\u00fa\u00fc\5\60\31\2\u00fb"+
		"\u00fa\3\2\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fd\3\2\2\2\u00fd\u00fe\7\t"+
		"\2\2\u00fe\u00ff\5$\23\2\u00ff+\3\2\2\2\u0100\u0101\5:\36\2\u0101-\3\2"+
		"\2\2\u0102\u0103\5:\36\2\u0103/\3\2\2\2\u0104\u0105\5:\36\2\u0105\61\3"+
		"\2\2\2\u0106\u0107\7.\2\2\u0107\u0108\7\b\2\2\u0108\u0109\5:\36\2\u0109"+
		"\u010a\7\t\2\2\u010a\u010b\5$\23\2\u010b\63\3\2\2\2\u010c\u010d\7/\2\2"+
		"\u010d\u010e\7&\2\2\u010e\65\3\2\2\2\u010f\u0110\7\60\2\2\u0110\u0111"+
		"\7&\2\2\u0111\67\3\2\2\2\u0112\u0114\7\61\2\2\u0113\u0115\5:\36\2\u0114"+
		"\u0113\3\2\2\2\u0114\u0115\3\2\2\2\u0115\u0116\3\2\2\2\u0116\u0117\7&"+
		"\2\2\u01179\3\2\2\2\u0118\u011e\5<\37\2\u0119\u011a\5L\'\2\u011a\u011b"+
		"\7\"\2\2\u011b\u011c\5:\36\2\u011c\u011e\3\2\2\2\u011d\u0118\3\2\2\2\u011d"+
		"\u0119\3\2\2\2\u011e;\3\2\2\2\u011f\u0120\5> \2\u0120=\3\2\2\2\u0121\u0122"+
		"\b \1\2\u0122\u0123\5@!\2\u0123\u012a\3\2\2\2\u0124\u0125\f\3\2\2\u0125"+
		"\u0126\5d\63\2\u0126\u0127\5@!\2\u0127\u0129\3\2\2\2\u0128\u0124\3\2\2"+
		"\2\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b?"+
		"\3\2\2\2\u012c\u012a\3\2\2\2\u012d\u012e\b!\1\2\u012e\u012f\5B\"\2\u012f"+
		"\u0136\3\2\2\2\u0130\u0131\f\3\2\2\u0131\u0132\5f\64\2\u0132\u0133\5B"+
		"\"\2\u0133\u0135\3\2\2\2\u0134\u0130\3\2\2\2\u0135\u0138\3\2\2\2\u0136"+
		"\u0134\3\2\2\2\u0136\u0137\3\2\2\2\u0137A\3\2\2\2\u0138\u0136\3\2\2\2"+
		"\u0139\u013a\b\"\1\2\u013a\u013b\5D#\2\u013b\u0142\3\2\2\2\u013c\u013d"+
		"\f\3\2\2\u013d\u013e\5h\65\2\u013e\u013f\5D#\2\u013f\u0141\3\2\2\2\u0140"+
		"\u013c\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2"+
		"\2\2\u0143C\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u0146\b#\1\2\u0146\u0147"+
		"\5F$\2\u0147\u014e\3\2\2\2\u0148\u0149\f\3\2\2\u0149\u014a\5j\66\2\u014a"+
		"\u014b\5F$\2\u014b\u014d\3\2\2\2\u014c\u0148\3\2\2\2\u014d\u0150\3\2\2"+
		"\2\u014e\u014c\3\2\2\2\u014e\u014f\3\2\2\2\u014fE\3\2\2\2\u0150\u014e"+
		"\3\2\2\2\u0151\u0152\b$\1\2\u0152\u0153\5H%\2\u0153\u015a\3\2\2\2\u0154"+
		"\u0155\f\3\2\2\u0155\u0156\5l\67\2\u0156\u0157\5H%\2\u0157\u0159\3\2\2"+
		"\2\u0158\u0154\3\2\2\2\u0159\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b"+
		"\3\2\2\2\u015bG\3\2\2\2\u015c\u015a\3\2\2\2\u015d\u015e\b%\1\2\u015e\u015f"+
		"\5J&\2\u015f\u0166\3\2\2\2\u0160\u0161\f\3\2\2\u0161\u0162\5n8\2\u0162"+
		"\u0163\5J&\2\u0163\u0165\3\2\2\2\u0164\u0160\3\2\2\2\u0165\u0168\3\2\2"+
		"\2\u0166\u0164\3\2\2\2\u0166\u0167\3\2\2\2\u0167I\3\2\2\2\u0168\u0166"+
		"\3\2\2\2\u0169\u016a\b&\1\2\u016a\u016b\5L\'\2\u016b\u0172\3\2\2\2\u016c"+
		"\u016d\f\3\2\2\u016d\u016e\5p9\2\u016e\u016f\5L\'\2\u016f\u0171\3\2\2"+
		"\2\u0170\u016c\3\2\2\2\u0171\u0174\3\2\2\2\u0172\u0170\3\2\2\2\u0172\u0173"+
		"\3\2\2\2\u0173K\3\2\2\2\u0174\u0172\3\2\2\2\u0175\u017c\5P)\2\u0176\u0177"+
		"\7\65\2\2\u0177\u017c\5Z.\2\u0178\u0179\5N(\2\u0179\u017a\5L\'\2\u017a"+
		"\u017c\3\2\2\2\u017b\u0175\3\2\2\2\u017b\u0176\3\2\2\2\u017b\u0178\3\2"+
		"\2\2\u017cM\3\2\2\2\u017d\u017e\t\2\2\2\u017eO\3\2\2\2\u017f\u0180\b)"+
		"\1\2\u0180\u0181\5T+\2\u0181\u018f\3\2\2\2\u0182\u0183\f\5\2\2\u0183\u0185"+
		"\7\6\2\2\u0184\u0186\5:\36\2\u0185\u0184\3\2\2\2\u0185\u0186\3\2\2\2\u0186"+
		"\u0187\3\2\2\2\u0187\u018e\7\7\2\2\u0188\u0189\f\4\2\2\u0189\u018a\7#"+
		"\2\2\u018a\u018e\5T+\2\u018b\u018c\f\3\2\2\u018c\u018e\5R*\2\u018d\u0182"+
		"\3\2\2\2\u018d\u0188\3\2\2\2\u018d\u018b\3\2\2\2\u018e\u0191\3\2\2\2\u018f"+
		"\u018d\3\2\2\2\u018f\u0190\3\2\2\2\u0190Q\3\2\2\2\u0191\u018f\3\2\2\2"+
		"\u0192\u0195\7\22\2\2\u0193\u0195\7\23\2\2\u0194\u0192\3\2\2\2\u0194\u0193"+
		"\3\2\2\2\u0195S\3\2\2\2\u0196\u01a3\7;\2\2\u0197\u01a3\5X-\2\u0198\u0199"+
		"\7\b\2\2\u0199\u019a\5:\36\2\u019a\u019b\7\t\2\2\u019b\u01a3\3\2\2\2\u019c"+
		"\u019d\7;\2\2\u019d\u019f\7\b\2\2\u019e\u01a0\5V,\2\u019f\u019e\3\2\2"+
		"\2\u019f\u01a0\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1\u01a3\7\t\2\2\u01a2\u0196"+
		"\3\2\2\2\u01a2\u0197\3\2\2\2\u01a2\u0198\3\2\2\2\u01a2\u019c\3\2\2\2\u01a3"+
		"U\3\2\2\2\u01a4\u01a5\b,\1\2\u01a5\u01a6\5:\36\2\u01a6\u01ac\3\2\2\2\u01a7"+
		"\u01a8\f\3\2\2\u01a8\u01a9\7%\2\2\u01a9\u01ab\5:\36\2\u01aa\u01a7\3\2"+
		"\2\2\u01ab\u01ae\3\2\2\2\u01ac\u01aa\3\2\2\2\u01ac\u01ad\3\2\2\2\u01ad"+
		"W\3\2\2\2\u01ae\u01ac\3\2\2\2\u01af\u01b4\7\4\2\2\u01b0\u01b4\7\3\2\2"+
		"\u01b1\u01b4\7\5\2\2\u01b2\u01b4\7\64\2\2\u01b3\u01af\3\2\2\2\u01b3\u01b0"+
		"\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b3\u01b2\3\2\2\2\u01b4Y\3\2\2\2\u01b5"+
		"\u01b7\5\f\7\2\u01b6\u01b8\5\\/\2\u01b7\u01b6\3\2\2\2\u01b7\u01b8\3\2"+
		"\2\2\u01b8[\3\2\2\2\u01b9\u01ba\b/\1\2\u01ba\u01bc\7\6\2\2\u01bb\u01bd"+
		"\5:\36\2\u01bc\u01bb\3\2\2\2\u01bc\u01bd\3\2\2\2\u01bd\u01be\3\2\2\2\u01be"+
		"\u01bf\7\7\2\2\u01bf\u01c8\3\2\2\2\u01c0\u01c1\f\3\2\2\u01c1\u01c3\7\6"+
		"\2\2\u01c2\u01c4\5:\36\2\u01c3\u01c2\3\2\2\2\u01c3\u01c4\3\2\2\2\u01c4"+
		"\u01c5\3\2\2\2\u01c5\u01c7\7\7\2\2\u01c6\u01c0\3\2\2\2\u01c7\u01ca\3\2"+
		"\2\2\u01c8\u01c6\3\2\2\2\u01c8\u01c9\3\2\2\2\u01c9]\3\2\2\2\u01ca\u01c8"+
		"\3\2\2\2\u01cb\u01cc\7\62\2\2\u01cc\u01cd\7;\2\2\u01cd\u01cf\7\n\2\2\u01ce"+
		"\u01d0\5`\61\2\u01cf\u01ce\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d1\3\2"+
		"\2\2\u01d1\u01d2\7\13\2\2\u01d2_\3\2\2\2\u01d3\u01d4\b\61\1\2\u01d4\u01d5"+
		"\5b\62\2\u01d5\u01da\3\2\2\2\u01d6\u01d7\f\3\2\2\u01d7\u01d9\5b\62\2\u01d8"+
		"\u01d6\3\2\2\2\u01d9\u01dc\3\2\2\2\u01da\u01d8\3\2\2\2\u01da\u01db\3\2"+
		"\2\2\u01dba\3\2\2\2\u01dc\u01da\3\2\2\2\u01dd\u01de\5\b\5\2\u01de\u01df"+
		"\7&\2\2\u01dfc\3\2\2\2\u01e0\u01e1\t\3\2\2\u01e1e\3\2\2\2\u01e2\u01e3"+
		"\t\4\2\2\u01e3g\3\2\2\2\u01e4\u01e5\t\5\2\2\u01e5i\3\2\2\2\u01e6\u01e7"+
		"\t\6\2\2\u01e7k\3\2\2\2\u01e8\u01e9\t\7\2\2\u01e9m\3\2\2\2\u01ea\u01eb"+
		"\t\b\2\2\u01ebo\3\2\2\2\u01ec\u01ed\t\t\2\2\u01edq\3\2\2\2+u}\u0081\u0090"+
		"\u0095\u009a\u00ae\u00b3\u00b7\u00c3\u00ca\u00d5\u00e2\u00ee\u00f3\u00f7"+
		"\u00fb\u0114\u011d\u012a\u0136\u0142\u014e\u015a\u0166\u0172\u017b\u0185"+
		"\u018d\u018f\u0194\u019f\u01a2\u01ac\u01b3\u01b7\u01bc\u01c3\u01c8\u01cf"+
		"\u01da";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}