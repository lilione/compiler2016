// Generated from G:/study/常规/大二/大二下/编译器/compiler2016/src/parser\MX.g4 by ANTLR 4.5.1
package parser;

package parser;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MXParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MXVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MXParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MXParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableDeclaration_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration_declaration(MXParser.VariableDeclaration_declarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition_declaration(MXParser.FunctionDefinition_declarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classDefinition_declaration}
	 * labeled alternative in {@link MXParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDefinition_declaration(MXParser.ClassDefinition_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#variableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(MXParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#singleParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleParameter(MXParser.SingleParameterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code basicType_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBasicType_variableType(MXParser.BasicType_variableTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_variableType}
	 * labeled alternative in {@link MXParser#variableType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_variableType(MXParser.Compound_variableTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primitiveType_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveType_variableType(MXParser.PrimitiveType_variableTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ID_variableType}
	 * labeled alternative in {@link MXParser#basicType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitID_variableType(MXParser.ID_variableTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code INT_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitINT_primitiveType(MXParser.INT_primitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BOOL_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBOOL_primitiveType(MXParser.BOOL_primitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code STRING_primitiveType}
	 * labeled alternative in {@link MXParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSTRING_primitiveType(MXParser.STRING_primitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#variableInitializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableInitializer(MXParser.VariableInitializerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expression_variableCreator}
	 * labeled alternative in {@link MXParser#variableCreator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_variableCreator(MXParser.Expression_variableCreatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#functionDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDefinition(MXParser.FunctionDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_functionType(MXParser.Compound_functionTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code functionBasicType_functionType}
	 * labeled alternative in {@link MXParser#functionType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionBasicType_functionType(MXParser.FunctionBasicType_functionTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code basicType_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBasicType_functionType(MXParser.BasicType_functionTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code VOID_functionType}
	 * labeled alternative in {@link MXParser#functionBasicType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVOID_functionType(MXParser.VOID_functionTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(MXParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by the {@code singleParameter_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingleParameter_parameterList(MXParser.SingleParameter_parameterListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_parameterList}
	 * labeled alternative in {@link MXParser#parameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_parameterList(MXParser.Compound_parameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#funcParameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncParameter(MXParser.FuncParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(MXParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_statement(MXParser.Compound_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primitiveStatement_statement}
	 * labeled alternative in {@link MXParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveStatement_statement(MXParser.PrimitiveStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code block_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock_statement(MXParser.Block_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement_statement(MXParser.ExpressionStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code variableDeclaration_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration_statement(MXParser.VariableDeclaration_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement_statement(MXParser.IfStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code forStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement_statement(MXParser.ForStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whileStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement_statement(MXParser.WhileStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code breakStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStatement_statement(MXParser.BreakStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code continueStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueStatement_statement(MXParser.ContinueStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returnStatement_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement_statement(MXParser.ReturnStatement_statementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SEMICOLON_statement}
	 * labeled alternative in {@link MXParser#primitiveStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSEMICOLON_statement(MXParser.SEMICOLON_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#expressionStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionStatement(MXParser.ExpressionStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(MXParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#forStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForStatement(MXParser.ForStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#forExpr0}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForExpr0(MXParser.ForExpr0Context ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#forExpr1}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForExpr1(MXParser.ForExpr1Context ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#forExpr2}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitForExpr2(MXParser.ForExpr2Context ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(MXParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#breakStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBreakStatement(MXParser.BreakStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#continueStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitContinueStatement(MXParser.ContinueStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#returnStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnStatement(MXParser.ReturnStatementContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binaryExpression_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpression_expression(MXParser.BinaryExpression_expressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_expression}
	 * labeled alternative in {@link MXParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_expression(MXParser.Compound_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#binaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryExpression(MXParser.BinaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_binaryRelationExpression(MXParser.Compound_binaryRelationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code binaryCalculusExpression_binaryRelationExpression}
	 * labeled alternative in {@link MXParser#binaryRelationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryCalculusExpression_binaryRelationExpression(MXParser.BinaryCalculusExpression_binaryRelationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_binaryCalculusExpression(MXParser.Compound_binaryCalculusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalRelationExpression_binaryCalculusExpression}
	 * labeled alternative in {@link MXParser#binaryCalculusExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualRelationExpression_binaryCalculusExpression(MXParser.EqualRelationExpression_binaryCalculusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationExpression_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationExpression_equalRelationExpression(MXParser.RelationExpression_equalRelationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_equalRelationExpression}
	 * labeled alternative in {@link MXParser#equalRelationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_equalRelationExpression(MXParser.Compound_equalRelationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code shiftExpression_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpression_relationExpression(MXParser.ShiftExpression_relationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_relationExpression}
	 * labeled alternative in {@link MXParser#relationExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_relationExpression(MXParser.Compound_relationExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_shiftExpression(MXParser.Compound_shiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additiveExpression_shiftExpression}
	 * labeled alternative in {@link MXParser#shiftExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpression_shiftExpression(MXParser.AdditiveExpression_shiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicativeExpression_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpression_additiveExpression(MXParser.MultiplicativeExpression_additiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_additiveExpression}
	 * labeled alternative in {@link MXParser#additiveExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_additiveExpression(MXParser.Compound_additiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpressio_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpressio_multiplicativeExpression(MXParser.UnaryExpressio_multiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_multiplicativeExpression}
	 * labeled alternative in {@link MXParser#multiplicativeExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_multiplicativeExpression(MXParser.Compound_multiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code suffixExpression_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuffixExpression_unaryExpression(MXParser.SuffixExpression_unaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NEWcreator_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNEWcreator_unaryExpression(MXParser.NEWcreator_unaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prefix_unaryExpression}
	 * labeled alternative in {@link MXParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefix_unaryExpression(MXParser.Prefix_unaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#prefixOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrefixOperator(MXParser.PrefixOperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryExpression_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression_suffixExpression(MXParser.PrimaryExpression_suffixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayAcess_suffixExpression(MXParser.ArrayAcess_suffixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code suffix_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSuffix_suffixExpression(MXParser.Suffix_suffixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classAcess_suffixExpression}
	 * labeled alternative in {@link MXParser#suffixExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassAcess_suffixExpression(MXParser.ClassAcess_suffixExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SELFPLUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSELFPLUS_suffixOperator(MXParser.SELFPLUS_suffixOperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code SELFMINUS_suffixOperator}
	 * labeled alternative in {@link MXParser#suffixOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSELFMINUS_suffixOperator(MXParser.SELFMINUS_suffixOperatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ID_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitID_primaryExpression(MXParser.ID_primaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code constant_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_primaryExpression(MXParser.Constant_primaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code PAREN_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPAREN_primaryExpression(MXParser.PAREN_primaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code FuncInvoke_primaryExpression}
	 * labeled alternative in {@link MXParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncInvoke_primaryExpression(MXParser.FuncInvoke_primaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_funcInvoke(MXParser.Compound_funcInvokeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expression_funcInvoke}
	 * labeled alternative in {@link MXParser#funcInvoke}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_funcInvoke(MXParser.Expression_funcInvokeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IntegerConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerConstant_constant(MXParser.IntegerConstant_constantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code BoolConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolConstant_constant(MXParser.BoolConstant_constantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code StringConstant_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringConstant_constant(MXParser.StringConstant_constantContext ctx);
	/**
	 * Visit a parse tree produced by the {@code NULL_constant}
	 * labeled alternative in {@link MXParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNULL_constant(MXParser.NULL_constantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#creator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCreator(MXParser.CreatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code single_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSingle_arrayCreator(MXParser.Single_arrayCreatorContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_arrayCreator}
	 * labeled alternative in {@link MXParser#arrayCreator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_arrayCreator(MXParser.Compound_arrayCreatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#classDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassDefinition(MXParser.ClassDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code classMemberDeclaration_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassMemberDeclaration_memberDeclaration(MXParser.ClassMemberDeclaration_memberDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compound_memberDeclaration}
	 * labeled alternative in {@link MXParser#memberDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_memberDeclaration(MXParser.Compound_memberDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#classMemberDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClassMemberDeclaration(MXParser.ClassMemberDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#binaryRelationOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryRelationOperator(MXParser.BinaryRelationOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#binaryCalculusOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBinaryCalculusOperator(MXParser.BinaryCalculusOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#equalRelationOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualRelationOperator(MXParser.EqualRelationOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#relationOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationOperator(MXParser.RelationOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#shiftOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftOperator(MXParser.ShiftOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#additiveOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveOperator(MXParser.AdditiveOperatorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MXParser#multiplicativeOperator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeOperator(MXParser.MultiplicativeOperatorContext ctx);
}