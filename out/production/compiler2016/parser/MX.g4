grammar MX;

@header {
package parser;
}

program
    : declaration* EOF
    ;

declaration
    : variableDeclaration   #variableDeclaration_declaration
    | functionDefinition    #functionDefinition_declaration
    | classDefinition   #classDefinition_declaration
    ;

variableDeclaration
    : singleParameter variableInitializer? SEMICOLON
    ;

singleParameter
    : variableType ID
    ;

variableType
    : basicType #basicType_variableType
    | variableType LSQUARE RSQUARE  #compound_variableType
    ;

basicType
    : primitiveType #primitiveType_variableType
    | ID    #ID_variableType
    ;

primitiveType
    : INT   #INT_primitiveType
    | BOOL  #BOOL_primitiveType
    | STRING    #STRING_primitiveType
    ;

variableInitializer
    : ASSIGNEQUAL variableCreator
    ;

variableCreator
    : expression    #expression_variableCreator
    ;

functionDefinition
    : functionType ID parameter block
    ;

functionType
    : functionBasicType #functionBasicType_functionType
    | functionType LSQUARE RSQUARE  #compound_functionType
    ;

functionBasicType
    : basicType  #basicType_functionType
    | VOID  #VOID_functionType
    ;

parameter
    : LPAREN parameterList? RPAREN
    ;

parameterList
    : funcParameter   #singleParameter_parameterList
    | parameterList COMMA funcParameter   #compound_parameterList
    ;

funcParameter
    : singleParameter
    ;

block
    : LBRACE statement? RBRACE
    ;

statement
    : primitiveStatement    #primitiveStatement_statement
    | statement primitiveStatement  #compound_statement
    ;

primitiveStatement
    : block #block_statement
    | expressionStatement   #expressionStatement_statement
    | variableDeclaration   #variableDeclaration_statement
    | ifStatement   #ifStatement_statement
    | forStatement  #forStatement_statement
    | whileStatement    #whileStatement_statement
    | breakStatement    #breakStatement_statement
    | continueStatement #continueStatement_statement
    | returnStatement   #returnStatement_statement
    | SEMICOLON #SEMICOLON_statement
    ;

expressionStatement
    : expression SEMICOLON
    ;

ifStatement
    : IF LPAREN expression RPAREN primitiveStatement (ELSE primitiveStatement)?
    ;

forStatement
    : FOR LPAREN forExpr0? SEMICOLON forExpr1? SEMICOLON forExpr2? RPAREN primitiveStatement
    ;

forExpr0
    : expression
    ;

forExpr1
    : expression
    ;

forExpr2
    : expression
    ;

whileStatement
    : WHILE LPAREN expression RPAREN primitiveStatement
    ;

breakStatement
    : BREAK SEMICOLON
    ;

continueStatement
    : CONTINUE SEMICOLON
    ;

returnStatement
    : RETURN expression? SEMICOLON
    ;

expression
    : binaryExpression #binaryExpression_expression
    | unaryExpression ASSIGNEQUAL expression #compound_expression
    ;

binaryExpression
    : binaryRelationExpression
    ;
binaryRelationExpression
    : binaryCalculusExpression #binaryCalculusExpression_binaryRelationExpression
    | binaryRelationExpression binaryRelationOperator binaryCalculusExpression #compound_binaryRelationExpression
    ;

binaryCalculusExpression
    : equalRelationExpression #equalRelationExpression_binaryCalculusExpression
    | binaryCalculusExpression binaryCalculusOperator equalRelationExpression   #compound_binaryCalculusExpression
    ;

equalRelationExpression
    : relationExpression    #relationExpression_equalRelationExpression
    | equalRelationExpression equalRelationOperator relationExpression  #compound_equalRelationExpression
    ;

relationExpression
    : shiftExpression   #shiftExpression_relationExpression
    | relationExpression relationOperator shiftExpression   #compound_relationExpression
    ;

shiftExpression
    : additiveExpression   #additiveExpression_shiftExpression
    | shiftExpression shiftOperator additiveExpression  #compound_shiftExpression
    ;

additiveExpression
    : multiplicativeExpression  #multiplicativeExpression_additiveExpression
    | additiveExpression additiveOperator multiplicativeExpression  #compound_additiveExpression
    ;

multiplicativeExpression
    : unaryExpression    #unaryExpressio_multiplicativeExpression
    | multiplicativeExpression multiplicativeOperator unaryExpression    #compound_multiplicativeExpression
    ;

unaryExpression
    : suffixExpression  #suffixExpression_unaryExpression
    | NEW creator   #NEWcreator_unaryExpression
    | prefixOperator unaryExpression    #prefix_unaryExpression
    ;

prefixOperator
    : SELFPLUS
    | SELFMINUS
    | NOT
    | TILDE
    | MINUS
    ;

suffixExpression
    : primaryExpression #primaryExpression_suffixExpression
    | suffixExpression LSQUARE expression? RSQUARE   #arrayAcess_suffixExpression
    | suffixExpression DOT primaryExpression   #classAcess_suffixExpression
    | suffixExpression suffixOperator   #suffix_suffixExpression
    ;

suffixOperator
    : SELFPLUS  #SELFPLUS_suffixOperator
    | SELFMINUS #SELFMINUS_suffixOperator
    ;

primaryExpression
    : ID    #ID_primaryExpression
    | constant  #constant_primaryExpression
    | LPAREN expression RPAREN #PAREN_primaryExpression
    | ID LPAREN funcInvoke? RPAREN  #FuncInvoke_primaryExpression
    ;

funcInvoke
    : expression    #expression_funcInvoke
    | funcInvoke COMMA expression    #compound_funcInvoke
    ;

constant
    : IntegerConstant   #IntegerConstant_constant
    | BoolConstant  #BoolConstant_constant
    | StringConstant    #StringConstant_constant
    | NULL  #NULL_constant
    ;

BoolConstant
    : TRUE
    | FALSE
    ;

IntegerConstant : (NUM | '0');

StringConstant : DOUBLEQUATATION CHAR* DOUBLEQUATATION;

creator
    : basicType arrayCreator?
    ;

arrayCreator
    : LSQUARE expression? RSQUARE    #single_arrayCreator
    | arrayCreator LSQUARE expression? RSQUARE   #compound_arrayCreator
    ;

classDefinition
    : CLASS ID LBRACE memberDeclaration? RBRACE
    ;

memberDeclaration
    : classMemberDeclaration   #classMemberDeclaration_memberDeclaration
    | memberDeclaration classMemberDeclaration #compound_memberDeclaration
    ;

classMemberDeclaration
    : singleParameter SEMICOLON;

binaryRelationOperator
    : ANDAND
    | OROR
    ;

binaryCalculusOperator
    : AND
    | XOR
    | OR
    ;

equalRelationOperator
    : EQUAL
    | NOTEQUAL
    ;

relationOperator
    : LESSTHANOREQUAL
    | GREATERTHANOREQUAL
    | LESSTHAN
    | GREATERTHAN
    ;

shiftOperator
    : SHIFTLEFT
    | SHIFTRIGHT
    ;

additiveOperator
    : PLUS
    | MINUS
    ;

multiplicativeOperator
    : STAR
    | DIVIDE
    | MOD
    ;

LSQUARE : '[';
RSQUARE : ']';
LPAREN : '(';
RPAREN : ')';
LBRACE : '{';
RBRACE : '}';

LESSTHANOREQUAL : '<=';
GREATERTHANOREQUAL : '>=';
LESSTHAN : '<';
GREATERTHAN : '>';

SHIFTLEFT : '<<';
SHIFTRIGHT : '>>';

SELFPLUS : '++';
SELFMINUS : '--';

PLUS : '+';
MINUS : '-';

STAR : '*';
DIVIDE : '/';
MOD : '%';

ANDAND : '&&';
OROR : '||';
AND : '&';
XOR : '^';
OR : '|';
NOT : '!';
TILDE : '~';

EQUAL : '==';
NOTEQUAL : '!=';

ASSIGNEQUAL : '=';

DOT : '.';
COLON : ':';
COMMA : ',';
SEMICOLON : ';';
DOUBLEQUATATION : '"';

INT : 'int';
BOOL : 'bool';
STRING : 'string';

IF : 'if';
ELSE : 'else';
FOR : 'for';
WHILE : 'while';
BREAK : 'break';
CONTINUE : 'continue';
RETURN : 'return';

CLASS : 'class';

VOID : 'void';

NULL : 'null';
NEW : 'new';

TRUE : 'true';
FALSE : 'false';

WS : (' ' | '\t' | '\n' | '\r') ->skip;

COMMENT : '/*' .*? '*/' ->skip;
LINE_COMMENT : '//' ~('\n' | '\r')* '\r'? '\n' ->skip;

ID : LETTER (LETTER | DIGIT | UNDERLINE)*;
LETTER : [a-zA-Z];
DIGIT : [0-9];
NONZERODIGIT : [1-9];
NUM : NONZERODIGIT DIGIT*;
UNDERLINE : '_';

CHAR : (~["\\\n]) | ('\\' [\\"nr]);

