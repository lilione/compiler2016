.data
var_N:
	.word 0
var_b:
	.word 0
var_resultCount:
	.word 0
str_0:
	.word 1
	.align 2
	.asciiz "\n"
str_1:
	.word 1
	.align 2
	.asciiz " "
str_2:
	.word 7
	.align 2
	.asciiz "Total: "
	.text
	.globl main
main:
subu $sp, $sp, 28
sw $ra, 24($sp)
li $a0, 15000
sw $a0, var_N
li $a0, 60008
sw $a0, 8($sp)
li $v0, 9
lw $a0, 8($sp)
syscall
sw $v0, 12($sp)
li $a0, 15001
sw $a0, 16($sp)
lw $a0, 16($sp)
lw $a1, 12($sp)
sw $a0, ($a1)
lw $a0, 12($sp)
sw $a0, var_b
li $a0, 0
sw $a0, var_resultCount
subu $sp, $sp, 100
sw $ra, 96($sp)
jal main_enter
lw $ra, 96($sp)
addu $sp, $sp, 100
lw $ra, 24($sp)
addu $sp, $sp, 28
jr $ra
printInt:
li $v0, 1
syscall
jr $ra
printlnInt:
li $v0, 1
syscall
li $v0, 4
la $a0, str_0
add $a0, $a0, 4
syscall
jr $ra
print:
li	$v0, 4
addu	$a0, $a0, 4
syscall
jr	$ra
println:
add $a0, $a0, 4
li $v0, 4
syscall
la $a0, str_0
add $a0, $a0, 4
syscall
jr $ra
getString:
li	$v0, 9
li	$a0, 8192
syscall
addu	$a0, $v0, 4
li	$a1, 8188
li	$v0, 8
syscall
subu	$v0, $a0, 4
getString_label:
lb	$a1, 0($a0)
addu	$a0, $a0, 1
bnez	$a1, getString_label
subu	$a0, $a0, $v0
subu	$a0, $a0, 5
sw	$a0, 0($v0)
jr	$ra
getInt:
li	$a1, 0
getInt_label1:
li	$v0, 12
syscall
beq	$v0, 45, getInt_label2
bge	$v0, 48, getInt_label3
j	getInt_label1
getInt_label2:
li	$v0, 12
syscall
li	$a1, 1
getInt_label3:
sub	$a0, $v0, 48
getInt_label6:
li	$v0, 12
syscall
blt	$v0, 48, getInt_label4
sub	$v0, $v0, 48
mul	$a0, $a0, 10
add	$a0, $a0, $v0
j getInt_label6
getInt_label4:
move	$v0, $a0
beq	$a1, 0, getInt_label5
neg	$v0, $v0
getInt_label5:
jr	$ra
toString:
move	$a1, $a0
li	$a2, 0
toString_label1:
div	$a1, $a1, 10
addu	$a2, $a2, 1
bnez	$a1, toString_label1
move	$a1, $a0
move	$a0, $a2
addu	$a0, $a0, 9
divu	$a0, $a0, 4
mulou	$a0, $a0, 4
li	$v0, 9
syscall
bltz	$a1, toString_label2
sw	$a2, 0($v0)
addu	$a0, $v0, 4
addu	$a0, $a0, $a2
j	toString_label3
toString_label2:
abs	$a1, $a1
addu	$a2, $a2, 1
sw	$a2, 0($v0)
addu	$a0, $v0, 4
li	$a3, 45
sb	$a3, 0($a0)
addu	$a0, $a0, $a2
toString_label3:
li	$a2, 0
sb	$a2, 0($a0)
toString_label4:
subu	$a0, $a0, 1
rem	$a3, $a1, 10
addu $a3, $a3, 48
sb	$a3, 0($a0)
div	$a1, $a1, 10
bnez	$a1, toString_label4
jr	$ra
substring:
addu	$a1, $a1, 4
addu	$a1, $a1, $a0
addu	$a2, $a2, 4
addu	$a2, $a2, $a0
li	$v0, 9
subu	$a0, $a2, $a1
addu	$a0, $a0, 9
divu	$a0, $a0, 4
mulou	$a0, $a0, 4
syscall
subu	$a3, $a2, $a1
addu	$a3, $a3, 1
sw	$a3, 0($v0)
addu	$a0, $v0, 4
substring_label1:
bgt	$a1, $a2, substring_label2
lb	$a3, 0($a1)
sb	$a3, 0($a0)
addu	$a1, $a1, 1
addu	$a0, $a0, 1
j substring_label1
substring_label2:
li	$a3, 0
sb	$a3, 0($a0)
jr	$ra
parseInt:
li	$a1, 0
addu	$a2, $a0, 4
lb	$a3, 0($a2)
bge	$a3, 48, parseInt_label1
addu	$a2, $a2, 1
lb	$a3, 0($a2)
li	$a1, 1
parseInt_label1:
sub	$a0, $a3, 48
parseInt_label2:
addu	$a2, $a2, 1
lb	$a3, 0($a2)
blt	$a3, 48, parseInt_label3
bgt	$a3, 57, parseInt_label3
sub	$a3, $a3, 48
mul	$a0, $a0, 10
add	$a0, $a0, $a3
j parseInt_label2
parseInt_label3:
move	$v0, $a0
beq	$a1, 0, parseInt_label4
neg	$v0, $v0
parseInt_label4:
jr	$ra
stringAdd:
lw	$a2, 0($a0)
lw	$a3, 0($a1)
add	$a3, $a2, $a3
move	$a2, $a0
move	$a0, $a3
add	$a0, $a0, 8
div	$a0, $a0, 4
mul	$a0, $a0, 4
li	$v0, 9
syscall
sw	$a3, 0($v0)
addu	$a0, $v0, 4
addu	$a2, $a2, 4
stringAdd_label1:
lb	$a3, 0($a2)
beqz	$a3, stringAdd_label2
sb	$a3, 0($a0)
addu	$a2, $a2, 1
addu	$a0, $a0, 1
j	stringAdd_label1
stringAdd_label2:
addu	$a1, $a1, 4
stringAdd_label3:
lb	$a3, 0($a1)
beqz	$a3, stringAdd_label4
sb	$a3, 0($a0)
addu	$a1, $a1, 1
addu	$a0, $a0, 1
j	stringAdd_label3
stringAdd_label4:
li	$a3, 0
sb	$a3, 0($a0)
jr	$ra
stringEquals:
lw	$a2, 0($a0)
lw	$a3, 0($a1)
bne	$a2, $a3, stringEquals_neq
addu	$a0, $a0, 4
addu	$a1, $a1, 4
stringEquals_start:
lb	$a2, 0($a0)
lb	$a3, 0($a1)
addu	$a0, $a0, 1
addu	$a1, $a1, 1
bne	$a2, $a3, stringEquals_neq
beq	$a2, 0, stringEquals_eq
j	stringEquals_start
stringEquals_eq:
li	$v0, 1
j stringEquals_end
stringEquals_neq:
li	$v0, 0
stringEquals_end:
jr	$ra
stringLessThan:
addu	$a0, $a0, 4
addu	$a1, $a1, 4
stringLessThan_start:
lb	$a2, 0($a0)
lb	$a3, 0($a1)
addu	$a0, $a0, 1
addu	$a1, $a1, 1
add	$v0, $a2, $a3
beq	$v0, 0, stringLessThan_no
beq	$a2, 0, stringLessThan_yes
beq	$a3, 0, stringLessThan_no
blt	$a2, $a3, stringLessThan_yes
bgt	$a2, $a3, stringLessThan_no
j	stringLessThan_start
stringLessThan_yes:
li	$v0, 1
j stringLessThan_end
stringLessThan_no:
li	$v0, 0
stringLessThan_end:
jr	$ra
stringLessThanOrEquals:
addu	$a0, $a0, 4
addu	$a1, $a1, 4
stringLessThanOrEquals_start:
lb	$a2, 0($a0)
lb	$a3, 0($a1)
addu	$a0, $a0, 1
addu	$a1, $a1, 1
add	$v0, $a2, $a3
beq	$v0, 0, stringLessThanOrEquals_yes
beq	$a2, 0, stringLessThanOrEquals_yes
beq	$a3, 0, stringLessThanOrEquals_no
blt	$a2, $a3, stringLessThanOrEquals_yes
bgt	$a2, $a3, stringLessThanOrEquals_no
j	stringLessThanOrEquals_start
stringLessThanOrEquals_yes:
li	$v0, 1
j stringLessThanOrEquals_end
stringLessThanOrEquals_no:
li	$v0, 0
stringLessThanOrEquals_end:
jr	$ra
main_enter:
li $a0, 1
sw $a0, 0($sp)
b l_0
l_0:
lw $a0, 0($sp)
lw $a1, var_N
sle $a2, $a0, $a1
sw $a2, 4($sp)
lw $a0, 4($sp)
bne $a0, 1, l_2
l_1:
lw $a0, 0($sp)
li $a1, 4
mul $a2, $a0, $a1
sw $a2, 8($sp)
lw $a0, var_b
lw $a1, 8($sp)
add $a2, $a0, $a1
sw $a2, 12($sp)
li $a0, 1
sw $a0, 16($sp)
lw $a0, 16($sp)
lw $a2, 12($sp)
add $a1, $a2, 4
sw $a0, ($a1)
lw $a0, 0($sp)
li $a1, 1
add $a2, $a0, $a1
sw $a2, 0($sp)
b l_0
l_2:
li $a0, 2
sw $a0, 0($sp)
b l_3
l_3:
lw $a0, 0($sp)
lw $a1, var_N
sle $a2, $a0, $a1
sw $a2, 20($sp)
lw $a0, 20($sp)
bne $a0, 1, l_5
l_4:
lw $a0, 0($sp)
li $a1, 4
mul $a2, $a0, $a1
sw $a2, 24($sp)
lw $a0, var_b
lw $a1, 24($sp)
add $a2, $a0, $a1
sw $a2, 28($sp)
lw $a2, 28($sp)
add $a1, $a2, 4
lw $a0, ($a1)
sw  $a0, 32($sp)
lw $a0, 32($sp)
bne $a0, 1, l_7
l_6:
li $a0, 2
sw $a0, 36($sp)
lw $a0, 0($sp)
li $a1, 3
sgt $a2, $a0, $a1
sw $a2, 40($sp)
lw $a0, 40($sp)
beq $a0, 1, l_12
bne $a0, 1, l_10
l_12:
lw $a0, 0($sp)
li $a1, 2
sub $a2, $a0, $a1
sw $a2, 44($sp)
lw $a0, 44($sp)
li $a1, 4
mul $a2, $a0, $a1
sw $a2, 48($sp)
lw $a0, var_b
lw $a1, 48($sp)
add $a2, $a0, $a1
sw $a2, 52($sp)
lw $a2, 52($sp)
add $a1, $a2, 4
lw $a0, ($a1)
sw  $a0, 56($sp)
lw $a0, 56($sp)
bne $a0, 1, l_10
l_9:
lw $a0, var_resultCount
li $a1, 1
add $a2, $a0, $a1
sw $a2, var_resultCount
la $a0, str_1
sw $a0, 60($sp)
lw $a0, 0($sp)
li $a1, 2
sub $a2, $a0, $a1
sw $a2, 64($sp)
subu $sp, $sp, 128
sw $ra, 124($sp)
lw $a0, 192($sp)
jal printInt
lw $ra, 124($sp)
addu $sp, $sp, 128
subu $sp, $sp, 128
sw $ra, 124($sp)
lw $a0, 188($sp)
jal print
lw $ra, 124($sp)
addu $sp, $sp, 128
subu $sp, $sp, 128
sw $ra, 124($sp)
lw $a0, 128($sp)
jal printlnInt
lw $ra, 124($sp)
addu $sp, $sp, 128
b l_11
l_10:
l_11:
b l_13
l_13:
lw $a0, 0($sp)
lw $a1, 36($sp)
mul $a2, $a0, $a1
sw $a2, 68($sp)
lw $a0, 68($sp)
lw $a1, var_N
sle $a2, $a0, $a1
sw $a2, 72($sp)
lw $a0, 72($sp)
bne $a0, 1, l_15
l_14:
lw $a0, 0($sp)
lw $a1, 36($sp)
mul $a2, $a0, $a1
sw $a2, 76($sp)
lw $a0, 76($sp)
li $a1, 4
mul $a2, $a0, $a1
sw $a2, 80($sp)
lw $a0, var_b
lw $a1, 80($sp)
add $a2, $a0, $a1
sw $a2, 84($sp)
li $a0, 0
sw $a0, 88($sp)
lw $a0, 88($sp)
lw $a2, 84($sp)
add $a1, $a2, 4
sw $a0, ($a1)
lw $a0, 36($sp)
li $a1, 1
add $a2, $a0, $a1
sw $a2, 36($sp)
b l_13
l_15:
b l_8
l_7:
l_8:
lw $a0, 0($sp)
li $a1, 1
add $a2, $a0, $a1
sw $a2, 0($sp)
b l_3
l_5:
la $a0, str_2
sw $a0, 92($sp)
subu $sp, $sp, 128
sw $ra, 124($sp)
lw $a0, 220($sp)
jal print
lw $ra, 124($sp)
addu $sp, $sp, 128
subu $sp, $sp, 128
sw $ra, 124($sp)
lw $a0, var_resultCount
jal printlnInt
lw $ra, 124($sp)
addu $sp, $sp, 128
li $v0, 0
jr $ra

