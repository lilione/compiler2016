import ast.*;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import ir.LinearIR;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import parser.MXLexer;
import parser.MXParser;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by abcdabcd987 on 2016-04-13.
 */
@RunWith(Parameterized.class)
public class IRBuilderTest {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList<>();
        for (File f : new File("G:\\study\\常规\\大二\\大二下\\编译器\\compiler2016\\compiler2016-testcases\\ir\\").listFiles()) {
            if (f.isFile() && f.getName().endsWith(".mx")) {
                params.add(new Object[] { "G:\\study\\常规\\大二\\大二下\\编译器\\compiler2016\\compiler2016-testcases\\ir\\" + f.getName() });
            }
        }
        return params;
    }
    private String filename;
    public IRBuilderTest(String filename) {
        this.filename = filename;
    }
    @Test
    public void testPass() throws IOException, InterruptedException {
        Table table = new Table();
        int scope = 0;
        LinearIR linearIR = new LinearIR();
         Table pos = new Table();
        System.out.println(filename);

        ByteArrayOutputStream irTextOut = new ByteArrayOutputStream();
        OutputStream tee = new TeeOutputStream(System.out, irTextOut); // if you need this, check out: https://gist.github.com/abcdabcd987/dbc9c82ccba90707da3e6f7d47a6468f
        PrintStream out = new PrintStream(tee);

        // ... Run your compiler
        InputStream in = new FileInputStream(filename); // or System.in;
        //InputStream in = System.in;
        ANTLRInputStream input = new ANTLRInputStream(in);
        //ANTLRInputStream input = new ANTLRInputStream(System.in);
        MXLexer lexer = new MXLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MXParser parser = new MXParser(tokens);
        ParseTree tree = parser.program(); // calc is the starting rule

        if (parser.getNumberOfSyntaxErrors() != 0)  {
            System.out.println("Syntax Error!");
            System.exit(1);
        }

        //System.out.println("LISP:");
        //System.out.println(tree.toStringTree(parser));
        System.out.println();

        ParseTreeWalker walker = new ParseTreeWalker();
        MX_AST evalByListener = new MX_AST();
        walker.walk(evalByListener, tree);
        //System.out.println("Listener:");

        AST root = evalByListener.stack.peek();
        /*Printer printer = new Printer();
        printer.visit(root);
        System.out.println("Done.");*/

        table.beginScope();
        scope++;

        table.put(Symbol.get("size"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("size"))));

        table.put(Symbol.get("print"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("print"), new VarDeclList(new VarDecl(new StringType())))));
        table.put(Symbol.get("println"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("println"), new VarDeclList(new VarDecl(new StringType())))));
        table.put(Symbol.get("getString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("getString"))));
        table.put(Symbol.get("getInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("getInt"))));
        table.put(Symbol.get("toString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("toString"), new VarDeclList(new VarDecl(new IntType())))));

        table.put(Symbol.get("length"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("length"))));
        table.put(Symbol.get("substring"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("substring"), new VarDeclList(new VarDecl(new IntType()), new VarDecl(new IntType())))));
        table.put(Symbol.get("parseInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("parseInt"))));
        table.put(Symbol.get("ord"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("ord"), new VarDeclList(new VarDecl(new IntType())))));

        root.round1(table, scope);
        root.round2(table, scope);
        root.round3(table, scope, false, null);

        root.translate(pos, table, scope, linearIR, null, null);
        out.print(linearIR.print());

        scope--;
        table.endScope();
        //ir.accept(irPrinter);
        out.flush();
        irTextOut.close();

        // Run the virtual machine
        byte[] irText = irTextOut.toByteArray();
        ByteInputStream vmIn = new ByteInputStream(irText, irText.length);
        LLIRInterpreter vm = new LLIRInterpreter(vmIn, false);
        vm.run();

        // Assert result
        BufferedReader br = new BufferedReader(new FileReader(filename));
        String line;
        do {
            line = br.readLine();
        } while (!line.startsWith("/*! assert:"));
        String assertion = line.replace("/*! assert:", "").trim();
        if (assertion.equals("exitcode")) {
            int expected = Integer.valueOf(br.readLine().trim());
            if (vm.getExitcode() != expected)
                throw new RuntimeException("exitcode = " + vm.getExitcode() + ", expected: " + expected);
        } else if (assertion.equals("exception")) {
            if (!vm.exitException())
                throw new RuntimeException("exit successfully, expected an exception.");
        } else {
            throw new RuntimeException("unknown assertion.");
        }
        br.close();
    }
}