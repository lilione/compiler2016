
        import ast.*;
        import org.antlr.v4.runtime.ANTLRInputStream;
        import org.antlr.v4.runtime.CommonTokenStream;
        import org.antlr.v4.runtime.misc.Pair;
        import org.antlr.v4.runtime.tree.ParseTree;
        import org.antlr.v4.runtime.tree.ParseTreeWalker;
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.junit.runners.Parameterized;
        import parser.MXLexer;
        import parser.MXParser;

        import java.io.File;
        import java.io.FileInputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.util.ArrayList;
        import java.util.Collection;

        import static org.junit.Assert.fail;

/**
 * Created by abcdabcd987 on 2016-04-02.
 */
@RunWith(Parameterized.class)
public class SemanticTest {
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList<>();
        for (File f : new File("compiler2016-testcases/passed/").listFiles()) {
            if (f.isFile() && f.getName().endsWith(".mx")) {
                params.add(new Object[] { "compiler2016-testcases/passed/" + f.getName(), true });
            }
        }
        for (File f : new File("compiler2016-testcases/compile_error/").listFiles()) {
            if (f.isFile() && f.getName().endsWith(".mx")) {
                params.add(new Object[] { "compiler2016-testcases/compile_error/" + f.getName(), false });
            }
        }
        return params;
    }

    private String filename;
    private boolean shouldPass;

    public SemanticTest(String filename, boolean shouldPass) {
        this.filename = filename;
        this.shouldPass = shouldPass;
    }

    @Test
    public void testPass() throws IOException {
        System.out.println(filename);

        try {
            Table table = new Table();
            int scope = 0;
            InputStream in = new FileInputStream(filename); // or System.in;
            ANTLRInputStream input = new ANTLRInputStream(in);
            //ANTLRInputStream input = new ANTLRInputStream(System.in);
            MXLexer lexer = new MXLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            MXParser parser = new MXParser(tokens);
            ParseTree tree = parser.program(); // calc is the starting rule

            //System.out.println("LISP:");
            //System.out.println(tree.toStringTree(parser));
            System.out.println();

            ParseTreeWalker walker = new ParseTreeWalker();
            MX_AST evalByListener = new MX_AST();
            walker.walk(evalByListener, tree);
            //System.out.println("Listener:");

            AST root = evalByListener.stack.peek();
            Printer printer = new Printer();
            printer.visit(root);

            table.beginScope();
            scope++;

            table.put(Symbol.get("size"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("size"))));

            table.put(Symbol.get("print"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("print"), new VarDeclList(new VarDecl(new StringType())))));
            table.put(Symbol.get("println"), new Pair<>(scope, new FuncDecl(new VoidType(), Symbol.get("println"), new VarDeclList(new VarDecl(new StringType())))));
            table.put(Symbol.get("getString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("getString"))));
            table.put(Symbol.get("getInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("getInt"))));
            table.put(Symbol.get("toString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("toString"), new VarDeclList(new VarDecl(new IntType())))));

            table.put(Symbol.get("length"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("length"))));
            table.put(Symbol.get("subString"), new Pair<>(scope, new FuncDecl(new StringType(), Symbol.get("subString"), new VarDeclList(new VarDecl(new IntType()), new VarDecl(new IntType())))));
            table.put(Symbol.get("parseInt"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("parseInt"))));
            table.put(Symbol.get("ord"), new Pair<>(scope, new FuncDecl(new IntType(), Symbol.get("ord"), new VarDeclList(new VarDecl(new IntType())))));

            root.round1(table, scope);
            root.round2(table, scope);
            root.round3(table, scope, false, null);

            scope--;
            table.endScope();
            if (!shouldPass) fail("Should not pass.");
        } catch (Exception e) {
            if (shouldPass) throw e;
            else e.printStackTrace();
        }
    }
}
